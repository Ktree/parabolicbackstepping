%% test misc.translate_to_grid.m
function [tests] = testTranslate_to_grid()
	% result = runtests('unittests.misc.testTranslate_to_grid')
	tests = functiontests(localfunctions);
end

function test1Darray(testCase)
%% 1D vectors
	value1Da = ones(9,1);
	value1Db = [1; -3; 2.5];
	newGridI = linspace(0,1,21);
	newGridII = [-120, 2, 4, 6];
	value1DaTranslatedI = misc.translate_to_grid(value1Da, newGridI);
	value1DbTranslatedI = misc.translate_to_grid(value1Db, newGridI);
	value1DbTranslatedII = misc.translate_to_grid(value1Db, newGridII);

	verifyEqual(testCase, value1DaTranslatedI, ones(numel(newGridI), 1));
	verifyEqual(testCase, value1DbTranslatedI([1, 11, 21]), value1Db([1, 2, 3]));
	verifyEqual(testCase, value1DbTranslatedII, [961; 13.5; 35.5; 57.5]);
end

function test1DfunctionHandle(testCase)
%% function handle arrays
	function1a = @(z) [sin(z^2)+1, 2; -1, z]+ones(2, 2);
	newGridII = [-120, 2, 4, 6];
	function1aReference = zeros(numel(newGridII), 2, 2);
	newGridII = [-120, 2, 4, 6];
	for k = 1 : numel(newGridII)
		function1aReference(k, :, :) = function1a(newGridII(k));
	end
	function1aEvaluated = misc.translate_to_grid(function1a, newGridII);
	verifyEqual(testCase, function1aReference, function1aEvaluated);
end

function testMethodArgument(testCase)
	%% optional argument method
	value1Dc = [0, 1, 4];
	gridOld1Dc = [0, 1, 2];
	value1DcTranslated = misc.translate_to_grid(value1Dc(:), 3, 'gridOld', gridOld1Dc, 'method', 'spline');
	verifyEqual(testCase, value1DcTranslated, 9);
end


function test2Darray(testCase)
	%% 2d arrays
	value2Da = [0, 1; 1, 2];
	value2DaTranslated = misc.translate_to_grid(value2Da, [0, 0.5, 1], 'zdim', 2);
	value2DaReference = [0, 0.5, 1; 0.5, 1, 1.5; 1, 1.5, 2];
	verifyEqual(testCase, value2DaTranslated, value2DaReference);

	value2Db = [1, 2, 3; 2, 3, 4];
	value2DbTranslated = misc.translate_to_grid(value2Db, [0, 0.25], 'zdim', 2);
	value2DbReference = [1, 1.5; 1.25, 1.75];
	verifyEqual(testCase, value2DbTranslated, value2DbReference);
end


function test0Darray(testCase)
	%% empty array
	value0Da = [];
	value2DaTranslatedI = misc.translate_to_grid(value0Da, [0, 0.5, 1], 'zdim', 1);
	verifyEmpty(testCase, value2DaTranslatedI);
	verifySize(testCase, value2DaTranslatedI, [3, 0]);
	
	value2DaTranslatedII = misc.translate_to_grid(value0Da, {[0, 1], [0, 0.5, 1]}, 'zdim', 2);
	verifyEmpty(testCase, value2DaTranslatedI);
	verifySize(testCase, value2DaTranslatedII, [2, 3, 0]);
end


function testNonhomogeneousGrid(testCase)
	%% Change gridOld
	value2Db = [1, 2, 3; 2, 3, 4];
	value2DbTranslated2 = misc.translate_to_grid(value2Db, {[1, 2], [2, 3, 4]}, 'zdim', 2, 'gridOld', {[1, 2], [2, 3, 4]});
	verifyEqual(testCase, value2DbTranslated2, value2Db);

	value2DbTranslated3 = misc.translate_to_grid(value2Db, {[1, 2], [1, 2, 3]}, 'zdim', 2);
	value2DbReference3 = value2Db + [3, 4, 5; 3, 4, 5];
	verifyEqual(testCase, value2DbTranslated3, value2DbReference3);

	value2DbTranslated4 = misc.translate_to_grid(value2Db, {[0, 0.5], [0, 2, 4]},'gridOld', {[0, 0.5], [0, 1, 2]});
	value2DbReference4 = value2Db + [0, 1, 2; 0, 1, 2];
	verifyEqual(testCase, value2DbTranslated4, value2DbReference4);
end

