%% test misc.diagNd.m
function [tests] = testDiagNd()
	% result = runtests('unittests.misc.testDiagNd')
	tests = functiontests(localfunctions);
end

function testDiag2d(testCase)
%% Matrix
testData = rand(123, 123);
testCase.verifyEqual(diag(testData), misc.diagNd(testData));
end

function testDiagNdComplicated(testCase)
test1.data = rand(2, 3, 2, 4, 2, 5);
test2.data = rand(3, 2, 2, 4, 2, 5);
test3.data = rand(2, 3, 2, 4, 5, 2);

test1.result = misc.diagNd(test1.data, [1, 3, 5]);
test2.result = misc.diagNd(test2.data, [2, 3, 5]);
test3.result = misc.diagNd(test3.data, [1, 3, 6]);

test1.forResult = zeros(2, 3, 4, 5);
test2.forResult = zeros(2, 3, 4, 5);
test3.forResult = zeros(2, 3, 4, 5);
for it = 1 : 2
	test1.forResult(it,:,:,:) = squeeze(test1.data(it, :, it, :, it, :));
	test2.forResult(it,:,:,:) = squeeze(test2.data(:, it, it, :, it, :));
	test3.forResult(it,:,:,:) = squeeze(test3.data(it, :, it, :, :, it));
end
testCase.verifyEqual(test1.forResult, test1.result);
testCase.verifyEqual(test2.forResult, test2.result);
testCase.verifyEqual(test3.forResult, test3.result);
end


function testDiagNdLessComplicated(testCase)
test1.data = rand(2, 3, 2);
test2.data = rand(3, 2, 2);
test3.data = rand(2, 2, 3);

test1.result = misc.diagNd(test1.data, [1, 3]);
test2.result = misc.diagNd(test2.data, [2, 3]);
test3.result = misc.diagNd(test3.data, [1, 2]);

test1.forResult = zeros(2, 3);
test2.forResult = zeros(2, 3);
test3.forResult = zeros(2, 3);
for it = 1 : 2
	test1.forResult(it,:) = squeeze(test1.data(it, :, it));
	test2.forResult(it,:) = squeeze(test2.data(:, it, it));
	test3.forResult(it,:) = squeeze(test3.data(it, it, :));
end
testCase.verifyEqual(test1.forResult, test1.result, 'test1');
testCase.verifyEqual(test2.forResult, test2.result, 'test2');
testCase.verifyEqual(test3.forResult, test3.result, 'test3');
end
