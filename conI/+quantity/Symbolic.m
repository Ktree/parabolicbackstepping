classdef Symbolic < quantity.Function
	
	properties (SetAccess = protected)
		valueSymbolic sym;
		variable sym;
	end
	properties (Constant)
		defaultSymVar = sym('z', 'real');
	end
	properties 
		symbolicEvaluation = false;
	end
	
	methods
		
		function obj = Symbolic(valueContinuous, varargin)
			
			parentVarargin = {};
			
			if nargin > 0
				
				if isa(valueContinuous, 'symfun')
					valueContinuous = simplify( formula(valueContinuous) );
				end
				
				variableParser = misc.Parser();
				variableParser.addParameter('variable', quantity.Symbolic.getVariable(valueContinuous));
				variableParser.addParameter('symbolicEvaluation', false);
				variableParser.parse(varargin{:});
				variable = variableParser.Results.variable;
				numVar = numel(variable);
				defaultGrid = cell(1, numVar);
				for it = 1:numVar
					if it==1
						defaultGrid{it} = linspace(0, 1, 101).';
					else
						defaultGrid{it} = reshape(linspace(0, 1, 101), [ones(1, it-1), numel(linspace(0, 1, 101))]);
					end
				end
				gridParser = misc.Parser();
				gridParser.addParameter('grid', defaultGrid);
				gridParser.parse(varargin{:});
				
				s = size(valueContinuous);
				S = num2cell(s);
				
				fun = cell(S{:});
				symb = cell(S{:});
				
				for k = 1:numel(valueContinuous)
					
					if iscell(valueContinuous)
						fun{k} = valueContinuous{k};
					elseif isa(valueContinuous, 'sym') || isnumeric(valueContinuous)
						fun{k} = valueContinuous(k);
					else
						error('Type of valueContinuous not supported')
					end
					
					% input check for valueContinuous
					% FIXME: check that varargin does not contain valueSymbolic.
					% FIXME: initialize valueSymbolic with the correct symbolic
					% variable
					if isa(fun{k}, 'sym')
						symb{k} = fun{k};
						fun{k} = quantity.Symbolic.setValueContinuous(symb{k}, variable);
					elseif isa(fun{k}, 'double')
						symb{k} = sym(fun{k});
						fun{k} = quantity.Symbolic.setValueContinuous(symb{k}, variable);						
					elseif isa(fun{k}, 'function_handle')
						symb{k} = sym(fun{k});
					else
						error(['valueContinuous has to be a symbolic function or a function_handle but is a ', class(fun{k})]);
					end
				end
				
				gridName = cellstr(string(variable));
				% check if the reuqired gridName fits to the current
				% variables:
				if isfield(gridParser.Unmatched, 'gridName') ...
						&& ~all(strcmp(gridParser.Unmatched.gridName, gridName))
					error('If the gridName(s) are set explicitly, they have to be the same as the variable names!')
				end
				
				parentVarargin = [{fun}, varargin(:)', {'grid'}, {gridParser.Results.grid}, ...
					{'gridName'}, {gridName}];
			end
			% call parent class
			obj@quantity.Function(parentVarargin{:});
			
			if nargin > 0
				
				if isempty(obj)
					obj = quantity.Symbolic.empty(size(obj));
				end
				
				% special properties
				for k = 1:numel(valueContinuous)
					obj(k).variable = variable; %#ok<AGROW>
					obj(k).valueSymbolic = symb{k}; %#ok<AGROW>
					obj(k).symbolicEvaluation = ...
						variableParser.Results.symbolicEvaluation;  %#ok<AGROW>
				end
			end
		end
				
		function itIs = isConstant(obj)
			itIs = true;
			for k = 1:numel(obj)
				itIs = itIs && isempty(symvar(obj(k).sym));
				if ~itIs 
					break
				end
			end
		end
		
		function i = eq(A, B)
			%==  Equal.
			%   A == B does element by element comparisons between A and B and returns
			%   an array with elements set to logical 1 (TRUE) where the relation is
			%   true and elements set to logical 0 (FALSE) where it is not. A and B
			%   must have compatible sizes. In the simplest cases, they can be the same
			%   size or one can be a scalar. Two inputs have compatible sizes if, for
			%   every dimension, the dimension sizes of the inputs are either the same
			%   or one of them is 1.
			%	
			%	Two quantity.Symbolic objects are equal if they have the
			%	same symbolic expression, same gridSize and same
			%	variables.
			if isempty(A)
				if isempty(B)
					if isequal(size(A),size(B))
						i=1;
						return
					else
						i=0;
						return
					end
				else
					i=0;
					return;
				end
			else
				if isempty(B)
					i=0;
					return
				end
			
				i = all(size(A) == size(B));

				if ~i
					return;
				end

				i = i && isequal([A.valueSymbolic], [B.valueSymbolic]);
				i = i && isequal(A.grid, B.grid);
				i = i && isequal(A.variable, B.variable);
			end
		end
		
		function res = ne(A,B)
			% ~= not equal.
			res = ~(A==B);
		end
		
		function Q = quantity.Discrete(obj, varargin)
			% Cast of a quantity.Symbolic object into a quantity.Discrete
			% object.
			myParser = misc.Parser();
			myParser.addParameter('grid', obj(1).grid);
			myParser.addParameter('gridName', obj(1).gridName);
			myParser.addParameter('name', obj(1).name);
			myParser.parse(varargin{:});
			assert(isequal(myParser.Results.gridName, obj(1).gridName))
			Q = quantity.Discrete(obj.on(), ...
				'grid', myParser.Results.grid, 'gridName', myParser.Results.gridName, ...
				'name', myParser.Results.name);
		end	
		function f = function_handle(obj)
			f = matlabFunction(sym(obj));
		end
		function F = quantity.Function(obj, varargin)
			myParser = misc.Parser();
			myParser.addParameter('grid', obj(1).grid);
			myParser.addParameter('gridName', obj(1).gridName);
			myParser.addParameter('name', obj(1).name);
			myParser.parse(varargin{:});
			assert(isequal(myParser.Results.gridName, obj(1).gridName))
			for k = 1:numel(obj)
				F(k) = quantity.Function(obj(k).function_handle(), ...
					'grid', myParser.Results.grid, ...
					'gridName', myParser.Results.gridName, ...
					'name', myParser.Results.name);			
			end
			
			F = reshape(F, size(obj));
		end
	end % methods
	
	%% mathematical operations
	methods (Access = public)
		function c = cat(dim, a, varargin)
			%CAT Concatenate arrays.
			%   CAT(DIM,A,B) concatenates the arrays of objects A and B
			%   from the class quantity.Discrete along the dimension DIM.
			%   CAT(2,A,B) is the same as [A,B].
			%   CAT(1,A,B) is the same as [A;B].
			%
			%   B = CAT(DIM,A1,A2,A3,A4,...) concatenates the input
			%   arrays A1, A2, etc. along the dimension DIM.
			%
			%   When used with comma separated list syntax, CAT(DIM,C{:}) or 
			%   CAT(DIM,C.FIELD) is a convenient way to concatenate a cell or
			%   structure array containing numeric matrices into a single matrix.
			%
			%   Examples:
			%     a = magic(3); b = pascal(3); 
			%     c = cat(4,a,b)
			%   produces a 3-by-3-by-1-by-2 result and
			%     s = {a b};
			%     for i=1:length(s), 
			%       siz{i} = size(s{i});
			%     end
			%     sizes = cat(1,siz{:})
			%   produces a 2-by-2 array of size vectors.
			%     
			%   See also NUM2CELL.

			%   Copyright 1984-2005 The MathWorks, Inc.
			%   Built-in function.
			if nargin == 1
				objArgin = {a};
			else
				% this function has the very special thing that it a does
				% not have to be an quantity.Discrete object. So it has to
				% be checked which of the input arguments is an
				% quantity.Discrete object. This is considered to be give
				% the basic values for the initialization of new
				% quantity.Discrete values
				objCell = [{a}, varargin(:)'];
				isAquantityDiscrete = cellfun(@(o) isa(o, 'quantity.Discrete'), objCell);
				objIdx = find(isAquantityDiscrete, 1);
				obj = objCell{objIdx};
				
				if ~isempty(obj)
					myVariable = obj(1).variable;
					myGrid = obj(1).grid;
				else
					myVariable = '';
					myGrid = {};
				end
				
				for k = 1:numel(objCell)
					
					if isa(objCell{k}, 'quantity.Symbolic')
						o = objCell{k};
					elseif isa(objCell{k}, 'double') || isa(objCell{k}, 'sym')
						o = quantity.Symbolic( objCell{k}, ...
							'variable', myVariable, ...
							'grid', myGrid);
					end
					objCell{k} = o;
				end
			end
			c = cat@quantity.Discrete(dim, objCell{:});
		end % cat()
		
		function solution = subs(obj, gridName, values)
			%% SUBS substitues gridName and values
			%   solution = subs(obj, gridName, values) TODO
			% This function substitutes the variables specified with
			% gradName with values. It can be used to rename grids or to
			% evaluate (maybe just some) grids.
			% GridName is cell-array of char-arrays
			% chosen from obj.gridName-property. Values is a cell-array of
			% the size of gridName. Each cell can contain arrays themself,
			% but those arrays must be of same size. Values can be
			% char-arrays standing for new gridName or or numerics.
			% In contrast to on() or at(), only some, but not necessarily
			% all variables are evaluated.
			if ~iscell(gridName)
				gridName = {gridName};
			end
			if ~iscell(values)
				values = {values};
			end
			isNumericValue = cellfun(@isnumeric, values);
			if any((cellfun(@(v) numel(v(:)), values)>1) & isNumericValue)
				error('only implemented for one value per grid');
			end
			numericValues = values(isNumericValue);
			if ~isempty(numericValues) && numel(obj(1).gridName) == numel(numericValues)
				% if all grids are evaluated, solution is a double array.
				solution = reshape(obj.on(numericValues), size(obj));
			else
				% evaluate numeric values
				subsGrid = obj(1).grid;
				selectRemainingGrid = false(1, numel(obj(1).grid));
				for currentGridName = gridName(isNumericValue)
					selectGrid = strcmp(obj(1).gridName, currentGridName);
					subsGrid{selectGrid} = values{strcmp(gridName, currentGridName)};
					selectRemainingGrid = selectRemainingGrid | selectGrid;
				end
				newGrid = obj(1).grid(~selectRemainingGrid);
				newGridName = obj(1).gridName(~selectRemainingGrid);
 				for it = 1 : numel(values)
 					if ~isNumericValue(it) && ~isempty(obj(1).gridName(~selectRemainingGrid)) 
						% check if there is a symbolic value and if this value exists in the object
						if ischar(values{it})
							newGridName{strcmp(obj(1).gridName(~selectRemainingGrid), gridName{it})} ...
								= values{it};
						elseif isa(values{it}, 'sym')
							gridNameTemp = symvar(values{it});
							assert(numel(gridNameTemp) == 1, 'replacing one gridName with 2 gridName is not supported');
							newGridName{strcmp(obj(1).gridName(~selectRemainingGrid), gridName{it})} ...
								= char(gridNameTemp);
							
						else
							error(['value{', num2str(it), '} is of unsupported type']);
						end
 					end
				end
								
				symbolicSolution = subs(obj.sym(), gridName, values);				
				
				if isempty(symvar(symbolicSolution(:)))
					% take the new values that are not numeric as the new
					% variables:
					newGridName = unique(newGridName, 'stable');
					newGrid = cell(1, numel(newGridName));
					charValues = cell(0);
					for it = 1 : numel(values)
						if ischar(values{it})
							charValues{end+1} = values{it};
						end
					end
					for it = 1 : numel(newGridName)
						if ~isempty(charValues) && any(contains(charValues, newGridName{it}))
							newGrid{it} = obj.gridOf(gridName{strcmp(values, newGridName{it})});
						else
							newGrid{it} = obj.gridOf(newGridName{it});
						end
					end
					solution = quantity.Symbolic(double(symbolicSolution), ...
						'grid', newGrid, 'variable', newGridName, 'name', obj(1).name);
				else
					% before creating a new quantity, it is checked that
					% newGridName is unique. If there are non-unique
					% gridName, multiple are removed and the finest grid
					% from newGrid is taken.
					uniqueGridName = unique(newGridName, 'stable');
					if numel(newGridName) == numel(uniqueGridName)
						solution = quantity.Symbolic(symbolicSolution, ...
							'grid', newGrid, 'variable', newGridName, 'name', obj(1).name);
					else
						uniqueGrid = cell(1, numel(uniqueGridName));
						for it = 1 : numel(uniqueGrid)
							gridCandidatesTemp = newGrid(...
								strcmp(uniqueGridName{it}, newGridName));
							for jt = 1 : numel(gridCandidatesTemp)
								if ~isempty(uniqueGrid{it})
									assert(uniqueGrid{it}(1) == gridCandidatesTemp{jt}(1) ...
										&& uniqueGrid{it}(end) == gridCandidatesTemp{jt}(end), ...
										'grids must have same domain');
								end
								if isempty(uniqueGrid{it}) || ...
										numel(gridCandidatesTemp{jt}) > numel(uniqueGrid{it})
									uniqueGrid{it} = gridCandidatesTemp{jt};
								end
							end
						end
						solution = quantity.Symbolic(symbolicSolution, ...
							'grid', uniqueGrid, 'variable', uniqueGridName, 'name', obj(1).name);
					end
				end
				
			end
		end % subs()
		
		function solution = subsValueWithArray(obj, gridName, values)
			% This function substitutes the variables specified with
			% gradName with values. Values must be a numerical scalar for
			% each gridName.
			% This method works for value-arrays but not for obj-arrays.
			% In contrast to on() or at(), only some, but not necessarily
			% all variables are evaluated.
			if numel(obj) ~= 1
				error('Yet this method is not implemented for quanitity arrays');
			end
			if ~iscell(gridName)
				gridName = {gridName};
			end
			if ~iscell(values)
				values = {values};
			end
			if numel(values) ~= numel(gridName)
				error(['Number of variables to be substituted and number ', ...
					'of values do not coincide']);
			end
			if isa(values{1}, 'sym')
				error('subs is yet only implemented for numeric values');
			end
			valueSize = [cellfun(@numel, values), 1];
			if all(strcmp(obj(1).gridName, gridName))
				% if all grids are evaluated, solution is a double array.
				solution = obj.at(values);
			else
				symbolicSolution = reshape(subs(obj.valueSymbolic, ...
					obj(1).variable(strcmp(obj(1).gridName, gridName)), values{:}), valueSize);
				solution = quantity.Symbolic(symbolicSolution, ...
					'variable', obj(1).variable(~strcmp(obj(1).gridName, gridName)),...
					'gridName', obj(1).gridName(~strcmp(obj(1).gridName, gridName)), ...
					'grid', obj(1).grid(~strcmp(obj(1).gridName, gridName)));
			end
		end % subsValueWithArray()
		
		function solution = solveAlgebraic(obj, rhs, gridName, varargin)
			%% this method solves
			%	obj(gridName) == rhs
			% for the variable specified by gridName.
			% rhs must be of apropriate size and gridName must
			% be an gridName of obj. If the result is constant (i.e., if
			% obj only depends on variable, then a double array is
			% returned. Else the solution is of the type as obj.
			if ~isequal(size(rhs), size(obj))
				error('rhs has not the same size as quantity');
			end
			if ~iscell(gridName)
				gridName = {gridName};
			end
			if numel(gridName) ~= 1
				error('this function can only solve for one variable');
			end
			if isempty(strcmp(obj(1).gridName, gridName{1}))
				error('quantity does not depend on variable');
			end
			symbolicSolution = solve(obj(:).sym == rhs(:), ...
				obj(1).variable(strcmp(obj(1).gridName, gridName{1})));
			if isnumeric(rhs) && (numel(obj(1).variable) == 1)
				% solution will be a double array
				solution = double(symbolicSolution);
			else
				solution = quantity.Symbolic(symbolicSolution);
			end
		end % solveAlgebraic()
		
		function solution = solveDVariableEqualQuantity(obj, varargin)
			%% solves the first order ODE
			%	dvar / ds = obj(var(s))
			%	var(s=0) = ic
			% for var(s, ic). Herein, var is the (only) continuous variale
			% obj.variable. The initial condition of the IVP is a variable
			% of the result var(s, ic).
			assert(numel(obj(1).gridName) == 1, ...
				'this method is only implemented for quanitities with one gridName');
			
			myParser = misc.Parser();
			myParser.addParameter('initialValueGrid', obj(1).grid{1});
			myParser.addParameter('variableGrid', obj(1).grid{1});
			myParser.addParameter('newGridName', 's');
			myParser.parse(varargin{:});
			
			s = sym(myParser.Results.newGridName);
			syms ic varNew(s)
			symbolicSolution = sym(zeros(size(obj)));
			for it = 1:numel(obj)
				symbolicSolution(it) = ...
					dsolve(diff(varNew(s), s) == subs(obj(it).valueSymbolic, ...
					obj(it).variable, varNew(s)), varNew(0)==ic);
			end
			solution = quantity.Symbolic(symbolicSolution, ...
				'gridName', {myParser.Results.newGridName, 'ic'}, 'variable', {s, ic}, ...
				'grid', {myParser.Results.variableGrid, myParser.Results.initialValueGrid}, ...
				'name', ['solve(', obj(1).name, ')']);
		end % solveDVariableEqualQuantity()
		
		function sym = sym(obj)
			sym = reshape([obj.valueSymbolic], size(obj));
		end % sym()
		
		function A = adj(obj)
			a = misc.adj(obj.valueSymbolic);
			argin = misc.struct2namevaluepair(obj.obj2struct());
			A = quantity.Symbolic(a, argin{:});
			warning('not tested')
		end % adj()
		
		function y = sqrt(x)
			% quadratic root for scalar and diagonal symbolic quantities
			y = quantity.Symbolic(sqrt(x.sym()), ...
				'grid', x(1).grid, 'variable', x(1).variable, ...
				'name', ['sqrt(', x(1).name, ')']);
		end % sqrt()
		
		function y = sqrtm(x)
			% quadratic root for matrices of symbolic quantities
			y = quantity.Symbolic(sqrtm(x.sym()), ...
				'grid', x(1).grid, 'variable', x(1).variable, ...
				'name', ['sqrtm(', x(1).name, ')']);
		end % sqrtm()
		
		function result = diff(obj, k, diffGridName)
			% diff applies the kth-derivative for the variable specified with
			% the input gridName to the obj. If no gridName is specified, then diff
			% applies the derivative w.r.t. to all gridNames / variables.
			if nargin == 1 || isempty(k)
				k = 1; % by default, only one derivatve per diffGridName is applied
			end
			if nargin <= 2 % if no diffGridName is specified, then the derivative 
				% w.r.t. to all gridNames is applied
				diffGridName = obj(1).gridName;
			end
			
			result = obj.copy();
			if iscell(diffGridName)
				for it = 1 : numel(diffGridName)
					result = result.diff(k, diffGridName{it});
				end
			else
				diffVariable = obj.gridName2variable(diffGridName);
				[result.name] = deal(['(d_{' char(diffVariable) '} ' result(1).name, ')']);
				[result.valueDiscrete] = deal([]);
				for l = 1:numel(obj)
					result(l).valueSymbolic = diff(obj(l).valueSymbolic, diffVariable, k);
					result(l).valueContinuous = obj.setValueContinuous(result(l).valueSymbolic, obj(1).variable);
				end
			end
		end % diff()
		
		function b = flipGrid(a, myGridName)
			if ~iscell(myGridName)
				myGridName = {myGridName};
			end
			variableOld = cell(numel(myGridName), 1);
			variableNew = cell(numel(myGridName), 1);
			for it = 1 : numel(myGridName)
				variableOld{it} = sym(myGridName{it});
				variableNew{it} = 1-variableOld{it};
			end
			b = quantity.Symbolic(subs(a.sym, variableOld, variableNew), ...
				'grid', a(1).grid, 'variable', a(1).variable, ...
				'name', ['flip(', a(1).name, ')']);
		end % flipGrid()
		
		function thisVariable = gridName2variable(obj, thisGridName)
			% this method returns the variable thisVariable stored in obj(1).variable
			% in the order specified by thisGridName. If thisGridName is a char, then
			% only one variable is returned. If thisGridName is a cell-array, then a
			% cell array of variables is returned.
			if ischar(thisGridName)
				assert(any(strcmp(obj(1).gridName, thisGridName)), ...
					['The gridName ', thisGridName, ' is not a gridName of this Quantity']);
				variableNames = arrayfun(@(v) char(v), obj(1).variable, 'UniformOutput', false);
				selectVariable = strcmp(variableNames, thisGridName);
				assert(sum(selectVariable) == 1);
				thisVariable = obj(1).variable(selectVariable);
				
			elseif iscell(thisGridName)
				thisVariable = cell(size(thisGridName));
				for it = 1 : numel(thisGridName)
					thisVariable{it} = obj.gridName2variable(thisGridName{it});
				end
			else
				error(['The input gridName of gridName2variable() must be a char-array', ...
					' or a cell-array']);
			end
		end % gridName2variable()
		
		function mObj = uminus(obj)
			% unitary minus: C = -A
			mObj = quantity.Symbolic(-obj.sym, 'grid', obj(1).grid, ...
				'variable', obj(1).variable, 'name', ['-', obj(1).name]);
		end % uminus()
		function mObj = uplus(obj)
			% unitary plus: C = +A
			mObj = copy(obj);
		end % uplus()
		
		function parameters = combineGridGridNameVariable(A, B, parameters)
			assert(isa(A, 'quantity.Symbolic') && isa(B, 'quantity.Symbolic'), ...
				'A and B must be quantity.Symbolic');
			
			if nargin < 3
				parameters = struct(A(1));
			end
			% combine grids of two quantities. This is often used in mathematical
			% operations to obtain the parameters of the resulting quantity.
			idx = computePermutationVectors(A, B);
			parameters.grid = ...
				[A(1).grid(idx.A.common), ...
				A(1).grid(~idx.A.common), ...
				B(1).grid(~idx.B.common)];
			parameters.gridName = [A(1).gridName(idx.A.common), ...
				A(1).gridName(~idx.A.common), ...
				B(1).gridName(~idx.B.common)];
			parameters.variable = [A(1).variable(idx.A.common), ...
				A(1).variable(~idx.A.common), ...
				B(1).variable(~idx.B.common)];
		end % combineGridGridNameVariable()
		
		function C = mtimes(A, B)
			% if one input is ordinary matrix, this is very simple.
			if isempty(B) ||  isempty(A)
				% TODO: actually this only holds for multiplication of
				% matrices. If higher dimensional arrays are multiplied
				% this can lead to wrong results.
				C = quantity.Symbolic( mtimes@quantity.Discrete(A, B) );
				return
			end		
			if isnumeric(B)
				C = quantity.Symbolic(A.sym() * B, ...
					'grid', A(1).grid, 'variable', A(1).variable, ...
					'name', [A(1).name, ' c']);
				return
			end
			if isnumeric(A)
				C = quantity.Symbolic(A * B.sym(), ...
					'grid', B(1).grid, 'variable', B(1).variable, ...
					'name', ['c ', B(1).name]);
				return
			end
			if ~(isa(B, 'quantity.Symbolic')) && isa(B, 'quantity.Function')
				C = (B' * A')';
				return;
			end
			
			parameters = combineGridGridNameVariable(A, B);
			parameters.name = [A(1).name, ' ', B(1).name];
			parameters = misc.struct2namevaluepair(parameters);
			
			C = quantity.Symbolic(A.sym() * B.sym(), parameters{:});
		end % mtimes()
		
		function C = plus(a, b)
			% minus() see quantity.Discrete.
			assert(isequal(size(a), size(b)), 'terms must be of same size');
			if ~isa(a, 'quantity.Discrete')
				a = quantity.Symbolic(a, 'name', 'c');
				parameters = struct(b(1));
			else
				if ~isa(b, 'quantity.Discrete')
					b = quantity.Symbolic(b, 'name', 'c');
				end
				parameters = struct(a(1));
			end
			
			parameters = combineGridGridNameVariable(a, b, parameters);
			parameters.name = [a(1).name, '+' , b(1).name];
			parameters = misc.struct2namevaluepair(parameters);
			
			C = quantity.Symbolic(a.sym() + b.sym(), parameters{:});
		end % plus()
		
		function absQuantity = abs(obj)
			% abs returns the absolut value of the quantity as a quantity
			absQuantity = quantity.Symbolic(abs(obj.sym()), ...
				'grid', obj(1).grid, 'variable', obj(1).variable, ...
				'name', ['|', obj(1).name, '|']);	
		end % abs()
		
		function y = real(obj)
			% real() returns the real part of the obj.
			y = quantity.Symbolic(real(obj.sym()), ...
				'name', ['real(', obj(1).name, ')'], ...
				'grid', obj(1).grid, ...
				'variable', obj(1).variable);
		end % real()
		
		function y = imag(obj)
			% imag() returns the real part of the obj.
			y = quantity.Symbolic(imag(obj.sym()), ...
				'name', ['real(', obj(1).name, ')'], ...
				'grid', obj(1).grid, ...
				'variable', obj(1).variable);
		end % imag()
		
		function y = inv(obj)
			% inv inverts the matrix obj.
			y = quantity.Symbolic(inv(obj.sym()), ...
				'name', ['(', obj(1).name, ')^{-1}'], ...
				'grid', obj(1).grid, ...
				'variable', obj(1).variable);
		end % inv()
		
		function y = exp(obj)
			% exp() is the exponential function using obj as the exponent.
			y = quantity.Symbolic(exp(obj.sym()), ...
				'name', ['exp(', obj(1).name, ')'], ...
				'grid', obj(1).grid, ...
				'variable', obj(1).variable);
		end % exp()
		
		function y = expm(obj)
			% exp() is the matrix-exponential function using obj as the exponent.
			y = quantity.Symbolic(expm(obj.sym()), ...
				'name', ['expm(', obj(1).name, ')'], ...
				'grid', obj(1).grid, ...
				'variable', obj(1).variable);
		end % expm()
		
		function y = ctranspose(obj)
			% ctranspose() or ' is the complex conjugate tranpose
			y = quantity.Symbolic(conj(obj.sym().'), ...
				'name', ['{', obj(1).name, '}^{H}'], ...
				'grid', obj(1).grid, ...
				'variable', obj(1).variable);
		end % expm()
		
		function C = int(obj, z, a, b)
			% from help sym/int:
			% "int(S,v,a,b) is the definite integral of S with respect to v
			%  from a to b."
			
			if nargin == 2
				if ~iscell(z)
					z = {z};
				end
				
				grdIdx = strcmp(obj(1).gridName, z);
				a = obj(1).grid{grdIdx}(1);
				b = obj(1).grid{grdIdx}(end);
			end
			
			if nargin == 3
				b = a;
				a = z;
				z = num2cell( obj(1).variable );
			end
			
			if nargin == 1
				assert(obj.nargin == 1, 'Not Implemented')
				z = obj(1).grid{1};
				a = z(1);
				b = obj.variable;
				z = num2cell( obj(1).variable );
			end
			
			if ~iscell(z)
				z = {z};
			end
			if obj.nargin == numel(z) && isnumeric(a) && isnumeric(b)
 				C = int@quantity.Function(obj, a, b);
% 				for k = 1:numel(obj)
% 				   C(k) = double( int( sym(obj(k)), z{:}, a, b) );
% 				end
% 				C = reshape(C, size(C));
			else
				% compute the symbolic integration
				I = int(obj.sym, z{:}, a, b);
				variableI = quantity.Symbolic.getVariable(I);
				if iscell(variableI)
					gridNameI = cellfun(@(v) char(v), variableI);
				else%if isvector(variableI)
					gridNameI = arrayfun(@(v) char(v), variableI, 'UniformOutput', false);
				end
				
				% create grid for result
				gridI = cell(1, numel(gridNameI));
				for it = 1 : numel(gridNameI)
					gridIdx = obj.gridIndex(gridNameI{it});
					if (gridIdx ~= 0) && ~isempty(gridIdx)
						gridI{it} = obj.gridOf(gridNameI{it});
					else
						% add default grid
						gridI{it} = linspace(0, 1, 101);
					end
				end
				try
					% if the integral could not be calculated explicitly, the
					% integral() function for functions is called. This can only deal
					% with numeric integral bounds. Therefore, this try-catch block
					% is needed.
					C = quantity.Symbolic(I, ...
						'grid', gridI, 'variable', variableI, ...
						'name', ['int[', obj(1).name, ']']);
				catch
					C = cumInt(quantity.Discrete(obj), z, a, b);					
				end
% 				obj.copy();
% 				for k = 1:numel(obj)
% 					C(k).valueSymbolic = I(k);
% 					C(k).valueDiscrete = [];
% 					C(k).valueContinuous = obj.setValueContinuous(I(k));
% 				end
			end
		end % int()
		
		function result = cumInt(obj, domain, lowerBound, upperBound)
			% CUMINT cumulative integration
			%	result = cumInt(obj, domain, lowerBound, upperBound)
			%	performes the integration over 'obj' for the 'domain' and
			%	the specified 'bounds'. 'domain' must be a gridName of the
			%	object. 'lowerBound' and 'upperBound' define the boundaries
			%	of the integration domain. These can be either doubles or
			%	gridNames. If it is double, the bound is constant. If it is
			%	a gridName, the bound is variable and the result is a
			%	function dependent of this variable.
			%	Since cumInt can be done with the usual Symbolic int(), this method
			%	just checks the input and calls int().
			
			% input parser since some default values depend on intGridName.
			myParser = misc.Parser;
			myParser.addRequired('domain', @(d) obj.gridIndex(d) ~= 0);
			myParser.addRequired('lowerBound', ...
				@(l) isnumeric(l) || ischar(l) );
			myParser.addRequired('upperBound', ...
				@(l) isnumeric(l) || ischar(l) );
			myParser.parse(domain, lowerBound, upperBound)
			
			% call int to perform integration
			result = int(obj, {domain}, lowerBound, upperBound);
		end
		
	end
	
	methods (Access = protected)
		
		function v = evaluateFunction(obj, varargin)
			% Evaluates the symbolic expression of this quantity. If the
			% flag symbolicEvaluation is set, the expression is evaluated
			% with subs(...) otherwise a function handle is used.
			if obj.symbolicEvaluation
				v = double(subs(sym(obj), obj.variable, varargin{:}));
			else
				v = evaluateFunction@quantity.Function(obj, varargin{:});
			end
		end		
		
		function p = getCopyParameters(obj)
			p = nameValuePair(obj, 'gridName');
		end
		
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@quantity.Discrete(obj);
			% #TODO insert code here if some properties should not be copied.
		end
		
		
		%TODO change this function to have the possibility to compute it to a
		%certain accuracy
		function Phi = internPeanoBakerSeries(obj, z, z0, N)
			% PEANOBAKERSERIES_SYM Symbolic computation of (partial) peano baker series
			% Detailed description for The Peano-Baker Series can be found in:
			% The Peano-Baker Series, MICHAEL BAAKE AND ULRIKE SCHLAEGEL, 2012
			%
			% [ Phi ] = panobakerseries_sym( A, z0, N )
			%
			% * A:   System-Operator Matrix \in C[s]^(nxn); with n is the number of
			%      states.
			% * z0:  Lower limit of Integration
			% * N:  Number of iterations
			%
			% * Phi: First result of the state transition matrix.(n x n x 1)
			
			I = quantity.Symbolic( zeros(size(obj,1), size(obj,2), N ));
			I(:,:,1) = eye( size(obj,1) );
			Phi = I(:,:,1);
			for k = 1 : N - 1
				I(:, :, k + 1) = int( obj * I(:, :, k), z, z0, z );
				Phi = Phi + I(:, :, k + 1);
			end
		end
		
	end
	
	methods (Static)
		
		function P = zeros(valueSize, varargin)
			%ZEROS initializes an zero quantity.Discrete object of the size
			%specified by the input valueSize.
			P = quantity.Symbolic(zeros(valueSize), varargin{:});
		end

		function [p, q, parameters] = inputNormalizer(a, b)
			
			parameters = [];
			
			function s = innerInputNormalizer(a)
				if isa(a, 'double')
					s.name = num2str(a(:)');
					s.value = a;
					% 					s.variable = [];
				elseif isa(a, 'sym')
					s.name = char(a);
					s.value = a;
					% 					s.variable = [];
				elseif isa(a, 'quantity.Symbolic')
					s.name = a(1).name;
					s.value = a.sym();
					% 					s.variable = a.variable;
					parameters = struct(a);
					parameters = rmfield(parameters, 'gridName');
				end
			end
			
			p = innerInputNormalizer(a);
			q = innerInputNormalizer(b);
			
		end
	end
	
	methods (Static, Access = protected)
		function var = getVariable(symbolicFunction, nVar)
			
			if misc.issym(symbolicFunction(:).')
				var = symvar(symbolicFunction(:).');
			else
				var = [];
			end
			
			if isempty(var)
				% 				var = quantity.Symbolic.defaultSymVar;
			else
				
				if var(1) ~= quantity.Symbolic.defaultSymVar
					% fast solution to order the spatial and temporal variable
					% in the common order: (z, t)
					var = flip(var);
				end
			end
		end
		function doNotCopy = doNotCopyPropertiesName()
			parentalProperties = quantity.Discrete.doNotCopyPropertiesName;
			doNotCopy = {parentalProperties{:}, ...
				'valueSymbolic', 'isConstant'};
		end
		function f = setValueContinuous(f, symVar)
			% converts symVar into a matlabFunction.
			if isa(f, 'sym')
				if nargin == 1
					symVar = quantity.Symbolic.getVariable(f);
				end
				
				if isempty(symvar(f))
					f = @(varargin) double(f) + quantity.Function.zero(varargin{:});
				else
					f = matlabFunction(f, 'Vars', symVar );
				end
			end
		end % setValueContinuous()
	end
end
