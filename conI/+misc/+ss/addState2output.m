function myStateSpaceNew = addState2output(myStateSpace, stateName)
% ADDSTATE2OUTPUT adds all states of the given myStateSpace to the output of
% myStateSpace by using stateName as OutputName.
%
% Inputs:
%	myStateSpace	state space which is modified
%	stateName		string or char-array to be added
% Outputs:
%	myStateSpace	the state space with modified parameters
%
% Example:
% -------------------------------------------------------------------------
%  	mySimulationModel = ss(1, [1, 2, 3], [2; 3], [], ...
% 		'OutputName', {'y', 'z'}, 'InputName', {'u(1)', 'u(2)', 'a'});
% 	mySimulationModel = misc.ss.addState2output(mySimulationModel, 'x');
% -------------------------------------------------------------------------

newC = vertcat(myStateSpace.C, eye(size(myStateSpace.A)));
newD = vertcat(myStateSpace.D, zeros(size(myStateSpace.A, 1), size(myStateSpace, 2)));
stateNameList = repmat({stateName}, size(myStateSpace.A, 1), 1);
for it = 1 : numel(stateNameList)
	stateNameList{it} = [stateName, '(', num2str(it), ')'];
end
newOutputName = [myStateSpace.OutputName; stateNameList];
myStateSpaceNew = ss(myStateSpace.A, myStateSpace.B, newC, newD, myStateSpace.Ts, ...
	'InputName', myStateSpace.InputName, ...
	'OutputName', newOutputName);

end