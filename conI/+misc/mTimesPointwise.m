function [C] = mTimesPointwise(A, B)
% MTIMESPOINTWISE pointwise multiplication of matrix-functions
%   C = mTimesPointwise(A, B) computes the matrix multiplication of a
%   matrix A(z) \in \R^{n times m} and B(z)\in\R^{m times p}.
% At this the first dimension of A, B has to be the discretization of z,
% and the further dimenions correspond to the size of the matrix.

% dimensions and initialization
q = size(A, 1);
n = size(A, 2);
m = size(B, 3);

C = zeros(q, n, m);

for k = 1 : n % rows of P
	for l = 1 : m % columns of P
		% perform the sum over the multiplied dimension and use pointwise
		% multiplication:
		C(:, k, l) = sum(permute(A(:, k, :), [1, 3, 2]) .* B(:, :, l), 2);
	end
end
end
