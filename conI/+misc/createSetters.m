function code = createSetters(class)
% Create definition of setter functions for dependent attributes.
%
% Description:
% >> code = createSetters(classe)
% Returns code lines which define the set and get functions for all 
% dependent properties of the class 'class'.
%   
% Inputs:
% class   Object of an class
%   
% Outputs:
% code    String containing set and get functions
%
% Example:
% -------------------------------------------------------------------------
% obj = dps.parabolic.system.ppide_sys('l',1);
% createSetters(obj)
% >> ans = function obj = set.ndiscPars(obj,value)
%              obj.ndiscPars = value;
%          end
%          function value = get.ndiscPars(obj)
%              value = obj.ndiscPars;
%          end
%          ...
% -------------------------------------------------------------------------
%
import misc.*

% Find dependent properties in class
names = findAttrValue(class,'Dependent',true);

code1= sprintf('properties(Access = private)\n');
code2= [];

% Loop through properties and create a string containging set and get
% function code for every property.
for p_idx = 1:length(names)
	nowName = erase(names{p_idx},'_disc');
	code2 = [code2...
		sprintf([...
		'function obj = set.' names{p_idx} '(obj,value)\n\t'...
			'obj.' nowName ' = value;\n\t'...
		'end\n'...
		'function value = get.' names{p_idx} '(obj)\n\t'...
			'value = obj.' nowName ';\n'...
		'end\n\n'])];
	code1  = [code1...
		sprintf(['\t' names{p_idx} 'Priv\n'])];
end
code1 = [code1 sprintf('end\n\n\n')];

code = [code1 code2];

end
% code2 = [code2...
% 		sprintf([...
% 		'function obj = set.' names{p_idx} '(obj,value)\n\t'...
% 			'obj.' names{p_idx} 'Priv = value;\n\t'...
%  			'obj.simulated = {0,0,0};\n'...
% 		'end\n'...
% 		'function value = get.' names{p_idx} '(obj)\n\t'...
% 			'value = obj.' names{p_idx} 'Priv;\n'...
% 		'end\n\n'])];