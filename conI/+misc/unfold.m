function xUnfolded = unfold(x,z0,dim,nNew)
% misc.unfold unfold discretized vector or array around folding point
%
% xUnfolded = UNFOLD(x,z0) assumes that the matrix x contains the result of a folding operation
% (see misc.fold). That means
%   xUnfolded(0:z0) = x(zdisc,1)
%   xUnfolded(z0:1) = x(zdisc,2)
%   with a correct resampling. The Folding point itself is assumed to belong to the right
%   side, i.e. x(1,2).
%
% xFolded = FOLD(x,z0,dim) permorms unfolding wrt. dimension dim. (spatial coordinate)
% 
% xFolded = FOLD(x,z0,dim,nFolded) uses a new discretization linspace(0,1,nFolded)
%
% see also misc.fold

% created on 07.10.2019 by Simon Kerschbaum

% warning('The folding point will appear twice! Think about fixing!')
% fixing is easy! Insert a spatial point very close to the folding point to achieve this
% double-point and then interpolate on a linear grid!!! SO easy!
	if isvector(x)
		error('x needs at least a spatial coordinate and a folding coordinate with 2 entries.');
	end
	sizX = size(x);
	if nargin < 3
		dim = 1;
	end
	if dim == length(sizX)
		error('The last dimension must be the folding dimension!')
	end
	if sizX(end)~= 2
		error('Length of folding dimension must be exactly 2.')
	end
	if nargin < 4 % nNew = nOld
		nNew = size(x,dim);
	end
	nOld = size(x,dim);
	zNew = linspace(0,1,nNew);
	z0Idx = find(zNew<z0,1,'last');

	if dim~= 1
		xPerm = permute(x,[dim 1:dim-1 dim+1:length(sizX)]);
		sizXPerm = size(xPerm);
	else
		xPerm = x;
		sizXPerm = sizX;
	end
	if ~ismatrix(xPerm)
		xResh = reshape(xPerm,sizX(dim),[],2);
		sizXResh = size(xResh);
		xLeftInterp = numeric.interpolant(...
			{linspace(z0,0,nOld),1:sizXResh(2)},...
			xResh(:,:,1));
		xRightInterp = numeric.interpolant(...
			{linspace(z0+1e-13,1,nOld),1:sizXResh(2)},...
			xResh(:,:,2));
		xUnfoldedReshTemp = zeros(nNew,sizXResh(2));
	else
		xResh = xPerm;
		sizXResh = size(xResh);
		xLeftInterp = numeric.interpolant(...
			{linspace(z0,0,nOld),1:2},xResh(:,1));
		xRightInterp = numeric.interpolant(...
			{linspace(z0+1e-13,1,nOld),1:2},xResh(:,2));
		xUnfoldedReshTemp = zeros(nNew,1);
	end
	
	xUnfoldedReshTemp(1:z0Idx,:) = xLeftInterp.eval({linspace(0,z0,z0Idx),1:sizXResh(2)});
	xUnfoldedReshTemp(z0Idx+1:end,:) = xRightInterp.eval({linspace(z0+1e-13,1,nNew-z0Idx),1:sizXResh(2)});
	% attention, xUnfoldedResh is defined on non-uniform grid:
	xUnfoldedReshInterp = numeric.interpolant({...
		[linspace(0,z0,z0Idx) linspace(z0+1e-13,1,nNew-z0Idx)],...
		1:size(xUnfoldedReshTemp,2)
		},...
		xUnfoldedReshTemp);
	xUnfoldedResh = xUnfoldedReshInterp.eval({zNew,1:size(xUnfoldedReshTemp,2)});
		

	if ~ismatrix(xPerm)
		xUnfoldedPerm = reshape(xUnfoldedResh,[nNew sizXPerm(2:end-1)]);
	else
		xUnfoldedPerm = xUnfoldedResh;
	end
	if dim~= 1
		xUnfolded = permute(xUnfoldedPerm,[2:dim 1 dim+1:length(sizX-1)]);
	else
		xUnfolded = xUnfoldedPerm;
	end
end % function fold
	