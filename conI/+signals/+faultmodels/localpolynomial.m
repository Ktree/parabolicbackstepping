classdef localpolynomial < fault.polynomial
    %FAULT Summary of this class goes here
    %   Detailed explanation goes here
    
    methods

        function obj = localpolynomial(varargin)
            obj@fault.polynomial(varargin{:})
        end
        
        function p = phi(obj, t, lm1, j)
        %PHI computes the monomial of the polynomial basis function
        %	
        %	p = phi(obj, t, k) computes the k-th derivative of the
        %	monomials of the polynomial basis function.
        %   
        %   p = phi(obj, t, k, j) as above, but only returns the j-th
        %   monomial.
           l = lm1+1;
           p = zeros(length(t), obj.n);
           for k = 1:obj.n
               if k-l >= 0
                   p(:,k) = factorial(k-1) / factorial(k-l) * ( t - t(end) ).^(k-l);
               end
           end
           
           
        end
        

        
        
    end
end

