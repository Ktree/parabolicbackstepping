function f1 = matrix_4D(K, varargin)
% PLOT_4D_MATRIX is designed to create a figure of the kernel-functions 
% K(z,zeta,i,j) over z and zeta for all i,j of an linear hyperbolic nxn system.
% For every (i,j) one subfigure represents K(:,:,i,j) on the domain z,
% zeta.
%		   
%   INPUT PARAMETERS:
%			 4D-MATRIX			   K : contains nxn kernel-functions
%   
%   OUTPUT PARAMETERS:
%				FIGURE			   f : figure with plots of kernel
%
% required subprogramms:
%   - 
%
% global variables:
%   - 
%
% history:
%	-	created on 24.06.2016 by Jakob Gabriel
%	-	edited on 29.06.2016 by Jakob Gabriel - no need of an argument sys.n,
%							   therefore reduced number of arguments.
%	-	edited on 30.04.2018 by Jakob Gabriel - some quick-fixes in order
%								to plot arbitrary 4D matrices

f1 = figure('name', '4D-Matrix');
n_C = size(K,3);
n_D = size(K,4);
numpoints_z_A = size(K,1);
numpoints_z_B = size(K,2);
grid_fig_A = linspace(0,1,numpoints_z_A);
grid_fig_B = linspace(0,1,numpoints_z_B);

%% Read Input
p = inputParser;
if numpoints_z_B ~= numpoints_z_A
	default_hide_upper_triangle = false;
else
	default_hide_upper_triangle = true;
end
addParameter(p, 'hide_upper_triangle', default_hide_upper_triangle);
parse(p, varargin{:});

%% Create subplots
subfigpos = 1;
for i = 1:n_C
	for j = 1:n_D
		if p.Results.hide_upper_triangle
			K_ij = K(:,:,i,j) .* (ones(numpoints_z_A) ... 
				+ triu(ones(numpoints_z_A)+NaN,+1));			
		else
			K_ij = K(:,:,i,j);
		end
		subplot(n_C, n_D, subfigpos);
		surf(grid_fig_A, grid_fig_B, K_ij', 'LineStyle', 'none');
		
		box on;
		xlabel('z');
		ylabel('zeta');
		zlabel(['K_', num2str(i), '_', num2str(j)]);
		subfigpos = 1 + subfigpos;
	end
end
end

