function fig_x_del_ic = plot_state_vector_over_time_and_space_without_IC(t, x_of_t, sys)

x_i_of_t = zeros(length(t), sys.numpoints_z, sys.n);
x_of_t_wo_ic = zeros(length(t), sys.n*sys.numpoints_z);
extra = 10;

for time=1:length(t)
    x_i_of_t(time,:,:) = separate_state_vector(x_of_t(time,:), sys);
    
    for i=1:sys.p
        z_del_max = min(sys.numpoints_z, max(sys.numpoints_z - ceil(time * sys.Lambda(i,i)) + extra, 1));
        z_del_min = 1;
        x_i_of_t(time, z_del_min:z_del_max, i) = 0;
    end
    for i=(sys.p+1):sys.n
        z_del_max = sys.numpoints_z;
        z_del_min = max(1 - floor(time * sys.Lambda(i,i)) - extra, 1);
        x_i_of_t(time, z_del_min:z_del_max, i) = 0;
    end
    x_i_temp(:,:) = x_i_of_t(time,:,:);
    x_of_t_wo_ic(time, :) = merge_to_state_vector(x_i_temp, sys);
end
clear x_i_temp x_i_of_t
fig_x_del_ic = plot_state_vector_over_time_and_space(t, x_of_t_wo_ic, sys, true, true);
end