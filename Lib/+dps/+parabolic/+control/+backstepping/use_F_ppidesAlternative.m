function delta_kernel = use_F_ppidesAlternative(kernel,ppide)
% USE_F_PPIDES compute the next step of the fixpoint iteration for systems
% of parabolic pides
%   
%   delta_G = USE_F_PPIDES(kernel,ppide)
%	Compute the next step of the fixpoint iteration
%      delta_G{i+1}(xi,eta) = F[G{i}](xi,eta).
%
%      INPUT ARGUMENTS
%    PPIDE_KERNEL  kernel   : each element of kernel.value is an object of the class
%	                         ppide_kernel_element, which represents the
%	                         corresponding element in the respective
%	                         iteration step.
%    PPIDE_SYS     ppide    : the system of parabolic equations for which
%                            the kernel equations are solved.
%
%      OUTPUT ARGUMENTS
%    PPIDE_KERNEL  delta_kernel  : result of the usage of F on G

% history:
% created on 30.07.2019 by Simon Kerschbaum starting from a copy of use_F_ppides


import misc.*
import numeric.*

n = ppide.n;
delta_GH = cell(n,n);
GH = kernel.value;
fast=1; % Set external in get_idx to 1 to be faster!

reverseStr = '';
msg2='';
msg1 = sprintf('');
fprintf(msg1);
run=0;
zdisc = kernel.zdisc;
ndisc = length(zdisc);
numtKernel = size(kernel.value{1,1}.G,3);



% preallocate
G = cell(n,n,n,n);


for i = 1:n
	for j=1:n
		GInterp = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.G);
% 		JInterp = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.J);
% 		HInterp = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.H);
		for k=1:n			
			for l=1:n				
				% store interpolations of G
				xiMatXi = kernel.xiMatXi{k,l,i,j}; % xi_ij(xi_kl,eta_kl)
				etaMatXi = kernel.etaMatXi{k,l,i,j}; % eta_ij(xi_kl,eta_kl)
				GAll = GInterp.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
				% G_ij(xi_kl,eta_kl)
				G{i,j,k,l} = reshape(GAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
			end
		end
	end
end
MuZ = kernel.MuZ;		   % MuZ{i,j,i,k} =  muij(z(xi_ik,eta_ik))

for lin_idx =1:n^2
% for i=1:n % all rows of G,H
% 	for j=1:n % all columns of G,H
	[i,j] = ind2sub([n n],lin_idx);
	kem = GH{lin_idx}; % abbrv.
	delta_GH{lin_idx} = GH{lin_idx}; % overtake old values. entries G,H are overwritten later
	Gij = GH{lin_idx}.G;
	Hij = GH{lin_idx}.H; % H1
	xiMat = kernel.xiMatXi{i,j,i,j};
	if size(xiMat,3)==1 && numtKernel > 1
		xiMat = repmat(xiMat,1,1,numtKernel);
	end
	etaMat = kernel.etaMatXi{i,j,i,j};
	if size(etaMat,3)==1 && numtKernel > 1
		etaMat = repmat(etaMat,1,1,numtKernel);
	end


	xi_disc = GH{lin_idx}.xi(:);
	eta_disc = GH{lin_idx}.eta(:);
	
	if j>n/2 
		shiftInd = j-n/2;
	else
		shiftInd = j+n/2;
	end
	etaijmn = GH{i,shiftInd}.eta;
	xiijmn = GH{i,shiftInd}.xi;
	etaMatjmn = kernel.etaMatXi{i,shiftInd,i,shiftInd};

	
	% integration inserts NaNs because of the NaN-grid! so remove them egain before
	% next interpolation
	Hijmn = GH{i,shiftInd}.H;
	HijmnAll = misc.writeToOutside(Hijmn);
	HijmnInterp = numeric.interpolant({xiijmn,etaijmn},HijmnAll);
	HijmnEtaEta = HijmnInterp.eval(etaijmn,etaijmn).'; % hier keine Inerp noetig, weil sowieso gleiche Koordinaten ueberall!
	
	HijAll = misc.writeToOutside(Hij);
	HijInterp = numeric.interpolant({xi_disc,eta_disc},HijAll);
	HijEtaEta = HijInterp.eval(eta_disc,eta_disc).';

	Gi = numeric.cumtrapz_fast_nDim(eta_disc,HijmnEtaEta+HijEtaEta,2)...
		+ numeric.cumtrapz_fast_nDim(xiMat,Hij,1,'equal','ignoreNaN');

	delta_GH{i,j}.G = Gi;
	delta_GH{lin_idx}.H =...
		MuZ{i,i,i,i}(1)/4*numeric.cumtrapz_fast_nDim(etaMat,Gij,2,'equal','ignoreNaN');
	
	delta_GH{i,j}.G_t = zeros(size(xiMat));
end % parfor lin_idx

varnames = {'G','H'};
for i=1:n
	for j=1:n
		s = GH{i,j}.s;
		a = (s+1)/2 *GH{i,j}.phi_i(end,:) - (s-1)/2*GH{i,j}.phi_j(end,:); % a Ortsbereich
		for nameIdx = 1:length(varnames)
			delta_GH{i,j}.(varnames{nameIdx})(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi))) & ones(1,1,numtKernel)) = NaN;
			delta_GH{i,j}.(varnames{nameIdx}) = misc.writeToOutside(delta_GH{i,j}.(varnames{nameIdx}));
		end
	end
end


msg2 = newline;
reverseStr = repmat(sprintf('\b'), 1, length(msg1)+length(msg2)-1);
fprintf(reverseStr); % alles wieder l�schen
% fprintf('\n')

delta_kernel = kernel; % Datenuebernahme
delta_kernel.value = delta_GH;
% get original coordinates for G_t

for i=1:n
	for j=1:n
		if any(isnan(delta_kernel.value{i,j}.G(:)))
			here=1;
		end
		if any(isnan(delta_kernel.value{i,j}.H(:)))
			here=1;
		end
		if any(isnan(delta_kernel.value{i,j}.J(:)))
			here=1;
		end
		% transform each element. Needed in the next loop of the fixed-point iteration!
		% Attention: In the result of the fixed-point iteration, which is calculated as the sum
		% of the delta-kernels,, K and K_t are not preserved! They need to be computed again!
		delta_kernel.value{i,j}.K = delta_kernel.value{i,j}.transform_kernel_element(1);
		% Derivative in original coordinates
		delta_kernel.value{i,j}.K_t = zeros(size(delta_kernel.value{i,j}.K));
	end
end
% delta_kernel.plot('G')
% delta_kernel.plot('H')
end



