function kernel = solveKernelEquationsSymbolic(ppide)
% Solve kernel equations symbolic

% lambda = ppide.l;
% a = ppide.a;
% c = ppide.c;
Bd = ppide.b_op1.b_d; % B dirichlet
Bn = ppide.b_op1.b_n; % B Neumann
gf = ppide.gf;

fitAcc = 1e-3;
ndiscPoly = 21;

ndisc = 11;
zdisc = linspace(0,1,ndisc);

syms z zeta xi eta zeta_s x
assume(0<=z<=1);
assume(0<=zeta<=1);
assume(0<=zeta_s<=1);
assume(xi>=0);

for i=1:ppide.n
	for j=1:ppide.n
		ppideAij = ppide.a{i,j};
		ppideF = ppide.f;
		muij = ppide.ctrl_pars.mu{i,j};
		li = subs(misc.fitFunction(ppide.l{i},fitAcc,ndiscPoly),z);
		liD = subs(misc.fitFunction(ppide.lD{i},fitAcc,ndiscPoly),z);
		liD2 = subs(misc.fitFunction(ppide.lD2{i},fitAcc,ndiscPoly),z);
		lj = subs(misc.fitFunction(ppide.l{j},fitAcc,ndiscPoly),z);
		ljD = subs(misc.fitFunction(ppide.lD{j},fitAcc,ndiscPoly),z);
		ljD2 = subs(misc.fitFunction(ppide.lD2{j},fitAcc,ndiscPoly),z);
		phiiOrig = int(1/sqrt(li),z,0,z);
		phii = subs(misc.fitFunction(phiiOrig,fitAcc,ndiscPoly),z);
		phii1 = subs(phii,z,1);
		phijOrig = int(1/sqrt(lj),z,0,zeta);
		phij = subs(misc.fitFunction(phijOrig,fitAcc,ndiscPoly),zeta);
		phij1 = subs(phij,zeta,1);
				
		phiiInv = subs(misc.polyInv(phii,fitAcc,ndiscPoly),x,z);
		phijInv = subs(misc.polyInv(phij,fitAcc,ndiscPoly),x,zeta);
		
% 		figure
% 		hold on
% 		plot(phiiDisc,zdisc);
% 		plot(fitobject);
		
		if subs(li,z,0) >= subs(lj,z,0)
			s=1;
		else
			s=-1;
		end
		zXiEta = subs(phiiInv,z,(1/2*(s*xi+eta)+1/2*(1-s)*double(phii1)));
		zetaXiEta = subs(phijInv,zeta,(1/2*(s*xi-eta)+1/2*(1-s)*double(phij1)));
		% Dependency z,xi on lower border:
		sigmaij = s*(phii+subs(phij,zeta,z))+1/2*(1-s)*(double(phii1)+double(phij1));
		
		[sigmaijInvOrig, gof] = misc.polyInv(sigmaij,fitAcc,ndiscPoly);
		sigmaijInv = subs(sigmaijInvOrig,x,xi);


		
		ci = ppide.c{i};
		ciD = ppide.cD{i};
		cj = ppide.c{j};
		cjD = ppide.cD{j};
		ai = (-(1/2)*liD + ci)/sqrt(li); % (z)
		aj = ((1/2)*subs(ljD,z,zeta) + subs(cj,z,zeta))/sqrt(subs(lj,z,zeta)); %(zeta)
		aij = ai - aj; %(z,zeta)
		bj = subs(cj,z,zeta)*subs(ljD,z,zeta)/subs(lj,z,zeta)-subs(cjD,z,zeta); % (zeta)
		aij_til = ... %(z,zeta)
			-1/2*(1/2*liD2-ciD) + 1/4*liD/li*(1/2*liD-ci) ...
			+1/2*(1/2*subs(ljD2,z,zeta)+subs(cjD,z,zeta))...
				-1/4*subs(ljD,z,zeta)/subs(lj,z,zeta).*(1/2*subs(ljD,z,zeta)+subs(cj,z,zeta));
		r3ij = sqrt(subs(lj,z,0))*Bd(j,j) - subs(cj,z,0)/sqrt(subs(lj,z,0)); 
		r1i = subs(li,z,0)*exp(-int(ci/li,z,0,z)); %(z)
		r2i = sqrt(li); %(z)
		r4j = sqrt(subs(lj,z,0));
		r5j=r4j;
		r6ij = lj*sqrt(li)/(lj-li).*(ppideAij+muij); %(z)
		r7i = 1/subs(li,z,zeta); %(zeta)
		e = subs(lj,z,zeta); %(zeta)
		d = -aij_til - bj; %(z,zeta)
		f = subs(lj,z,zeta)./subs(li,z,zeta); %(zeta)
		g = subs(lj,z,zeta)./subs(li,z,zeta_s); %(zeta,zeta_s)
		
		etaL = subs(phii,z,sigmaijInv) - subs(phij,zeta,sigmaijInv)-1/2*(1-s)*(phii1-phij1); %(xi)
		etaLD = s*(sqrt(subs(lj,z,sigmaijInv))-sqrt(subs(li,z,sigmaijInv)))/(sqrt(subs(li,z,sigmaijInv))+sqrt(subs(lj,z,sigmaijInv))); %(xi)
		c9 = subs(r6ij,z,subs(zXiEta,eta,etaL))*etaLD/(s*etaLD-1); %(xi)
		comInt = subs(misc.fitFunction(int((ppideAij+muij)/2/sqrt(li),z,0,subs(zXiEta,eta,0)),fitAcc,ndiscPoly),xi);
		c10 = -1/4*subs(liD,z,subs(zXiEta,eta,0))*comInt...
			-1/4*sqrt(subs(li,z,subs(zXiEta,eta,0)))*(subs(ppideAij,z,subs(zXiEta,eta,0))+muij);
		
		if i==j
			h01 = c10;
			h04 = -1/4/s*(subs(ai,z,subs(zXiEta,eta,0))-subs(aj,zeta,subs(zetaXiEta,eta,0)))*sqrt(subs(li,z,subs(zXiEta,eta,0)))*comInt; % (xi)
		else
			h01 = c9;
			h04 = 0;
		end
		h02 = -1/4/s*int(subs(lj,z,zetaXiEta)*subs(ppideF{i,j},[z,zeta],[zXiEta,zetaXiEta]),eta,etaL,eta); %(xi,eta)
		if s==-1
			h03 = -1/4/s*(subs(ai,z,subs(zXiEta,xi,eta)-subs(aj,zeta,subs(zetaXiEta,xi,eta))))*gf{i,j}; %(eta)
		else
			h03=0;
		end
		
		h0{i,j} = h01+h02+h03+h04;
		
		
		
		h{1} = h0{1,1};
		% TODO: PROBLEM: FIT GEHT AKTUELL IMMER NUR VON 0 BIS 1 HIER WIRD ABER BIS MAX XI
		% BENOETIGT! --> NOCH KORRIGIEREN! AM EINFACHSTEN GRENZEN MIT UEBERGEBEN!
		h0fit = misc.fitFunction(h0{i,j},fitAcc,ndiscPoly);
		
		testfit = misc.fitFunction(2*z^2+z+0.5+0.5*zeta^2+0.25*zeta,fitAcc,ndiscPoly);
		for k=2:10
			%h{k} = simplify(int(h{k-1},xi,0,xi)); % test von 0 bis xi integrieren 
		end
		
		% Grenzen muessen noch ausgerechnet werden!
		% Auf jeden Fall sieht h0 schon mal gut aus, einziges Problem ist, dass numerische
		% Auswertung des symbolischen Ausdrucks sehr lange dauert!
		% Symbolische Operationen daf�r verschwindend schnell.
		% Interessant wird, wie lange alles dauert, wenn man jetzt mehrfach dar�ber integriert.
		% Da muss noch an Performance geschraubt werden.
% 		xidisc = linspace(0,double(subs(sigmaij,1/2*(1+s))),ndisc);
% 		for xiIdx = 1:ndisc
% 				%h0Disc(xiIdx,:) = double(subs(h0{i,j},{xi,eta},{xidisc(xiIdx),xidisc}));
% 		end
% 		figure
% 		hold on
% 		misc.mesh_sparse(xidisc,xidisc,h0Disc,11,11,51,51)
	end
end








end

