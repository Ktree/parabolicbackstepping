function [controller,  controllerNumeric,  controllerAnalytical, distributedController, controllerFolded] ...
			= compute_ppide_controllerDecoupling(obj, decouplingKernel, KFolding, sortBack, ppide, debug) %#ok<INUSL>
%compute_ppide_controllerDecoupling Compute state feedback controller for the decoupling of
%  folded ppide_systems
%   
%	controller = ompute_ppide_controller(kernel, ppde)
%     compute the state feedback controller
%     K for usage in the control law
%       u = controller*x;
%     for coupled ppide_systems. The vector x contains the discretized values
%     of the different states as follows: x = [x1(0),  x1(h),  x1(2h), ... x2(0), 
%     x2(h), ...]^T,  where h is the discretization distance.
%
%   controller = compute_ppide_controller(kernel, ppde, debug) plots the comparison
%     of numerical and analytical derivatives
%
%   [~,  controllerNumeric] = compute_ppide_controller(kernel, ppde, ...) computes the
%     controller using a numerical derivative of the kernel
%
%   [~, ~, controllerAnalytical] = compute_ppide_controller(kernel, ppde, ...) gets the
%     analytical value of the state feedback (without turning it into a
%     matrix multiplication. (Attention: it also contains the boundary
%     feedback.)
%
%   [~,~,~,distributedController] = compute_ppide_controller(kernel,ppde,...) gets the
%     analytical value of the distributed state feedback (without boundary feedback)
%     ATTENTION: Only works correctly for Neumann Actuation! (TODO!)

% Created on 16.06.2020 by Simon Kerschbaum based on ...Decoupling

%% Import and input checks
import misc.*
import numeric.*
import external.fem.*

if ~exist('debug', 'var')
	debug = 0;
end
n = size(decouplingKernel.value, 2);

%% Initialise parameter
% matrices of actuated boundary
tic
fprintf('Computing decoupling controller...')
Bd = ppide.b_op2.b_d;
Bn = ppide.b_op2.b_n;

% BC cannot be time dependent for decoupling transformation
% Sd0Sd0T = Bd(1:n/2,1:n/2);
Sd1Sd1T = zeros(n/2,n/2);

% Sr0Sr0T = Bn(1:n/2,1:n/2);
% Sr1Sr1T = Bn(n/2+1:end,n/2+1:end);
Sr1Sr1T = zeros(n/2,n/2);

if ~ppide.ctrl_pars.makeTargetNeumann
	error('makeTargetNeumann must be set to 1 for decupling transformation!')
end

% norm to one to get BCs of Target system!
for i=1:n/2
	if Bn(i+n/2,i+n/2)~= 0
		Sr1Sr1T(i,i)=1;
	else
		Sd1Sd1T(i,i)=1;
	end
end

K = decouplingKernel.get_K;
dzK_1_zeta = decouplingKernel.get_K('dz_K_1_zeta',2);
numtKernel = size(decouplingKernel.value{1,1}.G,3);

if numtKernel >1
	timeDependent = 1;
else
	timeDependent = 0;
end
if timeDependent
	numtPlant = ppide.ndiscTime;
	tDiscPlant = linspace(0,1,numtPlant);
else
	numtPlant = 1;
	tDiscPlant = 1;
end


zdisc = decouplingKernel.zdisc;
ndisc = length(zdisc);
ndiscPlant = ppide.ndisc;
zdiscPlant = linspace(0, 1, ndiscPlant);

KInterp = numeric.interpolant({zdisc,zdisc,1:n/2,1:n,linspace(0,1,numtKernel)},K);
dzK_1_zetaInterp = numeric.interpolant({zdisc,1:n/2,1:n,linspace(0,1,numtKernel)},dzK_1_zeta);
KPlant = KInterp.eval({zdiscPlant,zdiscPlant,1:n/2,1:n,linspace(0,1,numtPlant)});
dzK_1_zetaPlant = dzK_1_zetaInterp.eval({zdiscPlant,1:n/2,1:n,linspace(0,1,numtPlant)});

% calculate numerical derivative for comparison
dzK_ij(1:ndisc,1:ndisc,numtKernel) = 0;
dzetaK(1:ndisc,1:ndisc,numtKernel) = 0;
dzK1(1:ndisc,1:n/2,1:n,numtKernel) = 0;
dzetaK0(1:ndisc,1:n/2,1:n,numtKernel) = 0;
for it=1:n/2
	for jt=1:n
		for zeta_idx =1:ndisc
			if zeta_idx < ndisc
				dzK_ij(zeta_idx:end,zeta_idx,:) = diff_num(zdisc(zeta_idx:end),decouplingKernel.value{it,jt}.K(zeta_idx:end,zeta_idx,:));
			end
			if zeta_idx>1
				dzetaK(zeta_idx,1:zeta_idx,:) = diff_num(zdisc(1:zeta_idx),decouplingKernel.value{it,jt}.K(zeta_idx,1:zeta_idx,:),2);
			end
		end
		dzK_ij(end,end,:) = dzK_ij(end,end-1,:); %cannot be computed numerically
		dzetaK(1,1,:) = dzetaK(2,1,:); %cannot be computed numerically
		dzK1(:,it,jt,:) = dzK_ij(end,:,:);
		dzetaK0(:,it,jt,:) = dzetaK(:,1,:);
	end
end
dzK1Interp = numeric.interpolant({zdisc,1:n/2,1:n,linspace(0,1,numtKernel)},dzK1);
dzK1Plant = dzK1Interp.eval({zdiscPlant,1:n/2,1:n,tDiscPlant});

% get mass matrix to implement integral as matrix multiplication
massMatrix = get_lin_mass_matrix(zdiscPlant);

% controller gain under integral
% zerlegung eigentlich nicht noetig!
%												       z i j t
Rleft = permute(misc.multArray(Sr1Sr1T,dzK_1_zetaPlant(:,:,1:n/2,:),2,2),[2 1 3 4])... i z j t --> z i j t
	+ permute(misc.multArray(Sd1Sd1T,reshape(KPlant(end,:,:,1:n/2,:),[ndiscPlant,n/2,n/2,numtPlant]),2,2),[2 1 3 4]);                   
Rright = permute(misc.multArray(Sr1Sr1T,dzK_1_zetaPlant(:,:,n/2+1:end,:),2,2),[2 1 3 4])...   
	+ permute(misc.multArray(Sd1Sd1T,reshape(KPlant(end,:,:,n/2+1:end,:),[ndiscPlant,n/2,n/2,numtPlant]),2,2),[2 1 3 4]);
RleftNumeric = permute(misc.multArray(Sr1Sr1T,dzK1Plant(:,:,1:n/2,:),2,2),[2 1 3 4])...     
	+ permute(misc.multArray(Sd1Sd1T,reshape(KPlant(end,:,:,1:n/2,:),[ndiscPlant,n/2,n/2,numtPlant]),2,2),[2 1 3 4]);
RrightNumeric = permute(misc.multArray(Sr1Sr1T,dzK1Plant(:,:,n/2+1:end,:),2,2),[2 1 3 4])...  
	+ permute(misc.multArray(Sd1Sd1T,reshape(KPlant(end,:,:,n/2+1:end,:),[ndiscPlant,n/2,n/2,numtPlant]),2,2),[2 1 3 4]);
%             z 1 i j t

RzSorted = repmat(reshape(cat(3,Rleft,Rright),[ndiscPlant,1,n/2,n,numtPlant]),1,ndiscPlant,1,1,1);
RzNumericSorted = repmat(reshape(cat(3,RleftNumeric,RrightNumeric),[ndiscPlant,1,n/2,n,numtPlant]),1,ndiscPlant,1,1,1);

% RzSorted = cat(3,zeros(size(RzSorted2)),RzSorted2);
% RzNumericSorted = cat(3,zeros(size(RzNumericSorted2)),RzNumericSorted2);

% onlz sort back columns!
Rz = RzSorted(:,:,:,sortBack,:);
RzNumeric = RzNumericSorted(:,:,:,sortBack,:);
% Rz is brought to same dimensions as K since equalDims is used in multArray.
%             1 zeta i j t
RzetaSorted = reshape(cat(3,Rleft,Rright),1,ndiscPlant,n/2,n,numtPlant);
RzetaNumericSorted = reshape(cat(3,RleftNumeric,RrightNumeric),1,ndiscPlant,n/2,n,numtPlant);
% RzetaSorted = cat(3,zeros(size(RzetaSorted2)),RzetaSorted2);
% RzetaNumericSorted = cat(3,zeros(size(RzetaNumericSorted2)),RzetaNumericSorted2);

Rzeta = RzetaSorted(:,:,:,sortBack,:);
RzetaNumeric = RzetaNumericSorted(:,:,:,sortBack,:);

% Rzeta does not need to be repeated along z!
KFoldPlant = misc.translate_to_grid(KFolding,zdiscPlant,'zdim',2);
if numtPlant > 1
	KFoldPlant = permute(misc.translate_to_grid(permute(KFoldPlant,[5 1 2 3 4]),tDiscPlant),[2 3 4 5 1]);
end
if numtPlant > 1
	RzK = permute(...
		misc.multArray(...
			Rz,KFoldPlant,4,3,[1 2 5]... z zeta t i j
		)...
	,[1 2 4 5 3]); % z zeta i j t
	RzKNumeric = permute(...
		misc.multArray(...
			RzNumeric,KFoldPlant,4,3,[1 2 5]... z zeta t i j
		)...
	,[1 2 4 5 3]); % z zeta i j t
else
	RzK = ...
		misc.multArray(...
			Rz,KFoldPlant,4,3,[1 2]... z zeta i j
		);
	RzKNumeric = ...
		misc.multArray(...
			RzNumeric,KFoldPlant,4,3,[1 2]... z zeta i j
		);
end
zMat = repmat(zdiscPlant(:),1,ndiscPlant,n/2,n,numtPlant);
zMat(zdiscPlant(:).' > zdiscPlant(:) & ones(1,1,n/2,n,numtPlant)) = NaN;
RTil = reshape(...
	Rzeta - numeric.trapz_fast_nDim(zMat,RzK,1,'equal','ignoreNaN')... 1 zeta i j t
	,ndiscPlant,n/2,n,numtPlant);
RTilLeft = permute(RTil(:,:,1:n/2,:),[2 3 1 4]); % z i j t --> i j z t
RTilRight = permute(RTil(:,:,n/2+1:end,:),[2 3 1 4]);

RTilNum = reshape(...
	RzetaNumeric - numeric.trapz_fast_nDim(zMat,RzKNumeric,1,'equal','ignoreNaN')... 1 zeta i j t
	,ndiscPlant,n/2,n,numtPlant);
RTilLeftNum = permute(RTilNum(:,:,1:n/2,:),[2 3 1 4]); % z i j t --> i j z t
RTilRightNum = permute(RTilNum(:,:,n/2+1:end,:),[2 3 1 4]);

% RNumeric = permute(misc.multArray(Bn,dzK1Plant,2,2),[1 3 2 4])...    i j z t
% 	+ permute(misc.multArray(Bd_til,squeeze(KPlant(end,:,:,:,:)),2,2),[1 3 2 4]);
% todo: numeric!


%					% i z j t							 
% Kbar = misc.multArray(Bn,dzK_1_zetaPlant,2,2)...   
% 	+ misc.multArray(Bd_til,squeeze(KPlant(end,:,:,:,:)),2,2);

% Collapse second and third dimension. That is: first all zeta for j=1 then all zeta for j=2 and so on.                               
% KbarResh = reshape(Kbar,n,[],numTKernel);
% r2 = KbarResh;
% create diagonal matrix of MMs
% MMtot = kron(eye(n), massMatrix);
% r2 = permute(misc.multArray(KbarResh,MMtot,2,1),[1 3 2]); % i, (jzeta), t	   
	
% default values 0 for not yet calculated things
% controllerAnalytical = zeros(n, n/2*ndisc,numtKernel);
controllerBotTemp = cat(2,RTilLeft,RTilRight);
controllerRTemp = [zeros(n/2,size(controllerBotTemp,2),size(controllerBotTemp,3),size(controllerBotTemp,4));controllerBotTemp];%/(1-z0(1)) fehlt aber nur bei Neumann-Eingriff!

controllerAnalytical = controllerRTemp(sortBack,:,:,:);
distributedController = zeros(n, n/2*ndisc,numtKernel);

% apply unfolding, and apply mass matrix for the integration
z0 = ppide.ctrl_pars.foldingPoint;
if isscalar(z0)
	z0 = repmat(z0,1,n/2);
end
z0Left = zeros(n/2,1);
zRight = zeros(n/2,ppide.ndiscPars);
for i=1:n/2
	zRight(i,:) = linspace(z0(i),1,ppide.ndiscPars);
	dZ = diff(zRight(i,:));
	z0Left(i) = z0(i)-dZ(1);
end

% n rows for the n inputs. The first n/2 inputs are the left inputs and the second are the
% right ones.
controllerTemp  = zeros(n/2,n/2*ndiscPlant,numtPlant);
controllerFoldedTemp  = zeros(n/2,n*ndiscPlant,numtPlant);
controllerNumericTemp  = zeros(n/2,n/2*ndiscPlant,numtPlant);
controller = zeros(n/2,n/2*ndiscPlant,numtPlant);
controllerFolded = zeros(n/2,n*ndiscPlant,numtPlant);
controllerNumeric = zeros(n/2, n/2*ndiscPlant, numtPlant);
for j=1:n/2
	z0iIdx = find(zdiscPlant<z0(j),1,'last');
	
	% the left part of the spatial domain of x belongs to w1
	% xi(0:z0i) = w1i(1:0)
	controllerTemp(:,(j-1)*ndiscPlant+1:(j-1)*ndiscPlant+z0iIdx,:) = ...
				1/z0Left(j)*permute(interp1(linspace(zdiscPlant(z0iIdx),0,ndiscPlant),permute(RTilLeft(:,j,:,:),[3 1 4 2]),zdiscPlant(1:z0iIdx)),...
				[2 1 3]);
	controllerNumericTemp(:,(j-1)*ndiscPlant+1:(j-1)*ndiscPlant+z0iIdx,:) = ...
				1/z0Left(j)*permute(interp1(linspace(zdiscPlant(z0iIdx),0,ndiscPlant),permute(RTilLeftNum(:,j,:,:),[3 1 4 2]),zdiscPlant(1:z0iIdx)),...
				[2 1 3]);
% 	controllerNumericTemp(:,(j-1)*ndiscPlant+1:(j-1)*ndiscPlant+z0iIdx,:) = ...
% 		1/z0Left(j)*permute(interp1(linspace(zdiscPlant(z0iIdx),0,ndiscPlant),permute(RNumeric(:,j,:,:),[3 1 4 2]),zdiscPlant(1:z0iIdx)),...
% 		[2 1 3]);
	% xi(z0i:1) = w2i(0:1);
	controllerTemp(:,(j-1)*ndiscPlant+z0iIdx+1:j*ndiscPlant,:) = ...
				1/(1-z0(j))*permute(interp1(linspace(zdiscPlant(z0iIdx+1),1,ndiscPlant),permute(RTilRight(:,j,:,:),[3 1 4 2]),zdiscPlant(z0iIdx+1:end)),...
				[2 1 3]);
	controllerNumericTemp(:,(j-1)*ndiscPlant+z0iIdx+1:j*ndiscPlant,:) = ...
				1/(1-z0(j))*permute(interp1(linspace(zdiscPlant(z0iIdx+1),1,ndiscPlant),permute(RTilRightNum(:,j,:,:),[3 1 4 2]),zdiscPlant(z0iIdx+1:end)),...
				[2 1 3]);
% 	controllerNumericTemp(:,(j-1)*ndiscPlant+z0iIdx+1:j*ndiscPlant,:) = ...
% 				1/(1-z0(j))*permute(interp1(linspace(zdiscPlant(z0iIdx+1),1,ndiscPlant),permute(RNumeric(:,j+n/2,:,:),[3 1 4 2]),zdiscPlant(z0iIdx+1:end)),...
% 				[2 1 3]);
	% the mass matrix can only be applied when the controller is created over the whole spatial
	% domain. 
	% If the controller has a 3rd (time) domain, it needs to be permuted after the
	% multiplication.
	if size(controller,3)>1
		controller(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
			permute(...
				misc.multArray(controllerTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1)...
			,[1 3 2]);
		controllerNumeric(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
			permute(...
				misc.multArray(controllerNumericTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1)...
			,[1 3 2]);
	else
		controller(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
				misc.multArray(controllerTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1);
		controllerNumeric(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
				misc.multArray(controllerNumericTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1);
	end
end
% add 0 controller for left inputs.
controllerR = [zeros(n/2,size(controller,2),size(controller,3));controller];%/(1-z0(1)) fehlt aber nur bei Neumann-Eingriff!
controllerNumericR = [zeros(n/2,size(controllerNumeric,2),size(controllerNumeric,3));controllerNumeric];

controller = controllerR(sortBack,:,:);

for j=1:n
	controllerFoldedTemp(:,(j-1)*ndiscPlant+1:(j)*ndiscPlant,:) = ...
				permute(controllerBotTemp(:,j,:,:),[1 3 4 2]);
	% the mass matrix can only be applied when the controller is created over the whole spatial
	% domain. 
	% If the controller has a 3rd (time) domain, it needs to be permuted after the
	% multiplication.
	if size(controller,3)>1
		controllerFolded(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
			permute(...
				misc.multArray(controllerFoldedTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1)...
			,[1 3 2]);
	else
		controllerFolded(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:) = ...
				misc.multArray(controllerFoldedTemp(:,(j-1)*ndiscPlant+1:j*ndiscPlant,:),...
				massMatrix,2,1);
	end
end
% add 0 controller for left inputs.
controllerFoldedR = [zeros(n/2,size(controllerFolded,2),size(controllerFolded,3));controllerFolded];%/(1-z0(1)) fehlt aber nur bei Neumann-Eingriff!
controllerFolded = controllerFoldedR(sortBack,:,:);


BnOrig = Bn(sortBack,sortBack);
for i=1:n
	if BnOrig(i,i)~= 0
		if i<=n/2 % left inputs
			controller(i,:,:) = controller(i,:,:)/z0Left(1);
		else
			controller(i,:,:) = controller(i,:,:)/(1-z0(1));
		end
	end
end
controllerNumeric = controllerNumericR(sortBack,:,:);

BnUnsorted = Bn(sortBack,sortBack);

% sign of first states actuation needs to be inverted sice boundary matrices did not respect
% the sign when folding!
for i=1:n/2
	if BnUnsorted(i,i) ~= 0
		controller(i,:,:) = -controller(i,:,:);
		controllerNumeric(i,:,:) = -controllerNumeric(i,:,:);
	end
end


% debug
if debug
	figure('Name', 'Numerisches und analytisches dzK(1, z)')
	id = 1;
	for it = 1:n/2
		for jt = 1:n
			subplot(n/2, n, id)
			hold on
			grid on
			if size(decouplingKernel.value{it,jt}.dz_K_1_zeta,2)>1
				numplots = 3;
				idxDiff = round(size(decouplingKernel.value{it,jt}.dz_K_1_zeta,2)/numplots);
				for tIdx=1:idxDiff:size(decouplingKernel.value{it,jt}.dz_K_1_zeta,2)
					plot(zdisc,decouplingKernel.value{it,jt}.dz_K_1_zeta(:,tIdx),'displayName','analytisch')
					plot(zdisc,squeeze(dzK1(:,it,jt,tIdx)),'r','DisplayName','numerisch')
				end
			else
				plot(zdisc,decouplingKernel.value{it,jt}.dz_K_1_zeta(:),'displayName','analytisch')
				plot(zdisc,squeeze(dzK1(:,it,jt)),'r','DisplayName','numerisch')
			end
% 			plot(zdisc,kernel.value{it,jt}.dz_K_1_zeta(:,1),'displayName','analytisch')
% 			plot(zdisc,squeeze(dzK1(:,it,jt,1)),'r','DisplayName','numerisch')
% 			plot(zdisc,kernel.value{it,jt}.dz_K_1_zeta(:,end),'displayName','analytisch')
% 			plot(zdisc,squeeze(dzK1(:,it,jt,end)),'r','DisplayName','numerisch')
			legend('-dynamicLegend')
			id = id+1;
		end
	end
	figure('Name','Numerisches und analytisches dzetaK(z,0)')
	id =1;
	for it = 1:n/2
		for jt=1:n
			subplot(n/2,n,id)
			hold on
			grid on
			plot(zdisc,decouplingKernel.value{it,jt}.dzeta_K_z_0(:,1),'displayName','analytisch')
			plot(zdisc,squeeze(dzetaK0(:,it,jt,1)),'r','DisplayName','numerisch')
			plot(zdisc,decouplingKernel.value{it,jt}.dzeta_K_z_0(:,end),'displayName','analytisch')
			plot(zdisc,squeeze(dzetaK0(:,it,jt,end)),'r','DisplayName','numerisch')
			legend('-dynamicLegend')
			id = id+1;
		end
	end
end
time=toc;
fprintf('Finished! (%0.2fs)\n', time);

end

