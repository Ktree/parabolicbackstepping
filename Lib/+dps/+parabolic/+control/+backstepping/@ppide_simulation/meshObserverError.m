function varargout = meshObserverError(obj,varargin)
	if ~obj.useStateObserver
		warning('Cannot mesh observer error, observer is deactivated!')
		if nargout>0
			varargout{1} = obj;
		end
		return
	end
	if ~obj.simulated
		obj = obj.simulate();
	end
	
	found=0;
	passed = [];
	arglist = {'numpointsT','numpointsZ','zLim','tLim','plotPars','extraZ','extraT','maxDiffT','maxDiffTRel'};
	if ~isempty(varargin)
		if mod(length(varargin),2) % uneven number
			error('When passing additional arguments, you must pass them pairwise!')
		end
		for index = 1:2:length(varargin) % loop through all passed arguments:
			for arg = 1:length(arglist)
				if strcmp(varargin{index},arglist{arg})
					passed.(arglist{arg}) = varargin{index+1};
					found=1;
					break
				end 
			end % for arg
			% argument wasnt found in for-loop
			if ~found
				error([varargin{index} ' is not a valid property to pass to meshObserverError!']);
			end
			found=0; % reset found
		end % for index
	end 
	if ~isfield(passed,'extraZ')
		passed.extraZ = [];
	end
	if ~isfield(passed,'extraT')
		passed.extraT = [];
	end
	if ~isfield(passed,'maxDiffT')
		passed.maxDiffT = 0;
	end
	if ~isfield(passed,'maxDiffTRel')
		passed.maxDiffTRel = 0;
	end
	if ~isfield(passed,'numpointsT')
		if isfield(obj.plotPars,'plot_res_2D')
			passed.numpointsT = obj.plotPars.plot_res_2D;
		else
			passed.numpointsT = 21;
		end
	end
	if ~isfield(passed,'numpointsZ')
		if isfield(obj.plotPars,'plot_res_2D')
			passed.numpointsZ = obj.plotPars.plot_res_2D;
		else
			passed.numpointsZ = 21;
		end
	end
	if ~isfield(passed,'zLim')
		zPlot = linspace(0,1,passed.numpointsZ);
	else
		zPlot = linspace(passed.zLim(1),pssed.zLim(2),passed.numpointsZ);
	end
	if ~isfield(passed,'tLim')
		tPlotIni = linspace(0,1,passed.numpointsT);
	else
		tPlotIni = linspace(passed.tLim(1),passed.tLim(2),passed.numpointsT);
	end
	if ~isfield(passed,'plotPars')
		passed.plotPars = {};
	else
		if ~iscell(passed.plotPars) 
			error('plotPars must be passed as cell array of strings representing name-value-pairs!')
		end
	end
	zPlot = sort([zPlot passed.extraZ]);
	tPlot = sort([tPlotIni passed.extraT]);


	n=obj.ppide.n;
	tPlot1 = tPlot;
	for i=1:n
		figure('name',['xHat' num2str(i)])
		if passed.maxDiffT ~= 0
			needsRefinement =1;
			while needsRefinement
				xTemp = obj.xHatQuantity(i).on({tPlot1,zPlot},{'t','z'});
				xDiffT = abs(diff(xTemp,1,1));
				idxVec = zeros(size(xTemp,1),1);
				if passed.maxDiffTRel
					idxVec(max(xDiffT,[],2)>passed.maxDiffT*abs((max(xTemp(:))-min(xTemp(:))))) = 1;
				else
					idxVec(max(xDiffT,[],2)>passed.maxDiffT) = 1;
				end
				curIdx = 1;
				tAdd = [];
				needsRefinement = 0;
				for idx = 1:length(idxVec)
					if idxVec(idx) 
						tAdd(curIdx) = tPlot1(idx)+(tPlot1(idx+1)-tPlot1(idx))/2; % insert point in the middle
						curIdx = curIdx+1;
						needsRefinement =1;
					end
				end
				tPlot1 = sort([tPlot1 tAdd]);
			end
			xPlot = obj.xHatQuantity(i).on({tPlot1,zPlot},{'t','z'});
		else
			xPlot = obj.xHatQuantity(i).on({tPlot1,zPlot},{'t','z'});
		end
% 		mesh(t,zdisc_plant,y(:,(i-1)*ndisc_plant+1:i*ndisc_plant).')
		mesh(tPlot1,zPlot,xPlot.',passed.plotPars)
		hold on
		box on
		objT = obj.t(:); % column vector!
		shortT = objT(objT>=tPlot1(1)&objT<=tPlot1(end));
		cm_op = obj.ppide.cm_op;
		% messungen f Beobachter
		if any(obj.ppide.cm_op.c_m(:,i)) % Messung Punkt
			xFolding = obj.xHatQuantity(i).on({shortT,obj.ppide.cm_op.z_m},{'t','z'});
			plot3(shortT,obj.ppide.cm_op.z_m*ones(length(shortT),1),xFolding,'r','linewidth',2);
		end
		if any(cm_op.c_0(:,i)) % Messung links
			xLeft = subs(obj.xHatQuantity(i),'z',0);
			plot3(shortT,zeros(length(shortT),1),xLeft.on({shortT}),'r','linewidth',2);
		end
		if any(cm_op.c_1(:,i)) % Messung rechts
			xRight = subs(obj.xHatQuantity(i),'z',1);
			plot3(shortT,zeros(length(shortT),1),xRight.on({shortT}),'r','linewidth',2);
		end
		title(['$xHat_' num2str(i) '(z,t)$'])
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end
% 	for i=1:n
% 		res_x(:,(i-1)*ndisc_obs+1:i*ndisc_obs) = interp1(zdisc_plant,obj.x(:,(i-1)*ndisc_plant+1:i*ndisc_plant).',zdisc_obs(:)).';
% 	end
% 	err_x = res_x - obj.xHat;
	tPlot2 = tPlot;
	for i=1:n
		err_x = obj.xQuantity(i) - obj.xHatQuantity(i);
		figure('name',['ex' num2str(i)])
		if passed.maxDiffT ~= 0
			needsRefinement =1;
			while needsRefinement
				xTemp = err_x.on({tPlot2,zPlot},{'t','z'});
				xDiffT = abs(diff(xTemp,1,1));
				idxVec = zeros(size(xTemp,1),1);
				if passed.maxDiffTRel
					idxVec(max(xDiffT,[],2)>passed.maxDiffT*abs((max(xTemp(:))-min(xTemp(:))))) = 1;
				else
					idxVec(max(xDiffT,[],2)>passed.maxDiffT) = 1;
				end
				idxVec(max(xDiffT,[],2)>passed.maxDiffT) = 1;
				curIdx = 1;
				tAdd = [];
				needsRefinement = 0;
				for idx = 1:length(idxVec)
					if idxVec(idx) 
						tAdd(curIdx) = tPlot2(idx)+(tPlot2(idx+1)-tPlot2(idx))/2; % insert point in the middle
						curIdx = curIdx+1;
						needsRefinement =1;
					end
				end
				tPlot2 = sort([tPlot2 tAdd]);
			end
			xPlot = err_x.on({tPlot2,zPlot},{'t','z'});
		else
			xPlot = err_x.on({tPlot2,zPlot},{'t','z'});
		end
% 		mesh(t,zdisc_plant,y(:,(i-1)*ndisc_plant+1:i*ndisc_plant).')
% 		misc.mesh_sparse(obj.t,zdisc_obs,err_x(:,(i-1)*ndisc_obs+1:i*ndisc_obs).',20,plot_pars.plot_res_2D_num_lines,plot_pars.plot_res_2D_lines,plot_pars.plot_res_2D_lines);
		mesh(tPlot2,zPlot,xPlot.',passed.plotPars)
		hold on
		box on
		objT = obj.t(:); % column vector!
		shortT = objT(objT>=tPlot2(1)&objT<=tPlot2(end));
		cm_op = obj.ppide.cm_op;
		% messungen f Beobachter
		if any(obj.ppide.cm_op.c_m(:,i)) % Messung Punkt
			xFolding = subs(err_x,'z',obj.ppide.cm_op.z_m);
			plot3(shortT,obj.ppide.cm_op.z_m*ones(length(shortT),1),xFolding.on({shortT}),'r','linewidth',2);
		end
		if any(cm_op.c_0(:,i)) % Messung links
			xLeft = subs(err_x,'z',0);
			plot3(shortT,zeros(length(shortT),1),xLeft.on({shortT}),'r','linewidth',2);
		end
		if any(cm_op.c_1(:,i)) % Messung rechts
			xRight = subs(err_x,'z',1);
			plot3(shortT,zeros(length(shortT),1),xRight.on({shortT}),'r','linewidth',2);
		end
		title(['$ex_' num2str(i) '(z,t)$'])
		set(gca,'Ydir','reverse')
		xlabel('$t$')
		ylabel('$z$')
	end
	if nargout>0
		varargout{1} = obj;
	end
end