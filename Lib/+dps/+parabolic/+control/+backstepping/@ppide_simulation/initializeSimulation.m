function obj = initializeSimulation(obj)
% initializeSimulation initialize ppide_simulation to execute simulation
%   obj = initinitializeSimulation(obj) computes approximations, 
%   controllers and observers to be ready to perform a simulation.

% created on 05/2018 by Simon Kerschbaum

% packages:
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*
%   Approximation should be computed in constructor but make sure it is:
	if isempty(obj.ppideApproximation)
		obj.ppideApproximation = ppide_approximation(obj.ppide);
		obj.checkEigenvalues();
	end
	if isempty(obj.simPpideApproximation) && ~isempty(obj.simPpide)
		obj.simPpideApproximation = ppide_approximation(obj.simPpide);
	end
	% Otherwise resampling doesnt work in its current form:
	if length(obj.ppide.ctrl_pars.zdiscObs) > obj.ppide.ndisc
		error('Plant approximation resolution must always be greater than observer approximation resolution!')
	end

	% Compute controllerKernel. Internally checks if kernel was already computed
	% TODO: add default values so no controller needs to be computed if not used!
% 	if obj.useStateFeedback
	timer = tic;
	obj = obj.computeControllerKernel;
	time = toc(timer);
	disp(['elapsed Time: ' num2str(round(time))]);
	%
% 	error('Hier weiter. Hier muss decouplePpide erzeugt werden. Dann mit timing computeDecouplingKernel ausgeloest werden!')
	
	if size(obj.ppide.b_1,2) == 2*size(obj.ppide.l,2) && obj.useDecouplingBilateral
		% 1. create ppide for decoupling
		obj.decouplingPpide = obj.designPpide;
		% check that diffusion coefficients are sorted correctly. If not, resort them:
		n = size(obj.ppide.l,2);
		lambda1 = reshape(obj.decouplingPpide.l(1,:,:),2*n,2*n);
		lambda1Vec = diag(lambda1);
		[~,sortVec] = sort(lambda1Vec,'descend');
		obj.sortVec = sortVec;
		unsorted = 1:length(sortVec);
		sortBack(sortVec) = unsorted;
		obj.sortBack = sortBack;
% 		zdiscPlant = linspace(0,1,size(obj.decouplingPpide.l,1));
% 		lInterp = numeric.interpolant({zdiscPlant,1:2*n,1:2*n},obj.decouplingPpide.l);
% 		lambdaSorted = reshape(...
% 			lInterp.eval(...
% 				repmat(zdiscPlant(:),numel(sortVec),1),...
% 				reshape(repmat(sortVec,numel(zdiscPlant),1),[],1),...
% 				reshape(repmat(sortVec,numel(zdiscPlant),1),[],1))...
% 		,numel(zdiscPlant),2*n,2*n);
		lambdaSorted = obj.decouplingPpide.l(:,sortVec,sortVec);
		lDiffSorted = obj.decouplingPpide.l_diff(:,sortVec,sortVec);
		lDiff2Sorted = obj.decouplingPpide.l_diff2(:,sortVec,sortVec);
		obj.decouplingPpide.l = lambdaSorted;
		obj.decouplingPpide.l_diff = lDiffSorted;
		obj.decouplingPpide.l_diff2 = lDiff2Sorted;
		
		BnOrigin = obj.decouplingPpide.b_op2.b_n;
% 		Bd = obj.decouplingPpide.b_op2.b_d;
		for i=1:2*n
			if BnOrigin(i,i) == 0
				Bd(i,i) = 1;
				Bn(i,i) = 0;
			else
				Bd(i,i) = 0;
				Bn(i,i) = 1;
			end
		end
		BdSorted = Bd(sortVec,sortVec);
		BnSorted = Bn(sortVec,sortVec);
		obj.decouplingPpide.b_op2.b_n = BnSorted;
		obj.decouplingPpide.b_op2.b_d = BdSorted;
		
		
		A0Temp = obj.controllerKernel.a0_til(:,sortVec,sortVec,:);
		A1Temp = obj.controllerKernel.a0_neu(:,sortVec,sortVec,:);
		A1DiffTemp = obj.controllerKernel.a0_neuDiff(:,sortVec,sortVec,:);
		
		
		z0 = obj.ppide.ctrl_pars.foldingPoint;
		% z0Til = z0/(1-z0);
		z0TilLeft = z0/(1-z0);
		z0TilVec = repmat(z0TilLeft,n,1);
		invZ0Til = (1-z0)/z0;
		invZ0TilVec = repmat(invZ0Til,n,1);
		z0TilGes = [z0TilVec invZ0TilVec];
		z0TilSorted = z0TilGes(sortVec);
		z0Til = z0TilSorted(1:n);
		A0 = zeros(size(A0Temp));
		A1 = zeros(size(A1Temp));
		A1Diff = zeros(size(A1DiffTemp));
		for i = 1:2*n
			for j=1:n
				A0(:,i,j,:) =A0Temp(:,i,j,:) + A0Temp(:,i,j+n,:);
				A1(:,i,j,:) = A1Temp(:,i,j,:) - 1/z0Til(j)*A1Temp(:,i,j+n,:);
				A1Diff(:,i,j,:) = A1DiffTemp(:,i,j,:) - 1/z0Til(j)*A1DiffTemp(:,i,j+n,:);
			end
		end
		obj.decouplingPpide.a_0 = A0;
		obj.decouplingPpide.c_0 = A1;
		obj.A1Diff = A1Diff;
		
		obj = obj.computeDecouplingKernel;
	end

% 	end
	if ~obj.controllerFlag %&& obj.useStateFeedback
		if ~obj.useStateFeedbackForInputDelay
			if ~isempty(obj.designPpide)
				obj.controller = ppide_controller(obj.controllerKernel, obj.designPpide, obj.debug);
				if obj.useDecouplingBilateral
					obj.decouplingController = ppide_controller(obj.decouplingKernel, obj.decouplingPpide, obj.debug,obj.controllerKernel.get_K,sortBack);
				end
			else
				obj.controller = ppide_controller(obj.controllerKernel, obj.ppide, obj.debug);
			end
			obj.controllerFlag = 1;
			obj.simulated  = 0;
			obj.simulatedTarget = 0;
		else
			obj.controller = ControllerForInputDelay(obj.controllerKernel, obj.ppide, obj.debug);
			obj.controllerFlag = 1;
			obj.controllerForInputDelayFlag = 1;
		end
	end
	
	% Calculate reference and disturbance feedforward control
	if ~obj.KvFlag
		[obj.Pi, obj.Kv, obj.Piz, obj.Pizz, obj.D] = dps.parabolic.control.backstepping.regulator_equations(obj.ppide, obj.sM, obj.controllerKernel);
		obj.KvFlag = 1;
	end
	if ~obj.KvHatFlag 
		if obj.useIntMod
			% Attention: There is no check, whether the observer configuration
			% fits to the internal model! Always assumes that Output is
			% measured!
			[obj.KvHat, obj.KvQPlant, obj.Q, obj.QTil, obj.DRobust] = robustDecouplingEquations(obj.ppide, obj.sM, obj.controllerKernel);
			obj.KvHatFlag = 1;
		else
			p = size(obj.ppide.b_2,2);
			obj.KvHat = zeros(p, obj.ppide.n*obj.sM.nv); 		
		end
	end
	
	% Compute observerKernel. Internally checks if kernel was already computed
	if obj.useStateObserver || obj.useDisturbanceObserver
		obj = storeObserverKernel(obj);

		% Compute observer gains and approximation
		if ~obj.observerFlag 
			if ~obj.GammaFlag
				obj.Gamma = dps.parabolic.control.backstepping.decoupling_equations(obj.ppide, obj.sM, obj.observerKernel);
				% compare solution with solution gained with generic BVP
				% Solution
	% 			import misc.*
	% 			zdiscPlant = linspace(0, 1, obj.ppide.ndisc);
	% 			zdisc = obj.ppide.ctrl_pars.zdiscObsKernel;
	% 			ndisc = length(zdisc);
	% 			n = obj.ppide.n;
	% 			l_disc = interp1(zdiscPlant, obj.ppide.l_disc, zdisc(:));
	% 			M = -obj.ppide.ctrl_pars.mu_o;
	% 			S = -obj.sM.Sd;
	% 			A0 = zeros(length(zdisc), n, n);
	% 			nv = obj.sM.nd;
	% 			G2 = obj.ppide.g_2; % dim: n x nd % left disturbance input 
	% 			G3 = obj.ppide.g_3; % dim: n x nd % right disturbance input 
	% 			nd = size(G2, 2); % length of disturbance vector
	% 			% calculate H_bar
	% 			G2_bar = G2*obj.sM.pd_til;
	% 			G3_bar = G3*obj.sM.pd_til;
	% 
	% 			p = obj.observerKernel.KI;
	% 			g1_disc = eval_pointwise(obj.ppide.g_1, zdisc);
	% 			ToG1 = permute(Tc(permute(g1_disc, [3 1 2]), -p, zdisc, zdisc), [2 3 1]);
	% 			H1_bar = zeros(ndisc, n, nd);
	% 			H = zeros(ndisc, n, nv);
	% 			for z_idx = 1:ndisc
	% 				H1_bar(z_idx, 1:n, 1:nd) = -squeeze(p(z_idx, 1, :, :))*squeeze(l_disc(1, :, :))...
	% 									 *G2 + sd(ToG1(z_idx, :, :));
	% 				H(z_idx, :, :) = -reshape(H1_bar(z_idx, :, :), [n nd])*obj.sM.pd_til;
	% 			end
	% 			
	% 			% define left boundary operator as output operator:
	% 			% theta_o = B1:
	% 			Bn0 = obj.ppide.b_op1.b_n;
	% 			Bd0 = obj.ppide.b_op1.b_d;
	% 			Bn1 = obj.ppide.b_op2.b_n;
	% 			Bd1 = obj.ppide.b_op2.b_d;
	% 			B1 = dps.output_operator('c_0', Bd0, 'c_d0', Bn0);
	% 			
	% 			% distributed output cannot be passed discretized at the
	% 			% moment!
	% 			A0_bar = obj.observerKernel.a0_bar;
	% 			a0_gen = @(z) reshape(interp1(zdisc, A0_bar, z), [n n]);
	% 			B2 = dps.output_operator('c_1', Bd1, 'c_d1', Bn1, 'c_c', a0_gen);
	% 			Gamma_comp = misc.solveGenericBVP(l_disc, M, S, A0, H, B1, G2_bar, B2, G3_bar, zdisc);

				obj.GammaFlag = 1;
	% 			for i = 1:n
	% 				for j = 1:n
	% 					figure('Name', [num2str(i) num2str(j)])
	% 					hold on
	% 					plot(zdisc, Gamma_comp(:, i, j))
	% 					plot(zdisc, obj.Gamma(:, i, j))
	% 				end
	% 			end
			end
			
			if ~obj.useStateObserverForOutputDelay
				% in domain-measurement: folding
				n = obj.ppide.n;
				if any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:)) % folding
					if ~isequal(obj.ppide.cm_op.c_m,[zeros(n,n);eye(n)]) || ~isequal(obj.ppide.cm_op.c_dm,[eye(n);zeros(n,n)])
						error('When the measurement is in-domain, both the state and the derivative must be measured.')
					end
					obj.observer1 = dps.parabolic.control.backstepping.ppide_observer(...
									obj.observerKernel1, obj.observerPpide1, obj.Gamma, obj.sM);
					obj.observer2 = dps.parabolic.control.backstepping.ppide_observer(...
									obj.observerKernel2, obj.observerPpide2, obj.Gamma, obj.sM);
					obj.observerPpide1.ndisc = length(obj.ppide.ctrl_pars.zdiscObs);
					obj.observerPpide2.ndisc = length(obj.ppide.ctrl_pars.zdiscObs);
					obj.observerApproximation1 = ppide_approximation(obj.observerPpide1);
					obj.observerApproximation2 = ppide_approximation(obj.observerPpide2);
					
				else
					obj.observer = dps.parabolic.control.backstepping.ppide_observer(...
									obj.observerKernel, obj.ppide, obj.Gamma, obj.sM);
					obj.observerApproximation = ppide_approximation(obj.observer.ppide);
				end
			else
				obj.observer = dps.parabolic.control.backstepping.ObserverForOutputDelay(...
								obj.observerKernel, obj.ppide, obj.Gamma, obj.sM);
				obj.observerApproximation = ppide_approximation(obj.observer.ppide);
				obj.observerForOutputDelayFlag = 1;
			end
			obj.observerFlag = 1;
			obj.simulated = 0;
		end
		if isempty(obj.observerApproximation) && ~(any(obj.ppide.cm_op.c_m(:))  || any(obj.ppide.cm_op.c_dm(:)))
			% only if not folding
			obj.observerApproximation = ppide_approximation(obj.observer.ppide);
		end
	end
end

