function obj = computeDecouplingKernel(obj)
	% Compute the kernel for the decoupling transformation of the state feedback controller.
	% in the bilateral case.
	%   obj = computeDecouplingKernel(obj)
	if ~obj.decouplingKernelFlag
		import dps.parabolic.control.backstepping.*
		fprintf('Computing decoupling kernel...\n')
		ct = tic;
		if ~isempty(obj.decouplingPpide)
			use_F_handle = @(kernel) use_F_ppidesDecoupling(kernel,obj.decouplingPpide,obj.sortVec,obj.sortBack);
			decouplingKern = DecouplingKernel(obj.decouplingPpide,obj.decouplingPpide.ctrl_pars.zdiscCtrl,obj.A1Diff,obj.sortVec);
		else
			error('Before decoupling, the backstepping transformation needs to be performed and the decouplingPpide must store the resulting coupling matrix in the correct format.')
		end
		
		[obj.decouplingKernel,~,avgTime] = dps.parabolic.tool.fixpoint_iteration(decouplingKern,use_F_handle,obj.ppide.ctrl_pars.it_min,obj.ppide.ctrl_pars.it_max,obj.ppide.ctrl_pars.tol);
		obj.decouplingKernel.avgTime =avgTime;
		
		obj.decouplingKernel = obj.decouplingKernel.transform_kernel(obj.decouplingPpide,obj.sortVec);

		% TODO!!
% 		obj.controllerKernel= check_pde(obj.controllerKernel,obj.ppide);
% 		obj.controllerKernel = check_ppde_boundaries(obj.controllerKernel,obj.ppide);
		fprintf('finished decoupling kernel. (%0.2fs)\n',toc(ct));
		obj.decouplingKernelFlag=1;
		obj.controllerFlag = 0; 
	end
end