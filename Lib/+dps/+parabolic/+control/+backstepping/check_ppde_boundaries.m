function kernel = check_ppde_boundaries ( kernel,ppde )
%CHECK_PDE_BOUNDARIES Check whether the ppde_kernel fulfills the boundary
%conditions (numerically)

% Created on 23.09.2016 by Simon Kerschbaum
% modified on 27.04.2017 by SK:
% - implementation for ppides
import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
if isa(kernel,'ppde_kernel') % old implementation, not yet improved!
	n = size(kernel.value,1);
	zdisc = kernel.zdisc;
	ndisc = length(zdisc);
	Bd = -ppde.b_op1.b_d; %Q Verkopplung
	for i=1:n
		for j=1:n
			grad_op = get_grad_op(zdisc);
			muij = ppde.ctrl_pars.mu(i,j);
			if i==j
				for z_idx = 1:ndisc
					if z_idx >1
						grad_op_zeta = get_grad_op(zdisc(1:z_idx));
						dzetaK = grad_op_zeta*kernel.value{i,j}.K(z_idx,1:z_idx).';
						dzetaK0 = dzetaK(1);
						dzetaKz(z_idx,1) = dzetaK0;					
					else
						dzetaKz(z_idx,1)=0;
					end
					errb3(z_idx) = 0;
					azetaij = 0;
					for zeta_idx = 1:z_idx
						azeta = ppde.a(zdisc(zeta_idx));
						azetaij(zeta_idx) = azeta(i,j);
					end

					errb2(z_idx) = kernel.value{i,j}.K(z_idx,z_idx)...
								   - Bd(i,i) + trapz_fast(zdisc(1:z_idx),...
											  (muij+azetaij)/2/ppde.l(i,i)); 
				end
				Ksum = 0;
					for k=1:n
						Ksum = Ksum + kernel.value{i,k}.K(:,1)*ppde.l(k,k)*...
									  Bd(k,j);
					end	
				errb1= dzetaKz(:,1) - 1/ppde.l(j,j)*Ksum;		
			elseif i<j
				for z_idx = 1:ndisc
					if z_idx >1
						grad_op_zeta = get_grad_op(zdisc(1:z_idx));
						dzetaK = grad_op_zeta*kernel.value{i,j}.K(z_idx,1:z_idx).';
						dzetaK0 = dzetaK(1);
						dzetaKz(z_idx,1) = dzetaK0;					
					else
						dzetaKz(z_idx,1)=0;
					end

					z_vec = zdisc(z_idx:end);
					grad_op_z = get_grad_op(z_vec);
					dzK = grad_op_z * kernel.value{i,j}.K(z_idx:end,z_idx);
					az = ppde.a(zdisc(z_idx));
					azij = az(i,j);
					errb3(z_idx) = dzK(1) - azij/(ppde.l(j,j)-ppde.l(i,i));

					errb2(z_idx) = kernel.value{i,j}.K(z_idx,z_idx); 
				end
				% letzte Ableitung kann nicht numerisch berechnet werden!
				errb3(end) = 0;
				Ksum = 0;
					for k=1:n
						Ksum = Ksum + kernel.value{i,k}.K(:,1)*ppde.l(k,k)*...
									  Bd(k,j);
					end	
				errb1 = dzetaKz(:,1) - 1/ppde.l(j,j)*Ksum;
			else
				for z_idx = 1:ndisc
					if z_idx >1
						grad_op_zeta = get_grad_op(zdisc(1:z_idx));
						dzetaK = grad_op_zeta*kernel.value{i,j}.K(z_idx,1:z_idx).';
						dzetaK0 = dzetaK(1);
						dzetaKz(z_idx,1) = dzetaK0;					
					else
						dzetaKz(z_idx,1)=0;
					end

					z_vec = zdisc(z_idx:end);
					grad_op_z = get_grad_op(z_vec);
					dzK = grad_op_z * kernel.value{i,j}.K(z_idx:end,z_idx);
					az = ppde.a(zdisc(z_idx));
					azij = az(i,j);
					errb3(z_idx) = dzK(1) - azij/(ppde.l(j,j)-ppde.l(i,i));
					errb2(z_idx) = kernel.value{i,j}.K(z_idx,z_idx); 
				end
				% letzte Ableitung kann nicht numerisch berechnet werden!
				errb3(end) = 0;
				Ksum = 0;
					for k=1:n
						Ksum = Ksum + kernel.value{i,k}.K(:,1)*ppde.l(k,k)*...
									  Bd(k,j);
					end	
				errb1 = zeros(ndisc,1);
			end		
			kernel.value{i,j}.errb{1} = errb1;
			kernel.value{i,j}.errb{2} = errb2;
			kernel.value{i,j}.errb{3} = errb3;	
		end
	end
elseif isa(kernel,'ppide_kernel') % new implementation
	if kernel.checked_boundaries ==0
		n = size(kernel.value,1); 
		numtKernel = size(kernel.value{1,1}.G,3);
		numTPlant = ppde.ndiscTime;
		tDiscPlant = linspace(0,1,numTPlant);
		tDisc = linspace(0,1,numtKernel);
		zdisc = kernel.zdisc;
		ndisc = length(zdisc);
		zdisc_plant=linspace(0,1,ppde.ndiscPars);
		if isa(ppde.b_op1.b_d,'function_handle')
			Bd = permute(misc.eval_pointwise(ppde.b_op1.b_d,linspace(0,1,numtKernel)),[2 3 1]);
		else
			Bd = repmat(ppde.b_op1.b_d,1,1,numtKernel);
		end
		Bn = ppde.b_op1.b_n;
		a0Interp = numeric.interpolant({zdisc_plant,1:n,1:n,tDiscPlant},ppde.a_0_disc);
		aInterp = numeric.interpolant({zdisc_plant,1:n,1:n,tDiscPlant},ppde.a_disc);
		cInterp = numeric.interpolant({zdisc_plant,1:n,1:n,tDiscPlant},ppde.c);
		a0_disc = a0Interp.evaluate(zdisc,1:n,1:n,tDisc);
		a_disc = aInterp.evaluate(zdisc,1:n,1:n,tDisc);
		c_disc = cInterp.evaluate(zdisc,1:n,1:n,tDisc);
		if size(c_disc,4)<2
			c_disc = repmat(c_disc,1,1,1,numtKernel);
		end
		if size(a_disc,4)<2
			a_disc = repmat(a_disc,1,1,1,numtKernel);
		end
		if size(a0_disc,4)<2
			a0_disc = repmat(a0_disc,1,1,1,numtKernel);
		end
		
		% Hartwig edited
% 		if ndisc~= n
% 			if isa(ppde.ctrl_pars.mu,'function_handle') % function
% 				muHelp = eval_pointwise(ppde.ctrl_pars.mu,zdisc);
% 			elseif isequal(size(ppde.ctrl_pars.mu,1),ndisc) % is discretized 
% 				muHelp = ppde.ctrl_pars.mu;
% 			elseif isequal(size(ppde.ctrl_pars.mu,1),n) % constant matrix, two dimensions
% 				muHelp(1:ndisc,:,:) = repmat(shiftdim(ppde.ctrl_pars.mu,-1),ndisc,1,1);
% 			else % discretized matrix
% 				error('dimensions of mu_o not correct!')
% 			end
% 		else
% 			error('ndisc=n is a very bad choice!')
% 		end
		if ndisc~= n
			if isa(ppde.ctrl_pars.mu,'function_handle') % function
				if nargin(ppde.ctrl_pars.mu)==1
					muHelp = eval_pointwise(ppde.ctrl_pars.mu,zdisc);
				else
					muHelp = zeros(ndisc,n,n,length(tDisc));
					for z_idx=1:ndisc
						muHelp(z_idx,:,:,:) = permute((eval_pointwise(@(z)ppde.ctrl_pars.mu(zdisc(z_idx),z),tDisc)),[2 3 1]);
					end
				end
			elseif isequal(size(ppde.ctrl_pars.mu,1),length(ppde.ctrl_pars.zdiscCtrl)) % is discretized
				if length(size(ppde.ctrl_pars.mu))>3
					muInterpolant = interpolant({ppde.ctrl_pars.zdiscCtrl,1:n,1:n,ppde.ctrl_pars.tDiscKernel}, ppde.ctrl_pars.mu);
					muHelp = muInterpolant.evaluate(zdisc,1:n,1:n,tDisc);
				else
					muInterpolant = interpolant({ppde.ctrl_pars.zdiscCtrl,1:n,1:n}, ppde.ctrl_pars.mu);
					muHelp = muInterpolant.evaluate(zdisc,1:n,1:n);
				end
				%muHelp = ppde.ctrl_pars.mu;
			elseif isequal(size(ppde.ctrl_pars.mu,1),n) % constant matrix, two dimensions
				muHelp(1:ndisc,:,:) = repmat(shiftdim(ppde.ctrl_pars.mu,-1),ndisc,1,1);
			else % discretized matrix
				error('dimensions of mu not correct!')
			end
		else
			error('ndisc=n is a very bad choice!')
		end
		mu = muHelp;
		
		mTN = ppde.ctrl_pars.makeTargetNeumann;
		for i=1:n
			for j=1:n
				kern_elem = kernel.value{i,j};
				muij = squeeze(mu(:,i,j,:));
				errb2 = zeros(ndisc,numtKernel);
% 				for tIdx=1:numT
					c_j = reshape(c_disc(:,j,j,:),[ndisc, numtKernel]);
					% get analytic dzetaK(z,0)
	% 				if ppde.ctrl_pars.eliminate_convection
						dzetaKz = kern_elem.dzeta_K_z_0;
	% 				else
% 						for z_idx = 2:length(zdisc)
% 							dzetaKzAll(z_idx,1:z_idx,:) = diff_num(zdisc(1:z_idx),kern_elem.K(z_idx,1:z_idx,:),2);
% 						end
% 						dzetaKz = reshape(dzetaKzAll(:,1,:),[ndisc, numT]);
	% 				end
					if kern_elem.lambda_i(1)==kern_elem.lambda_j(1)
						% errb1:  dzetaK(z,0) - RHS1 / K(z,0)
						% errb2: K(z,z) - RHS2
						errb3(1:ndisc,1:numtKernel) = 0; % only 2 BC for i==j
						rhs2(1:ndisc,1:numtKernel) = 0; % preallocate
						for z_idx = 1:ndisc
							% RHS for err2
							%             Q is taken into the target system!
							rhs2(z_idx,:) = - mTN*reshape(Bd(i,i,:),[1 numtKernel]).*sqrt(kern_elem.lambda_i(1,:)./kern_elem.lambda_i(z_idx,:))...
										   - trapz_fast_nDim(zdisc(1:z_idx).',...
													  (muij(z_idx,:)+reshape(a_disc(1:z_idx,i,j,:),[z_idx numtKernel]))/2./sqrt(kern_elem.lambda_i(z_idx,:))./sqrt(kern_elem.lambda_i(1:z_idx,:))); 
						end
						for tIdx=1:numtKernel
							if max(abs(rhs2(:,tIdx))) ~= 0
								%        K(z,z) = diag(K)!
								errb2(:,tIdx) = (mydiag(kernel.value{i,j}.K(:,:,tIdx),1:ndisc,1:ndisc)...
											   - rhs2(:,tIdx))/max(abs(rhs2(:,tIdx)));
							else
								errb2(:,tIdx) = zeros(ndisc,1);
							end
						end
						a0sum(1:ndisc,1:numtKernel) = 0;
						for z_idx = 1:ndisc
							for k=1:n
								a0sum(z_idx,:) = a0sum(z_idx,:) + trapz_fast_nDim(zdisc(1:z_idx).',...
									reshape(kernel.value{i,k}.K(z_idx,1:z_idx,:),z_idx,numtKernel).*reshape(a0_disc(1:z_idx,k,j,:),z_idx,[]));
							end
						end
						rhs1 = (reshape(Bd(j,j,:),[1 numtKernel])+(kern_elem.lambda_j_diff(1,:)-c_j(1,:))./kern_elem.lambda_j(1,:)).*reshape(kern_elem.K(:,1,:),ndisc,numtKernel)...
									-reshape(a0_disc(:,i,j,:),ndisc,[])./kern_elem.lambda_j(1,:) ...
									+ a0sum./kern_elem.lambda_j(1,:);
						if Bn(j,j) ~= 0 % Rob/Neu
							errb1 = zeros(ndisc,numtKernel);
							for tIdx=1:numtKernel
								if max(abs(rhs1(:,tIdx))) ~= 0
									errb1(:,tIdx) = (dzetaKz(:,tIdx)...
												   + rhs1(:,tIdx))/max(abs(rhs1(:,tIdx)));
								else
									errb1 = zeros(ndisc,numtKernel);
								end
							end
						else % Dir
							errb1 = reshape(kern_elem.K(:,1,:),ndisc,numtKernel); % K(z,0)
						end
					else % i~=j
						% TODO!!
						% errb1: dzetaK(z,0) - RHS1 if i<j else 0
						% errb2: K(z,z)
						% errb3: dzK(z,z) - RHS3
						dzK = kern_elem.dz_K;
						rhs3(1:ndisc,1:numtKernel) = 0; % preallocate
						dzK1 = reshape(mydiag(dzK,1:ndisc,1:ndisc),[ndisc, numtKernel]); 
						for z_idx = 1:ndisc
% 							dzK = reshape(numeric.diff_num(zdisc(z_idx:end),kernel.value{i,j}.K(z_idx:end,z_idx,:)),[ndisc-z_idx+1 numT]);
							rhs3(z_idx,:) = (reshape(a_disc(z_idx,i,j,:),[1 numtKernel])+muij(z_idx,:))./(kern_elem.lambda_i(z_idx,:)-kern_elem.lambda_j(z_idx,:));
% 							dzK1(z_idx,:) = dzK(1,:);
						end
						if max(max(abs(rhs3))) ~= 0
							errb3 = (dzK1+rhs3)./max(abs(rhs3));
						else
							errb3 = zeros(ndisc,numtKernel);
						end
						% last derivative cant be computed numerically, as vector
						% has length 0
						errb3(end,:) = 0;

						if max(abs(mydiag(kernel.value{i,j}.K,1:ndisc,1:ndisc))) ~= 0
							errb2 = mydiag(kernel.value{i,j}.K,1:ndisc,1:ndisc); 
						else
							errb2 = zeros(ndisc,numtKernel);
						end

						if kern_elem.lambda_i(1) > kern_elem.lambda_j(1)
							if Bn(j,j) ~= 0 % rob/neu
								% calculate errb1
								a0sum(1:ndisc,1:numtKernel) = 0; %preallocate
								for z_idx = 1:ndisc
									for k=1:n
										a0sum(z_idx,:) = a0sum(z_idx,:) + numeric.trapz_fast_nDim(zdisc(1:z_idx),...
											reshape(kernel.value{i,k}.K(z_idx,1:z_idx,:),[z_idx,numtKernel]).*reshape(a0_disc(1:z_idx,k,j,:),[z_idx,numtKernel]));
									end
								end
								%        Q is always here
								rhs1 = (reshape(Bd(j,j,:),[1 numtKernel])+(kern_elem.lambda_j_diff(1,:)-c_j(1,:))/kern_elem.lambda_j(1,:)).*reshape(kern_elem.K(:,1,:),[ndisc,numtKernel])...
											-reshape(a0_disc(:,i,j,:),[ndisc,numtKernel])./kern_elem.lambda_j(1,:) ...
											+ a0sum./kern_elem.lambda_j(1,:);
								if max(max(abs(rhs1))) ~= 0
									errb1 = (dzetaKz...
												   + rhs1)./max(abs(rhs1));
								else
									errb1 = zeros(ndisc,numtKernel);
								end
							else
								errb1 = reshape(kern_elem.K(:,1,:),[ndisc numtKernel]);
							end
						else % lambda_i < lambda_j
							errb1=zeros(ndisc,numtKernel); % no further BC for i>j!
						end
					end % i~= j		
					kernel.value{i,j}.errb{1} = errb1;
					kernel.value{i,j}.errb{2} = errb2;
					kernel.value{i,j}.errb{3} = errb3;	
% 				end
			end
		end
		kernel.checked_boundaries =1;
	else
		disp('kernel BC already checked.')
	end
else
	error('kernel must be of type ppde_kernel or ppide_kernel')
end


end

