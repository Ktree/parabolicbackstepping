function GH = getFixedPointStartDecoupling(obj,ppide,sortVec)
%getFixedPointStartFolding get starting values of fixed point iteration
%
% GH: cell array of kernel_elements

% created on 07.08.2019 by Simon Kerschbaum

% overtake old values
GH = obj.value;

n = ppide.n/2;
numtKernel = size(GH{1,1}.G,3);
if numtKernel > 1
	tDisc = obj.tDisc;
else
	tDisc = 1;
end
z0 = ppide.ctrl_pars.foldingPoint;
z0TilLeft = z0/(1-z0);
z0TilVec = repmat(z0TilLeft,n,1);
invZ0Til = (1-z0)/z0;
invZ0TilVec = repmat(invZ0Til,n,1);
z0TilGes = [z0TilVec invZ0TilVec];
z0TilSorted = z0TilGes(sortVec);
z0Til = z0TilSorted(1:n);

for i=1:n
	for j=1:2*n
		kem = obj.value{i,j};
		if size(GH{i,j}.xiMat_xi,3) == 1 && numtKernel > 1
			xiMat = repmat(GH{i,j}.xiMat_xi,1,1,numtKernel);
		else
			xiMat = GH{i,j}.xiMat_xi;
		end
		etaDisc = kem.eta;
		xiDisc = kem.xi;
		
		% H0 and G0
		Ji = zeros(size(xiMat));
		Jii = zeros(size(xiMat));
	
		% Aij(z(xi_kj,eta_kj)) = obj.A0zXiEta{i,k,j} for j<=n and 
		% Aij-n(z(xi_kj,eta_kj)) = obj.A0zXiEta{i,k,j} for j>n 
		A0lrXiEtaTemp = obj.A0lrzXiEta{i,i,j};
		A0lrXiEta = misc.writeToOutside(A0lrXiEtaTemp);
		A1lrXiEtaTemp = obj.A1lrzXiEta{i,i,j};
		A1lrXiEta = misc.writeToOutside(A1lrXiEtaTemp);
		A1lrDiffXiEtaTemp = obj.A1lrDiffzXiEta{i,i,j};
		A1lrDiffXiEta = misc.writeToOutside(A1lrDiffXiEtaTemp);
		lambdaZXiEtaTemp = obj.lambdaZ{i+n,i,j};
		lambdaZXiEta = misc.writeToOutside(lambdaZXiEtaTemp);
		
		A0lrXiEtaInterp = numeric.interpolant({xiDisc,etaDisc,1:numtKernel},A0lrXiEta);
		A1lrXiEtaInterp = numeric.interpolant({xiDisc,etaDisc,1:numtKernel},A1lrXiEta);
		A1lrDiffXiEtaInterp = numeric.interpolant({xiDisc,etaDisc,1:numtKernel},A1lrDiffXiEta);
		lambdaZXiEtaInterp = numeric.interpolant({xiDisc,etaDisc},lambdaZXiEta);
		A0lrEtaEta = reshape(...
			A0lrXiEtaInterp.eval(...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
			),...
			1,numel(etaDisc(etaDisc>=0)),numtKernel);
		A1lrEtaEta = reshape(...
			A1lrXiEtaInterp.eval(...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
			),...
			1,numel(etaDisc(etaDisc>=0)),numtKernel);
		A1lrDiffEtaEta = reshape(...
			A1lrDiffXiEtaInterp.eval(...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
			),...
			1,numel(etaDisc(etaDisc>=0)),numtKernel);
		lambdaZEtaEta = reshape(...
			lambdaZXiEtaInterp.eval(...
				etaDisc(etaDisc>=0),...
				etaDisc(etaDisc>=0)...
			),...
			1,numel(etaDisc(etaDisc>=0)));
		if j>n % volterra-Part
			facJii1 = -1/2*z0Til(j-n);
			facJii2 = 1/2;
 		else % now fredholm part. j<=n
			facJii1 = 1/2;
			facJii2 = 1/(2*z0Til(j));
		end
		
		
		if j<=n
			% index belonging to right
			jrIdx = j+n;
		else
			jrIdx = j;
		end
		if ppide.l(1,i+n,i+n) < ppide.l(1,jrIdx,jrIdx) % fredholm, WP BC
			Ji(:,etaDisc>=0,:) = repmat(...
					reshape(ppide.ctrl_pars.g_strich{i,j}(etaDisc(etaDisc>=0)),1,[],1)...
				,length(xiDisc),1,numtKernel);
		else
			Jii(:,etaDisc>=0,:) = zeros(size(xiMat(:,etaDisc>=0,:))) + ... for siz
				facJii1 * A1lrDiffEtaEta .* sqrt(lambdaZEtaEta) ...
				+ facJii2 * sqrt(ppide.l(1,jrIdx,jrIdx))* A0lrEtaEta;
		end
		
		GH{i,j}.G = zeros(size(xiMat));
		GH{i,j}.H = zeros(size(xiMat));
		GH{i,j}.J = Ji + Jii;
	
		GH{i,j}.G_t = zeros(size(xiMat));

		GH{i,j}.K = GH{i,j}.transform_kernel_element(1);
		GH{i,j}.K_t = numeric.diff_num(tDisc,GH{i,j}.K,3);

		% only in the end! Must be used for fixed-point iteration, but not for starting values!
	% 		GH{i,j}.J = GH{i,j}.J + GH{i,j}.s/4*numeric.cumtrapz_fast_nDim(xiMat,GH{i,j}.G_t,1,'equal','ignoreNaN');
	% 		GH{i,j}.H = GH{i,j}.H + GH{i,j}.s/4*numeric.cumtrapz_fast_nDim(etaMat,GH{i,j}.G_t,2,'equal','ignoreNaN');
		if any(isnan(GH{i,j}.G(:))) || any(isnan(GH{i,j}.G(:))) || any(isnan(GH{i,j}.J(:)))
			here=1;
		end
	end % for j 
end % for i

end % function

