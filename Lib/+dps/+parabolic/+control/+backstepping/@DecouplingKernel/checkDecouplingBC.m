function [err1, err2, errGeta1, errGeta2, test, a0TilCanon,devA0Til,A0TilEverywhere,err1All] = checkDecouplingBC(kernel,ppide)
%checkDecouplingBC check the coupling BC of the decoupling kernel equations in the canonical
%coordinates.

% 1.
% G_xi(eta,eta) - G_eta(eta,eta) = -z0Til (D_xi(eta,eta) - D_eta(eta,eta))
%      - sqrt(lambda_j^r(0)) A0 + int_0^z( sqrt(lambda_j^r(0)) / lambda_k^r(zeta)
%                                    * Gik A0kj ) dzeta
%                               + int_0^1( sqrt(lambda_j^r(0)) / lambda_k^l(zeta)
%                                    * Dik A0kj ) dzeta
%
% 2.
% D(eta,eta) = A1 - int_0^z 1/lambda_k^r(zeta) Gik A1kj - int_0^1 1/lambda_k^l(zeta)Dik A1kj dzeta

n = size(kernel.value,1);
numtKernel = size(kernel.value{1,1}.G,3);
zdisc = kernel.zdisc;
ndisc = numel(zdisc);
zetaMatLeft = repmat(zdisc(:).',ndisc,1,numtKernel); % quadratic area
zetaMatRight = zetaMatLeft;
zetaMatRight(zdisc(:)<zdisc(:).' & ones(1,1,numtKernel)) = NaN; % triangular area.
GInterp = cell(n,2*n);HInterp = cell(n,2*n);JInterp = cell(n,2*n);

z0 = ppide.ctrl_pars.foldingPoint;
z0Til = z0/(1-z0);

A0lrXiEta = kernel.A0lrzXiEta; % AileftInd(z(xi_kj,eta_kj)) = kernel.A0zXiEta{i,k,j}
A1lrXiEta = kernel.A1lrzXiEta;
A0lXiEta = kernel.A0lzXiEta; % AileftInd(z(xi_kj,eta_kj)) = kernel.A0zXiEta{i,k,j}
A1lXiEta = kernel.A1lzXiEta;
A1DiffXiEta = kernel.A1lrDiffzXiEta;

% dependend on zeta (for integration)
A0lr = kernel.a0lrZeta; %(zeta,i,j,t)
A1lr = kernel.a1lrZeta;
A0l = kernel.a0lZeta; %(zeta,i,j,t)
A1l = kernel.a1lZeta;
lambdaZ = kernel.lambdaZ;  % lambdaZ{k,i,j} = lambda_k(z(xi_ij,eta_ij))
Gzzeta = cell(n,2*n);Hzzeta = cell(n,2*n);Jzzeta = cell(n,2*n);
G = cell(n,2*n,n,2*n);H = cell(n,2*n,n,2*n);J = cell(n,2*n,n,2*n);
dzeta_K = kernel.get_K('dzeta_K');
K = kernel.get_K;
for i = 1:n
	for j=1:2*n
		GInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.G);
		JInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.J);
		HInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.H);
		
		xiMatZ = kernel.value{i,j}.xiMat; %xi,eta(z,zeta)
		etaMatZ = kernel.value{i,j}.etaMat;
		GijAll = GInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
		HijAll = HInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
		JijAll = JInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
		% used for integrations over z or zeta
		Gzzeta{i,j} = reshape(GijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
		Hzzeta{i,j} = reshape(HijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
		Jzzeta{i,j} = reshape(JijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
		for k=1:n			
			for l=1:2*n				
				% store interpolations of G
				xiMatXi = kernel.xiMatXi{k,l,i,j}; % xi_ij(xi_kl,eta_kl)
				etaMatXi = kernel.etaMatXi{k,l,i,j}; % eta_ij(xi_kl,eta_kl)
				% Important! Do not evaluate Interpolant with a grid! This creates far too many
				% data points!
				GAll = GInterp{i,j}.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
				% G_ij(xi_kl,eta_kl)
				G{i,j,k,l} = reshape(GAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
				HAll = HInterp{i,j}.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
% 				% G_ij(xi_kl,eta_kl)
				H{i,j,k,l} = reshape(HAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
				JAll = JInterp{i,j}.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
				% G_ij(xi_kl,eta_kl)
				J{i,j,k,l} = reshape(JAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
			end
		end
	end
end

err1 = cell(n,n);
err2 = cell(n,n);
errGeta1 = cell(n,n);
errGeta2 = cell(n,n);
test = cell(n,n);
err1All = cell(n,n);

ndisc = numel(zdisc);
a0TilCanon = zeros(numel(zdisc),n,n,numtKernel);
devA0Til = zeros(numel(zdisc),n,n,numtKernel);
A0TilEverywhere = zeros(ndisc,ndisc,n,n,numtKernel);
for i=1:n
	for j=1:n
		xiMat = kernel.xiMatXi{i,j,i,j};
		
		kem=kernel.value{i,j};
		etaDisc = kem.eta;
		GH = kernel.value;
			
		GikSumzZeta2 = zeros(size(zetaMatRight));
		for k=1:n
			lambda_kr = reshape(GH{i,k+n}.lambda_j,1,numel(GH{i,k+n}.lambda_j));
			lambda_kl = reshape(GH{i,k}.lambda_j,1,numel(GH{i,k}.lambda_j));
			A1lrkj = reshape(A1lr(:,k,j,:),1,size(A1lr,1),numtKernel);
			A1lkj = reshape(A1l(:,k,j,:),1,size(A1l,1),numtKernel);
			
			GikSumzZeta2 = GikSumzZeta2 + ...
				 (...
				 numeric.trapz_fast_nDim(zetaMatRight,...
					1./ lambda_kr .* Gzzeta{i,k+n} .* A1lrkj, 2, 'equal','ignoreNaN')... % Gik
				+numeric.trapz_fast_nDim(zetaMatLeft,...
					1./ lambda_kl .* Gzzeta{i,k} .* A1lkj, 2, 'equal','ignoreNaN')); % Dik
		end
		GikSumInterp2 = numeric.interpolant({zdisc,zdisc,1:numtKernel},GikSumzZeta2);
		GikSumAll2 = GikSumInterp2.eval(repmat(kem.zMat(:),numtKernel,1),repmat(kem.zetaMat(:),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(kem.zMat),1),[numtKernel*numel(kem.zMat),1]));
		GikSum2 = reshape(GikSumAll2,size(kem.zMat,1),size(kem.zMat,2),numtKernel);
		
		% now check the final, implemented BC
		GikSumzZeta3 = zeros(size(zetaMatRight));
		for k=1:n
			lambda_kr = reshape(GH{i,k+n}.lambda_j,1,numel(GH{i,k+n}.lambda_j));
			lambda_kl = reshape(GH{i,k}.lambda_j,1,numel(GH{i,k}.lambda_j));
			A0lrkj = reshape(A0lr(:,k,j,:),1,size(A0lr,1),numtKernel);
			A0lkj = reshape(A0l(:,k,j,:),1,size(A0l,1),numtKernel);
			
			GikSumzZeta3 = GikSumzZeta3 + ...
				 (...
				 numeric.trapz_fast_nDim(zetaMatRight,...
					1./ lambda_kr .* Gzzeta{i,k+n} .* A0lrkj, 2, 'equal','ignoreNaN')... % Gik
				+numeric.trapz_fast_nDim(zetaMatLeft,...
					1./ lambda_kl .* Gzzeta{i,k} .* A0lkj, 2, 'equal','ignoreNaN')); % Dik
		end
		GikSumInterp3 = numeric.interpolant({zdisc,zdisc,1:numtKernel},GikSumzZeta3);
		GikSumAll3 = GikSumInterp3.eval(repmat(kem.zMat(:),numtKernel,1),repmat(kem.zetaMat(:),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(kem.zMat),1),[numtKernel*numel(kem.zMat),1]));
		GikSum3 = reshape(GikSumAll3,size(kem.zMat,1),size(kem.zMat,2),numtKernel);
		
		GikSumzZeta4 = zeros(size(zetaMatRight));
		for k=1:n
			lambda_kr = reshape(GH{i,k+n}.lambda_j,1,numel(GH{i,k+n}.lambda_j));
			lambda_kl = reshape(GH{i,k}.lambda_j,1,numel(GH{i,k}.lambda_j));
			A1lrkj = reshape(A1lr(:,k,j,:),1,size(A1lr,1),numtKernel);
			A1lkj = reshape(A1l(:,k,j,:),1,size(A1l,1),numtKernel);
			
			GikSumzZeta4 = GikSumzZeta4 + ...
				 1/2*(numeric.trapz_fast_nDim(zetaMatRight,...
					1./ lambda_kr .* (GH{i,k+n}.s*Hzzeta{i,k+n}+Jzzeta{i,k+n}).* A1lrkj, 2, 'equal','ignoreNaN')... % Gik
				+numeric.trapz_fast_nDim(zetaMatLeft,...
					1./ lambda_kl .* (Hzzeta{i,k}+Jzzeta{i,k}) .* A1lkj, 2, 'equal','ignoreNaN')); % Dik
		end
		GikSumInterp4 = numeric.interpolant({zdisc,zdisc,1:numtKernel},GikSumzZeta4);
		GikSumAll4 = GikSumInterp4.eval(repmat(kem.zMat(:),numtKernel,1),repmat(kem.zetaMat(:),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(kem.zMat),1),[numtKernel*numel(kem.zMat),1]));
		GikSum4 = reshape(GikSumAll4,size(kem.zMat,1),size(kem.zMat,2),numtKernel);
		
		errGeta1All = -J{i,j,i,j}+ 1/z0Til*H{i,j+n,i,j}...
			+ 1/(2*z0Til) * sqrt(ppide.l(1,j+n,j+n))*A0lrXiEta{i,i,j}...
			+ sqrt(lambdaZ{i+n,i,j})/2 .* A1DiffXiEta{i,i,j} -...
			1/(2*z0Til)* sqrt(GH{i,j+n}.lambda_j(1)) *GikSum3...
			- GikSum4;
		errGeta1AllOut = misc.writeToOutside(errGeta1All);
		errGeta1Interp = numeric.interpolant({kem.xi,kem.eta,1:numtKernel},errGeta1AllOut);
		
		errGeta1{i,j} =  ...
			reshape(...
				errGeta1Interp.eval(...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
				)...
			,1,numel(etaDisc(etaDisc>=0)),numtKernel);
		
		
		errGeta2All = -J{i,j+n,i,j} + z0Til*H{i,j,i,j} ...
			+ 1/2 * sqrt(ppide.l(1,j+n,j+n))*A0lrXiEta{i,i,j}...
			-1/2*z0Til*sqrt(lambdaZ{i+n,i,j}).*A1DiffXiEta{i,i,j} ...
			- 1/2*sqrt(GH{i,j+n}.lambda_j(1))* GikSum3 ...
			+ z0Til* GikSum4;
		errGeta2AllOut = misc.writeToOutside(errGeta2All);
		errGeta2Interp = numeric.interpolant({kem.xi,kem.eta,1:numtKernel},errGeta2AllOut);
		
		errGeta2{i,j} =  ...
			reshape(...
				errGeta2Interp.eval(...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
				)...
			,1,numel(etaDisc(etaDisc>=0)),numtKernel);
		
		% K1_til
		err1All{i,j} = -H{i,j+n,i,j} + J{i,j+n,i,j} - z0Til*(H{i,j,i,j} - J{i,j,i,j})...
			- sqrt(ppide.l(1,j+n,j+n))*A0lrXiEta{i,i,j} ...
			+ sqrt(ppide.l(1,j+n,j+n))*GikSum3;
		err1AllOut = misc.writeToOutside(err1All{i,j});
		err1Interp = numeric.interpolant({kem.xi,kem.eta,1:numtKernel},err1AllOut);
		
		err1{i,j} =  ...
			reshape(...
				err1Interp.eval(...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
				)...
			,1,numel(etaDisc(etaDisc>=0)),numtKernel);
		
		err1interp = numeric.interpolant({1,etaDisc(etaDisc>=0),numtKernel},err1{i,j});
		[~,~,~,etaZ] = kem.get_xi(zdisc,0);
		err1Lin = err1interp.eval({1,etaZ,numtKernel});
		a0TilCanon(:,i,j,:) = -err1Lin/sqrt(ppide.l(1,j+n,j+n));
		
		devA0Til(:,i,j,:) = a0TilCanon(:,i,j,:) - kernel.a0_til(:,i,j,:);
		
		
		% K2_til:
		err2All = -G{i,j,i,j} + 1/z0Til*G{i,j+n,i,j} + A1lrXiEta{i,i,j} - GikSum2;
		
		err2AllOut = misc.writeToOutside(err2All);
		err2Interp = numeric.interpolant({kem.xi,kem.eta,1:numtKernel},err2AllOut);
		
		err2{i,j} =  ...
			reshape(...
				err2Interp.eval(...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
				)...
			,1,numel(etaDisc(etaDisc>=0)),numtKernel);
		
		testAll = ppide.l_diff(1,j,j)/ppide.l(1,j,j) * G{i,j,i,j} + ppide.l_diff(1,j+n,j+n)/ppide.l(1,j+n,j+n) * G{i,j+n,i,j};
		testAllOut = misc.writeToOutside(testAll);
		testInterp = numeric.interpolant({kem.xi,kem.eta,1:numtKernel},testAllOut);
		
		test{i,j} = ...
			reshape(...
				testInterp.eval(...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
				)...
			,1,numel(etaDisc(etaDisc>=0)),numtKernel);
		
		GAll = misc.writeToOutside(G{i,j+n,i,j});
		GInterp = numeric.interpolant({kem.xi,kem.eta,1:numtKernel},GAll);
		GetaEta = reshape(...
				GInterp.eval(...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
				)...
			,1,numel(etaDisc(etaDisc>=0)),numtKernel);
		GetaEtaInterp = numeric.interpolant({1,kem.eta(kem.eta>=0)/kem.eta(end),1:numtKernel},GetaEta);
		GetaEtaz = GetaEtaInterp.eval({1,zdisc,numtKernel});
		Kz0 = kernel.value{i,j+n}.K(:,1,:);
		
		test2{i,j} = GetaEtaz - ppide.l(1,j+n,j+n)*Kz0.';
		
		A0TilEverywhere(:,:,i,j,:) = ...
			ppide.l(1,j+n,j+n)* dzeta_K(:,:,i,j+n,:) + ...
			ppide.l(1,j,j)* dzeta_K(:,:,i,j,:) + ...
			ppide.l_diff(1,j+n,j+n)*kernel.value{i,j+n}.K + ...
			ppide.l_diff(1,j,j)*kernel.value{i,j}.K + ...
			reshape(A0lr(:,i,j,:),ndisc,1,1,1,numtKernel)  ...
			- reshape(...
					numeric.trapz_fast_nDim(zetaMatRight,...
						permute(...
							misc.multArray(K(:,:,i,n+1:end),... %zeta,z,i,1,j
								permute(A0lr(:,:,j),[4 1 2 3]),4,3,[2])...
						,[2 1 3 4 5])... z zeta 1 1 1 
					,2,'equal','ignoreNaN')...% z 1	
				,ndisc,1,1,numtKernel)...
				- reshape(...
					numeric.trapz_fast_nDim(zetaMatLeft,...
						permute(...
							misc.multArray(K(:,:,i,1:n),... %zeta,t,z,i,1,j
								permute(A0l(:,:,j,:),[4 1 2 3]),4,3,[2])...
						,[2 1 3 4 5])... z zeta t 1 1 1 
					,2,'equal','ignoreNaN')...% z 1 t	
				,ndisc,1,1,numtKernel);
	end
end



end

