function GH = init_kernel_elementsDecoupling(~,ppide,zdisc)
%INIT_KERNEL_ELEMENTS initialize kernel elements for ppide system

% created on 14.09.2016 by Simon Kerschbaum
% modified on 03.04.2017 by SK:
%  - implementation of PIDEs with spatially varying diffusion
import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
ndisc = length(zdisc);
ndiscPars = ppide.ndiscPars;
zdiscPars = linspace(0,1,ndiscPars);
ndiscTime = length(ppide.ctrl_pars.tDiscKernel);

%%% Interpolate system parameters to kernel resolution 
pars.lambda_disc = translate_to_grid(ppide.l_disc,zdisc);
pars.lambda_diff_disc = translate_to_grid(ppide.l_diff_disc,zdisc);
pars.lambda_diff2_disc = translate_to_grid(ppide.l_diff2_disc,zdisc);
pars.a0_disc = translate_to_grid(ppide.a_0_disc,zdisc(:));
pars.c0_disc = translate_to_grid(ppide.c_0_disc,zdisc(:));


% all time-dependent coefficients need to be resampled in time-direction
varlist = {'a0_disc','c0_disc'};
dims = 4*ones(length(varlist),1);
tDisc = ppide.ctrl_pars.tDiscKernel;
timeDependent=0;
for i=1:length(varlist)
	dim = dims(i);
	if size(pars.(varlist{i}),dim)>1
		pars.(varlist{i}) = permute(misc.translate_to_grid(permute(pars.(varlist{i}),[dim 1:dim-1]),tDisc),[2:dim 1]);
		timeDependent=1;
	end
end

n = size(pars.lambda_disc,2)/2;

% time dimension of the kernel is discretized with ndiscTime, if one of the parameters is
% time-dependent
if timeDependent
	numtL = size(pars.lambda_disc,4); % number of timesteps in lambda
	numtKernel = ndiscTime; % number of timesteps in kernel
else
	numtL = 1;
	numtKernel = 1;
end

% numerical parameters:
accur = 1/ppide.ctrl_pars.min_z_res; % accuracy towards all values are rounded
min_xi_diff = ppide.ctrl_pars.min_xi_diff; % minimum distance that points inside the xi/eta domain have to vary
maxNumXi = ppide.ctrl_pars.maxNumXi;
min_eta_diff = ppide.ctrl_pars.min_eta_diff; % in sparse grid
min_xi_diff_border = ppide.ctrl_pars.min_xi_diff_border; % minimum distance that points wich arise from a boundary have to vary
min_eta_diff_border = ppide.ctrl_pars.min_eta_diff_border; % in sparse grid
min_dist = ppide.ctrl_pars.min_dist; % distance below which points are considered as equal. Should be higher as min_z_res
% (needed if points are the result of a math operation, e.g. 2*a-xi)

GH{n,2*n} = 0; %preallocate

z0 = ppide.ctrl_pars.foldingPoint;
if isscalar(z0) % scalar
	z0 = ones(1,n)*z0;
end
z0Left = zeros(2*n);
zRight = zeros(2*n,ndiscPars);
for i=1:n
	zRight(i,:) = linspace(z0(i),1,ppide.ndiscPars);
	dZ = diff(zRight(i,:));
	z0Left(i) = z0(i)-dZ(1);
end
for i=1:n
	for j=1:2*n
		%%%%%%%%%% create grid %%%%%%%%%
		xi = zeros(sum(1:(ndisc)),numtL); % preallocate 
		eta = zeros(sum(1:(ndisc)),numtL);% at first, all possible combinations are loaded
		xiMat = zeros(ndisc,ndisc,numtL);
		etaMat = zeros(ndisc,ndisc,numtL);
		
% 		if j<=n
% 			elementPars.A0 = reshape(pars.a0_disc(:,i+n,j,:),ndisc,[]);
% 			elementPars.A1 = reshape(pars.c0_disc(:,i+n,j,:),ndisc,[]);
% 		else
% 			elementPars.A0 = reshape(pars.a0_disc(:,i+n,j-n,:),ndisc,[]);
% 			elementPars.A1 = reshape(pars.c0_disc(:,i+n,j-n,:),ndisc,[]);
% 		end
		
		% either left or right depending on j
		elementPars.lambda_j = reshape(pars.lambda_disc(:,j,j,:),ndisc,[]);
		elementPars.lambda_j_diff = reshape(pars.lambda_diff_disc(:,j,j,:),ndisc,[]);
		elementPars.lambda_j_diff2 = reshape(pars.lambda_diff2_disc(:,j,j,:),ndisc,[]);
		
		% lambda_i only needed from right system
		elementPars.lambda_i = reshape(pars.lambda_disc(:,i+n,i+n,:),ndisc,[]); % reshape is needed if there is a time-dimension
		elementPars.lambda_i_diff = reshape(pars.lambda_diff_disc(:,i+n,i+n,:),ndisc,[]);
		elementPars.lambda_i_diff2 = reshape(pars.lambda_diff2_disc(:,i+n,i+n,:),ndisc,[]);

		
		k=1; % running index for xi, eta. 
		% important: compute phi with high-res because value of integral is changed!
		% only phi_i_r is needed.
		phi_iFineR = cumtrapz(zdiscPars,(1./sqrt(reshape(ppide.l(:,i+n,i+n,:),ndiscPars,[]))),1);
		phi_iFineInterpR = numeric.interpolant({zdiscPars},phi_iFineR);
		phi_iR = phi_iFineInterpR.eval({zdisc});
		% left or right depending on j
		phi_jFine = cumtrapz(zdiscPars,(1./sqrt(reshape(ppide.l(:,j,j,:),ndiscPars,[]))),1);
		phi_jFineInterp = numeric.interpolant({zdiscPars},phi_jFine);
		phi_j = phi_jFineInterp.eval({zdisc});
		
		% calculate xieta right
		if j>n
			for z_idx = 1:length(zdisc)
				for zeta_idx = 1:z_idx	
					if elementPars.lambda_i(1)>= elementPars.lambda_j(1) % still no different check is performed! 
						% This check should be drastically improved!
						xi(k,:) = phi_iR(z_idx,:) + phi_j(zeta_idx,:); 
						eta(k,:) = phi_iR(z_idx,:) - phi_j(zeta_idx,:);
					else
						xi(k,:) = phi_iR(end,:)+phi_j(end,:)-phi_iR(z_idx,:)-phi_j(zeta_idx,:);
						eta(k,:) = -phi_iR(end,:)+phi_j(end,:)+phi_iR(z_idx,:)-phi_j(zeta_idx,:);
					end
					xiMat(z_idx,zeta_idx,1:numtL) = xi(k,:);
					etaMat(z_idx,zeta_idx,1:numtL) = eta(k,:);
					k=k+1;
				end
				% TODO: keine For-Schleife noetig!
				for zeta_idx = z_idx+1:length(zdisc)
					xiMat(z_idx,zeta_idx,1:numtL) = xiMat(z_idx,z_idx,1:numtL); % take the diagonal value into the not defined area to prevent interpolation errors
					etaMat(z_idx,zeta_idx,1:numtL) = etaMat(z_idx,z_idx,1:numtL); % take the diagonal value into the not defined area to prevent interpolation errors
				end
			end
		else
			% calculate xieta left
			for z_idx = 1:length(zdisc)
				for zeta_idx = 1:length(zdisc)	
					xi(k,:) = phi_iR(z_idx,:) + phi_j(zeta_idx,:); 
					eta(k,:) = phi_iR(z_idx,:) - phi_j(zeta_idx,:);
					xiMat(z_idx,zeta_idx,1:numtL) = xi(k,:);
					etaMat(z_idx,zeta_idx,1:numtL) = eta(k,:);
					k=k+1;
				end
			end
		end
		xiMatInterp = numeric.interpolant({zdisc,zdisc,1:numtL},xiMat);
		etaMatInterp = numeric.interpolant({zdisc,zdisc,1:numtL},etaMat);
		% remove duplicate entries after rounding to accur, has to be
		% higher resolution than min_dist!
		% The sort command is neccessary beacause the unique command does not sort each column, 
		% when 'rows' is passed.
		xi_fine = sort(unique(acc(xi,accur),'rows')); 
		eta_fine = sort(unique(acc(eta,accur),'rows'));
		% remove all entries, which are closer than min_dist, but no
		% rounding!
		xi_fine = unique_accur(xi_fine,min_dist); 
		eta_fine = unique_accur(eta_fine,min_dist); 
		% In case of time-dependent coordinates, the different columns of xi,eta may contain
		% duplicates, because a row can only be removed, if all the elements' deviation is
		% small.
		% The following function removes the duplicates.
		xi_fine = distributeDuplicates(xi_fine); 
		eta_fine = distributeDuplicates(eta_fine);
		
		% create kernel_element. The sparse grid is created by the
		% constructor
		if j<=n
			fredholm=1;
		else
			fredholm=0;
		end
		GH{i,j} = dps.parabolic.control.backstepping.ppide_kernel_element('lambda_i',elementPars.lambda_i,...
									  'lambda_j',elementPars.lambda_j,...
									  'lambda_iFine',reshape(ppide.l(:,i+n,i+n,:),ndiscPars,[]),...
									  'lambda_jFine',reshape(ppide.l(:,j,j,:),ndiscPars,[]),...
									  'zdiscFine',linspace(0,1,ndiscPars),...
									  'lambda_i_diff',elementPars.lambda_i_diff,...
									  'lambda_j_diff',elementPars.lambda_j_diff,...
									  'lambda_i_diff2',elementPars.lambda_i_diff2,...
									  'lambda_j_diff2',elementPars.lambda_j_diff2,...
									  'i',i,'j',j,...
									  'xi_fine',xi_fine,'eta_fine',eta_fine,...
									  'xiMat',xiMat,...
									  'etaMat',etaMat,...
									  'xiMatInterp',xiMatInterp,...
									  'etaMatInterp',etaMatInterp,...
									  'zdisc',zdisc,...
									  'min_xi_diff',min_xi_diff,...
									  'maxNumXi',maxNumXi,...
									  'min_eta_diff',min_eta_diff,...
									  'min_xi_diff_border',min_xi_diff_border,...
									  'min_eta_diff_border',min_eta_diff_border,...
									  'accur',accur,...
									  'min_dist',min_dist,...
									  'eliminate_convection',0,...
									  'fredholm',fredholm);	
		xi = GH{i,j}.xi; % get the sparse grid
		eta = GH{i,j}.eta; 
		
		parlist = {'lambda_i','lambda_j','lambda_i_diff','lambda_i_diff2','lambda_j_diff',...
					'lambda_j_diff2'};
		
		% Store repeated values in time, wich are needed for multiple arguments
		elementParsTime = elementPars;
		for parIdx =1:length(parlist)
			if size(elementPars.(parlist{parIdx}),2)<numtKernel
				elementParsTime.(parlist{parIdx}) = repmat(elementPars.(parlist{parIdx}),1,numtKernel);
			end
		end
								
		%%%%%%%%%% abbreviations %%%%%%%%%
		obj = GH{i,j}; % short form
		% abbreviations from BC
		GH{i,j}.a_i = (-1/2*elementPars.lambda_i_diff)./sqrt(elementPars.lambda_i);
		aidisc = GH{i,j}.a_i;
		GH{i,j}.a_iInterp = interpolant({zdisc,1:numtL},aidisc);
		a_iz_xietaTemp = GH{i,j}.a_iInterp.eval(GH{i,j}.zMat(:),1:numtL);
		GH{i,j}.aiZXieta = reshape(a_iz_xietaTemp,size(GH{i,j}.xi,1),size(GH{i,j}.eta,1),numtL);
		GH{i,j}.a_j = (1/2*elementPars.lambda_j_diff)./sqrt(elementPars.lambda_j);
		ajdisc = GH{i,j}.a_j;
		GH{i,j}.a_jInterp = interpolant({zdisc,1:numtL},ajdisc);
		a_jzeta_xietaTemp = GH{i,j}.a_jInterp.eval(GH{i,j}.zetaMat(:),1:numtL);
		GH{i,j}.ajZetaXieta = reshape(a_jzeta_xietaTemp,size(GH{i,j}.xi,1),size(GH{i,j}.eta,1),numtL);
		% used in get_G_eta0 and for d
		GH{i,j}.b_j = 0;		
		% used for d:
		% sign is exactly opposite of p.61 diss.
% 		GH{i,j}.a_ij_til = ...
% 			-1/2*(...
% 				1/2*reshape(elementPars.lambda_i_diff2,[ndisc,1,numtL])...
% 				)...
% 			+1/4*reshape(elementPars.lambda_i_diff,[ndisc,1,numtL])...
% 				./reshape(elementPars.lambda_i,[ndisc,1,numtL])....
% 				.*(1/2*reshape(elementPars.lambda_i_diff,[ndisc,1,numtL])...
% 				)...
% 			+1/2*(...
% 				1/2*reshape(elementPars.lambda_j_diff2,[1,ndisc,numtL])...
% 				)...
% 			-1/4*reshape(elementPars.lambda_j_diff,[1,ndisc,numtL])...
% 				./reshape(elementPars.lambda_j,[1,ndisc,numtL])...
% 				.*(...
% 				1/2*reshape(elementPars.lambda_j_diff,[1,ndisc,numtL])...
% 				);
% 		aijInterp = numeric.interpolant({zdisc,zdisc,1:numtL},GH{i,j}.a_ij_til);
% 		aijXiEtaTemp = aijInterp.eval(repmat(GH{i,j}.zMat(:),numtL,1),repmat(GH{i,j}.zetaMat(:),numtL,1),...
% 			reshape(repmat(1:numtL,numel(GH{i,j}.zMat),1),[numtL*numel(GH{i,j}.zMat),1]));
% 		GH{i,j}.a_ij_tilXiEta = reshape(aijXiEtaTemp,size(GH{i,j}.xi,1),size(GH{i,j}.eta,1),numtL);
			
			
		% TODO: Schauen, ob/wo die r �behaupt noch verwendet werden!

		% constant c1 = (sqrt(lambda_jr(0)/lambda_jl(0))+ z0/(1-z0)) --> r{1}
		if j<=n
			indR = j+n;
			indL = j;
		else
			indR = j;
			indL = j-n;
		end
		r{1} = sqrt(pars.lambda_disc(1,indR,indR)/pars.lambda_disc(1,indL,indL)) + z0(i)/(1-z0(i));

		GH{i,j}.r = r; 


		% abbreviations from PDE:
% 			GH{i,j}.e = elementPars.lambda_j;

		% used in use_F_ppides (all 3)
% 		GH{i,j}.d = -GH{i,j}.a_ij_til - reshape(GH{i,j}.b_j,[1,ndisc,numtL]);
% 		GH{i,j}.f = elementPars.lambda_j./elementPars.lambda_i;
		GH{i,j}.g = ...
			reshape(elementPars.lambda_j,[ndisc,1,numtL])...
			./reshape(elementPars.lambda_i,[1,ndisc,numtL]);
		
		%%%%%%%%%% initial values of fixpoint iteration %%%%%%%%%
		GH{i,j}.G = zeros(size(xi,1),size(eta,1),numtKernel); % preallocate
		GH{i,j}.H = zeros(size(xi,1),size(eta,1),numtKernel); % preallocate
		GH{i,j}.J = zeros(size(xi,1),size(eta,1),numtKernel); % preallocate
		
	end % for j
% first need to calculate all j because j+n is needed!
end


end



