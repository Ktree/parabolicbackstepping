function test = checkDecouplingKernel(decouplingKernel, ppide, plotResult)
% checkControllerKernel inserts the previously calculated controller-kernel
% into its kernel equations and prints the residuals, to give some insight
% if they are correct or not. Note that the residual of the pde is usually
% relatively big, because numerical derivatives are used, which are very
% inaccurate. If one is uncertain about the interpretation of the numbers,
% having a look at the plots of the distributed residuals might be helpful.
	if nargin < 3
		plotResult = false;
	end
	%% Read parameter
	n = ppide.n/2;
	disc.n = ppide.ndiscPars;
	disc.z = linspace(0,1,disc.n);
	disc.dz = disc.z(2) - disc.z(1);
	disc.Lambda = ppide.l;
	if size(disc.Lambda,4)>1
		error('Not yet implemented for time-varying lambda!')
	end
	disc.Lambda_dz = ppide.l_diff;
	disc.Lambda_ddz = ppide.l_diff2;
	K = decouplingKernel.get_K;
	K_t = decouplingKernel.get_K('K_t',3);
	disc.ntKernel = size(K,5);
	
	disc.nKernel = size(K,1);
	disc.zKernel = linspace(0,1,disc.nKernel);
	dzetaK = zeros(disc.nKernel,disc.n,n,2*n,disc.ntKernel);
	for zIdx = 2:disc.nKernel
		dzetaK(zIdx,1:zIdx,:,:,:) = numeric.diff_num(disc.zKernel(1:zIdx),K(zIdx,1:zIdx,:,:,:),2); 
	end
	dzetaK(1,1,:,:,:) = dzetaK(2,1,:,:,:);
	
	% K is already printed to outside of the valid spatial domain,
% 	for it = 1:n
% 		for jt = 1:n
% 			K(:,:,it,jt) = inpaint_nans(K(:,:,it,jt));
% 		end
% 	end
	
	test.K.value = misc.translate_to_grid(K, disc.z, 'zdim', 2);%, 'method', 'spline');
	test.Kt.value = misc.translate_to_grid(K_t, disc.z, 'zdim', 2);%, 'method', 'spline');
	[test.K.dzeta, test.K.dz] = numeric.gradient_on_2d_triangular_domain(test.K.value , disc.dz);
	[test.K.ddzeta, ~] = numeric.gradient_on_2d_triangular_domain(test.K.dzeta, disc.dz);
	% use normal derivative for first derivative
	test.K.dzetaNum = misc.translate_to_grid(dzetaK, disc.z, 'zdim', 2);%, 'method', 'spline');
	[~, test.K.ddz] = numeric.gradient_on_2d_triangular_domain(test.K.dz, disc.dz);
	
	Gzzeta = cell(n,2*n);Hzzeta = cell(n,2*n);Jzzeta = cell(n,2*n);HEtazzeta = cell(n,2*n);Gtzzeta = cell(n,2*n);
	JXizzeta = cell(n,2*n);
	numtKernel = size(decouplingKernel.value{1,1}.G,3);
	lKernel = misc.translate_to_grid(ppide.l,disc.zKernel);
	l_diffKernel = misc.translate_to_grid(ppide.l_diff,disc.zKernel);
	for i = 1:n
		for j=1:2*n
			GInterp{i,j} = numeric.interpolant({decouplingKernel.value{i,j}.xi,decouplingKernel.value{i,j}.eta,1:numtKernel},decouplingKernel.value{i,j}.G);
			GtInterp{i,j} = numeric.interpolant({decouplingKernel.value{i,j}.xi,decouplingKernel.value{i,j}.eta,1:numtKernel},decouplingKernel.value{i,j}.G_t);
			JInterp{i,j} = numeric.interpolant({decouplingKernel.value{i,j}.xi,decouplingKernel.value{i,j}.eta,1:numtKernel},decouplingKernel.value{i,j}.J);
			HInterp{i,j} = numeric.interpolant({decouplingKernel.value{i,j}.xi,decouplingKernel.value{i,j}.eta,1:numtKernel},decouplingKernel.value{i,j}.H);
			kem = decouplingKernel.value{i,j};
			etaDisc = kem.eta;
			xiDisc = kem.xi;
			Heta = numeric.diff_num(etaDisc,kem.H,2);
			Jxi = numeric.diff_num(xiDisc,kem.J,1);
			HetaAll = misc.writeToOutside(Heta);
			JXiAll = misc.writeToOutside(Jxi);
			HEtaInterp = numeric.interpolant({xiDisc,etaDisc,1:numtKernel},HetaAll);
			JXiInterp = numeric.interpolant({xiDisc,etaDisc,1:numtKernel},JXiAll);
			
			xiMatZ = decouplingKernel.value{i,j}.xiMat; %xi,eta(z,zeta)
			etaMatZ = decouplingKernel.value{i,j}.etaMat;
			GijAll = GInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
			GtijAll = GtInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));		
			HijAll = HInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
			JijAll = JInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
			HEtaijAll = HEtaInterp.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
			JXiijAll = JXiInterp.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));		
					
			% used for integrations over z or zeta
			Gzzeta{i,j} = reshape(GijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
			Gtzzeta{i,j} = reshape(GtijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
			Hzzeta{i,j} = reshape(HijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
			HEtazzeta{i,j} = reshape(HEtaijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
			Jzzeta{i,j} = reshape(JijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
			JXizzeta{i,j} = reshape(JXiijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
		end 
	end
	
	test.G = zeros(disc.nKernel,disc.nKernel,n,2*n,numtKernel);
	% check canoncial PDE:
	for i=1:n
		for j=1:2*n
			kem = decouplingKernel.value{i,j};
			% 4sG_{xieta} = (-1/2 lambda_i^r'(z)/sqrt(lambda_i^r) + 1/2
			%   lambda_j^l'(zeta)/sqrt(lambda_j^l) G_xi
			%   + (-1/2 lambda_i^r'(z)/sqrt(lambda_i^r)) - 1/2
			%   lambda_j^l'(zeta)/sqrt(lambda_j(zeta)) G_eta + G_t
			test.G(:,:,i,j,:) = ...
				+ 4*kem.s.*HEtazzeta{i,j} ...
				+ (-1/2*l_diffKernel(:,i+n,i+n)./sqrt(lKernel(:,i+n,i+n))...
				+ 1/2*l_diffKernel(:,j,j).'./sqrt(lKernel(:,j,j)).') .* kem.s.*Hzzeta{i,j} ...
			    + (-1/2*l_diffKernel(:,i+n,i+n)./sqrt(lKernel(:,i+n,i+n))...
				- 1/2*l_diffKernel(:,j,j).'./sqrt(lKernel(:,j,j)).').* Jzzeta{i,j} ...
				- Gtzzeta{i,j};
			test.GAlt(:,:,i,j,:) = ...
				+4*kem.s.*JXizzeta{i,j} ...
				+ (-1/2*l_diffKernel(:,i+n,i+n)./sqrt(lKernel(:,i+n,i+n))...
				+ 1/2*l_diffKernel(:,j,j).'./sqrt(lKernel(:,j,j)).') .* kem.s.*Hzzeta{i,j} ...
			    + (-1/2*l_diffKernel(:,i+n,i+n)./sqrt(lKernel(:,i+n,i+n))...
				- 1/2*l_diffKernel(:,j,j).'./sqrt(lKernel(:,j,j)).').* Jzzeta{i,j} ...
				- Gtzzeta{i,j};
			test.GAltAlt(:,:,i,j,:) = ...
				+4*kem.s.*JXizzeta{i,j} ...
				+ (kem.a_i + kem.a_j.') .* kem.s.*Hzzeta{i,j} ...
			    + (kem.a_i - kem.a_j.').* Jzzeta{i,j} ...
				- Gtzzeta{i,j};
		end
	end
	
	
	%% upper boundary for fredholm part
	test.K.bc.zeta1 = zeros(disc.nKernel,n,n,numtKernel);
	for i=1:n
		for j=1:n
			if ppide.b_op1.b_n(j+n,j+n) ~= 0
				test.K.bc.zeta1(:,i,j,:) = ...
					decouplingKernel.value{i,j}.dzeta_K(:,end,:)*ppide.l(end,j,j)...
					+ decouplingKernel.value{i,j}.K(:,end,:)*ppide.l_diff(end,j,j);
			else
				test.K.bc.zeta1(:,i,j,:) = decouplingKernel.value{i,j}.K(:,end,:);
			end
		end
	end
	
	
	%% PDE
	% Lambda(z) K_ddz(z, zeta) - (K(z, zeta) Lambda(zeta))_ddzeta - K(z, zeta) (A(zeta) + mu I) = 0
	test.K.pde = zeros(disc.n, disc.n, n, 2*n,disc.ntKernel); 
	LddzSum = zeros(disc.n,disc.n,n,2*n,disc.ntKernel);
	KddzetaLSum = zeros(disc.n,disc.n,n,2*n,disc.ntKernel);
	KdzetaLdzSum = zeros(disc.n,disc.n,n,2*n,disc.ntKernel);
	KLddzSum = zeros(disc.n,disc.n,n,2*n,disc.ntKernel);
	% to avoid loops in z,zeta and t direction, the matrix multiplication is performed in a
	% loop, which allows to use the .*-operator that is very performant!
	for k=1:n
		LddzSum(:,:,:,1:n,:) = LddzSum(:,:,:,1:n,:) + reshape(disc.Lambda(:,n+1:end,k+n),disc.n,1,n).*test.K.ddz(:,:,k,1:n,:);
		LddzSum(:,:,:,n+1:end,:) = LddzSum(:,:,:,n+1:end,:) + reshape(disc.Lambda(:,n+1:end,k+n),disc.n,1,n).*test.K.ddz(:,:,k,n+1:end,:);
		KddzetaLSum(:,:,:,1:n,:) = KddzetaLSum(:,:,:,1:n,:) + test.K.ddzeta(:,:,1:n,k,:).*reshape(disc.Lambda(:,k,1:n),1,disc.n,1,n);
		KddzetaLSum(:,:,:,n+1:end,:) = KddzetaLSum(:,:,:,n+1:end,:) + test.K.ddzeta(:,:,:,k+n,:).*reshape(disc.Lambda(:,k+n,n+1:end),1,disc.n,1,n);
		KdzetaLdzSum(:,:,:,1:n,:) = KdzetaLdzSum(:,:,:,1:n,:) + test.K.dzeta(:,:,1:n,k,:).*reshape(disc.Lambda_dz(:,k,1:n),1,disc.n,1,n);
		KdzetaLdzSum(:,:,:,n+1:end,:) = KdzetaLdzSum(:,:,:,n+1:end,:) + test.K.dzeta(:,:,:,k+n,:).*reshape(disc.Lambda_dz(:,k+n,n+1:end),1,disc.n,1,n);
		KLddzSum(:,:,:,1:n,:) = KLddzSum(:,:,:,1:n,:) + test.K.value(:,:,1:n,k,:).*reshape(disc.Lambda_ddz(:,k,1:n),1,disc.n,1,n);
		KLddzSum(:,:,:,n+1:end,:) = KLddzSum(:,:,:,n+1:end,:) + test.K.value(:,:,:,k+n,:).*reshape(disc.Lambda_ddz(:,k+n,n+1:end),1,disc.n,1,n);
	end
	test.K.pde = LddzSum - KddzetaLSum - 2*KdzetaLdzSum - KLddzSum - test.Kt.value;
	
	%% BC at zeta=0
	% K(z, 0) Lambda(0) + A0_tilde(z) = 0
% 	A0_tilde = misc.translate_to_grid(controllerKernel.a0_til, disc.z);
% 	if ppide.folded
% 		dzeta_K_z_0 = controllerKernel.get_K('dzeta_K_z_0',2);
% 		dzeta_K_z_0Interp = numeric.interpolant({disc.zKernel,1:n,1:n,linspace(0,1,disc.ntKernel)},dzeta_K_z_0);
% 		dzeta_K_z_0Plant = dzeta_K_z_0Interp.eval({disc.z,1:n,1:n,linspace(0,1,disc.ntKernel)});
% 		A0_neu = misc.translate_to_grid(controllerKernel.a0_neu, disc.z);
% 		z0 = ppide.ctrl_pars.foldingPoint;
% 		if isscalar(z0)
% 			z0 = ones(n/2,1)*z0;
% 		end
% 		zdiscPars = linspace(0,1,ppide.ndiscPars);
% 		z0Pars = zeros(ppide.n/2);
% 		for i=1:ppide.n/2
% 			z0Pars(i) = zdiscPars(find(zdiscPars<z0(i),1,'last'));
% 		end
% % 		z0Til = -diag(z0)./(1-diag(z0));
% 		z0Til = -diag(z0Pars)./(1-diag(z0));
% 		S2 = [eye(n/2);eye(n/2)];
% 		S1 = [z0Til;eye(n/2)];
% 		analytical = 1;
% 		if ~analytical
% 			dzetaKz0 = permute(test.K.dzetaNum(:,1,:,:,:),[1 3 4 5 2]);
% 		else
% 			dzetaKz0 = dzeta_K_z_0Plant;
% 		end
% 		
% 		if size(test.K.value,5)>1 % time dependent
% 			test.K.bc.zeta0.value = permute(misc.multArray(...
% 				permute(misc.multArray(...                      %z i j t - -
% 					test.K.value(:,1,:,:,:),ppide.l(1,:,:),4,2),[1 3 6 4 2 5])...
% 					,S1,3,1),[1 2 4 3])...
% 				+permute(misc.multArray(...
% 					A0_neu...
% 				,S1,3,1),[1 2 4 3]); 
% 			test.K.bc.zeta0.dzeta = permute(misc.multArray(...
% 				permute(misc.multArray(...                      %z i j t - -test.K.dzetaNum(:,1,:,:,:),ppide.l(1,:,:),4,2),[1 3 6 4 2 5])...
% 					dzetaKz0,ppide.l(1,:,:),3,2),[1 2 5 3 4])...
% 					,S2,3,1),[1 2 4 3])...
% 				+ permute(misc.multArray(...
% 				permute(misc.multArray(...                      %z i j t - -
% 					test.K.value(:,1,:,:,:),ppide.l_diff(1,:,:),4,2),[1 3 6 4 2 5])...
% 					,S2,3,1),[1 2 4 3])...
% 				-permute(misc.multArray(...
% 					A0_tilde...
% 				,S2,3,1),[1 2 4 3]); 
% 		else
% 			test.K.bc.zeta0.value = misc.multArray(...
% 				permute(misc.multArray(...                      %z i j  - -
% 					test.K.value(:,1,:,:,:),ppide.l(1,:,:),4,2),[1 3 5 2 4])...
% 					,S1,3,1)...
% 				+misc.multArray(...
% 					A0_neu...
% 				,S1,3,1); 
% 			test.K.bc.zeta0.dzeta = misc.multArray(...
% 				permute(misc.multArray(...                         %z i j - -test.K.dzetaNum(:,1,:,:,:),ppide.l(1,:,:),4,2),[1 3 5 2 4])...
% 					dzetaKz0,ppide.l(1,:,:),3,2),[1 2 4 3])...
% 					,S2,3,1)...
% 				+ misc.multArray(...
% 				permute(misc.multArray(...                      %z i j t - -
% 					test.K.value(:,1,:,:,:),ppide.l_diff(1,:,:),4,2),[1 3 5 2 4])...
% 					,S2,3,1)...
% 				-misc.multArray(...
% 					A0_tilde...
% 				,S2,3,1); 
% 		end
% 	else
% 		test.K.bc.zeta0 = zeros(disc.n, n, n);
% 		for jt = 1:n
% 			test.K.bc.zeta0(:,:,jt) = reshape(test.K.value(:,1,:,jt), [disc.n, n]) * disc.Lambda(1,jt,jt);
% 		end
% 		test.K.bc.zeta0 = test.K.bc.zeta0 + A0_tilde;		
% 	end
% 	
% 	%% Is A0_tilde lower triangular?
% 	test.A0_tilde_upper = 0;
% 	test.A0_neu_upper = 0;
% 	numberOfA0Elements = 1;
% 	for it = 1:n
% 		for jt = 1:n
% 			if ppide.l(1,it,it,1)>= ppide.l(1:jt,jt,1) % A0Til should be 0
% 				test.A0_tilde_upper = test.A0_tilde_upper + mean(abs(A0_tilde(:, it, jt)));
% 				test.A0_neu_upper = test.A0_neu_upper + mean(abs(A0_neu(:, it, jt)));
% 				numberOfA0Elements = numberOfA0Elements+1;
% 			end
% 		end
% 	end
% 	test.A0_tilde_upper = test.A0_tilde_upper / numberOfA0Elements;
% 	test.A0_neu_upper = test.A0_neu_upper / numberOfA0Elements;
% 	
% 	%% BC at z=zeta (algebraic)
% 	% K(z, z) Lambda(z) - Lambda(z) K(z, z) = 0
% 	test.K.zEqualZeta = misc.diag_nd(test.K.value);
% 	test.K.bc.zzeta.algebraic = zeros(disc.n, n, n, disc.ntKernel);
% 	for zIdx = 1:disc.n
% 		for tIdx = 1:disc.ntKernel
% 			test.K.bc.zzeta.algebraic(zIdx, :, :, tIdx) = ...
% 				reshape(test.K.zEqualZeta(zIdx, :, :, tIdx), [n, n]) * reshape(disc.Lambda(zIdx, :, :), [n, n]) ...
% 				- reshape(disc.Lambda(zIdx, :, :), [n, n]) * reshape(test.K.zEqualZeta(zIdx, :, :, tIdx), [n, n]);
% 		end
% 	end
% 	
% 	%% BC at z=zeta (pde)
% 	% Lambda(z) (dz K(z, z) + K_dz(z, z)) + K_dzeta(z, z) Lambda(z) ...
% 	%		+ K(z, z) Lambda_dz(z) + A(z) + Mu(z) = 0
% 	if n == 1
% 		test.K.zEqualZetaTotalDiff = numeric.diff_num(disc.z.', test.K.zEqualZeta);
% 	else
% 		[~, test.K.zEqualZetaTotalDiff] = gradient(test.K.zEqualZeta, disc.dz);
% 	end
% 	test.K.zEqualZeta_dz = misc.diag_nd(test.K.dz);
% 	test.K.zEqualZeta_dzeta = misc.diag_nd(test.K.dzeta);
% 	test.K.bc.zzeta.pde = zeros(disc.n, n, n, disc.ntKernel);
% 	for zIdx = 1:disc.n
% 		for tIdx = 1:disc.ntKernel
% 			if size(disc.A,4)>1, tIdxA = tIdx; else, tIdxA = 1; end
% 			if size(mu,4)>1, tIdxMu = tIdx; else, tIdxMu = 1; end
% 			test.K.bc.zzeta.pde(zIdx, :, :, tIdx) = ...
% 				reshape(disc.Lambda(zIdx, :, :), [n, n]) * ...
% 					(reshape(test.K.zEqualZetaTotalDiff(zIdx, :, :, tIdx), [n, n]) ...
% 					+ reshape(test.K.zEqualZeta_dz(zIdx, :, :, tIdx), [n, n])) ...
% 				+ reshape(test.K.zEqualZeta_dzeta(zIdx, :, :, tIdx), [n, n]) * ...
% 					reshape(disc.Lambda(zIdx, :, :), [n, n]) ...
% 				+ reshape(test.K.zEqualZeta(zIdx, :, :, tIdx), [n, n]) * ...
% 					reshape(disc.Lambda_dz(zIdx, :, :), [n, n]) ...
% 				+ reshape(disc.A(zIdx, :, :, tIdxA), [n, n]) ...
% 				+ reshape(mu(zIdx,:,:, tIdxMu),[n,n]);
% 		end
% 	end
	
	%% BC at z=zeta (solution of pde) for lambda_i == lambda_j
	% K_ij(z, z) = K_ij(0, 0) * sqrt(lambda_i(0)/lambda_i(z)) ...
	%		- 1/sqrt(lambda_i(z)) * ...
	%			int_0^z (A_ij(zeta) + Mu_ij(zeta)) / (2 * sqrt(lambda_i(zeta))) dzeta
% 	test.K.bc.zzeta.integralSolution = zeros(disc.n, n, n, disc.ntKernel);
% 	test.K.bc.zzeta.numericSolution = zeros(disc.n, n, n, disc.ntKernel);
% 	for it = 1:n
% 		for jt=1:n
% 			if disc.Lambda(1,it,it,1) == disc.Lambda(1,jt,jt,1)
% 				test.K.bc.zzeta.numericSolution(:, it, jt, :) = test.K.zEqualZeta(:, it, jt, :);
% 				test.K.bc.zzeta.integralSolution(:, it, jt, :) = reshape(K(1, 1, it, jt, :),1,1,1,disc.ntKernel) .*sqrt(disc.Lambda(1, it, it) ./ disc.Lambda(:, it, it))...
% 					- (1./sqrt(disc.Lambda(:, it, it))) .* ...
% 					numeric.cumtrapz_fast_nDim(disc.z, (disc.A(:, it, jt,:) + mu(:,it, jt,:)) ./ (2*sqrt(disc.Lambda(:, it, it))));
% 			end
% 		end
% 	end
	
	fprintf(['\nAbsolut average of K-PDE-error: ', ...
		num2str(mean(abs(test.K.pde(:)))), '\n']);
% 	fprintf(['Absolut average of K-BC-error at zeta=0: ', ...
% 		num2str(mean(abs(test.K.bc.zeta0.value(:)))), '\n']);
% 	fprintf(['Absolut average of Kdzeta-BC-error at zeta=0: ', ...
% 		num2str(mean(abs(test.K.bc.zeta0.dzeta(:)))), '\n']);
% 	fprintf(['Absolut average of upper triangular elements of A0_tilde: ', ...
% 		num2str(test.A0_tilde_upper), '\n']);
% 	fprintf(['Absolut average of K-BC-error at z=zeta of algebraic bc: ', ...
% 		num2str(mean(abs(test.K.bc.zzeta.algebraic(:)))), '\n']);
% 	fprintf(['Absolut average of K-BC-error at z=zeta of pde bc: ', ...
% 		num2str(mean(abs(test.K.bc.zzeta.pde(:)))), '\n']);
% 	fprintf(['Absolut average of K-BC-error at z=zeta of solution of pde and\n', ...
% 		'        successive approximation are compared: ', ...
% 		num2str(mean(abs(test.K.bc.zzeta.numericSolution(:)-test.K.bc.zzeta.integralSolution(:)))), '\n\n']);
	
	%% plots?
	if plotResult
		dps.hyperbolic.plot.matrix_4D(test.K.pde, 'hide_upper_triangle', false,'title','K-PDE');
		dps.hyperbolic.plot.matrix_4D(test.G, 'hide_upper_triangle', false,'title','G-PDE via H');
% 		dps.hyperbolic.plot.matrix_4D(test.GAlt, 'hide_upper_triangle', false,'title','G-PDE via J');
% 		dps.hyperbolic.plot.matrix_4D(test.GAltAlt, 'hide_upper_triangle', false,'title','G-PDE via ai and aj');
		dps.hyperbolic.plot.matrix_3D(test.K.bc.zeta1, 'title', 'K-BC at zeta = 1');
		
		
		figure('Name','testG')
		id=1;
		for i=1:n
			for j=1:2*n
				subplot(n,2*n,id);
				surf(disc.zKernel,disc.zKernel,test.G(:,:,i,j,1).');
				hold on
				surf(disc.zKernel,disc.zKernel,test.GAlt(:,:,i,j,1).');
				id = id+1;
			end
		end

% 		dps.hyperbolic.plot.matrix_3D(test.K.bc.zeta0.value, 'title', 'K-BC at zeta = 0');
% 		dps.hyperbolic.plot.matrix_3D(test.K.bc.zeta0.dzeta, 'title', 'Kdzeta-BC at zeta = 0');
% 		dps.hyperbolic.plot.matrix_3D(test.K.bc.zzeta.algebraic, 'title', 'K-BC algebraic at zeta = z');
% 		if size(test.K.bc.zzeta.pde,4)>1
% 			dps.hyperbolic.plot.matrix_4D(permute(test.K.bc.zzeta.pde,[1 4 2 3]), 'title', 'K-BC pde at zeta = z');
% 		else
% 			dps.hyperbolic.plot.matrix_3D(test.K.bc.zzeta.pde, 'title', 'K-BC pde at zeta = z');
% 		end
% 		dps.hyperbolic.plot.matrix_3D(test.K.bc.zzeta.integralSolution-test.K.bc.zzeta.numericSolution, 'title', 'K-BC integral-fixpoint solution at zeta = z');
	end
end