classdef DecouplingKernel
	%DecouplingKernel 
	%   only contains the cell array of the kernel elements but is needed
	%   to define the plus function for the cell array.
	
	% created on 20.05.202 by Simon Kerschbaum as a copy of ppide_kernel
	
	
	properties
		value = cell(2,2); % kernel elements K,G,H
		zdisc; % spatial discretization
		tDisc; % temporal discretization
		iteration=1; % number of iterations (plus one) used for fixed-point iteration
		transformed = 0;
		checked_PDE = 0;
		checked_boundaries = 0;
		inverted = 0;
		% resulting target system couplings!
		a0_til;                 % coupling matrix state feedback
		a0_neu;					% neumann coupling matrix needed for folding
		KI;
		zInterp    % cell arrarys zInterp{i,j,k,l} wich contain the interpolants to interpolate 
		zetaInterp % zMat, which is defined over xi_ij, eta_ij over xi_kl, eta_kl.
% 		xiInterp
% 		etaInterp
		xiMatXi    % cell array xiMatXi{i,j,k,l} stores the transformation of the coordinates 
				   % xi_kl(xi_ij,eta_ij)
		etaMatXi   % xi_ij,eta_ij into the coordinates xi_kl,eta_kl.
				   % eta_kl(xi_ij,eta_ij)
		aimjMat    % cell array that stores a_i-a_j discretized over xi,eta
		Azeta      % Azeta{i,j,k,j} = Aij(zeta(xi_kj,eta_kj))
		MuZ		   % MuZ{i,j,i,k} =  muij(z(xi_ik,eta_ik))
		lambdaZeta % lambdaZeta{k,i,j} = lambda_k(zeta(xi_ij,eta_ij))
		lambdaZ    % lambdaZ{k,i,j} = lambda_k(zeta(xi_ij,eta_ij))
		
		% parameters of the PDE (resulting from folding-Kernel)
		A0lrzXiEta
		a0lrZeta     % a0(zeta,i,j,t)
		A0lzXiEta
		a0lZeta     % a0(zeta,i,j,t)
		
		A1lrzXiEta
		a1lrZeta     % a1(zeta,i,j,t)
		A1lzXiEta
		a1lZeta     % a1(zeta,i,j,t)
		
		A1lrDiffzXiEta
		a1lrDiffZeta % a1'(zeta,i,j,t)
		
		avgTime    % average time required for one fixed-point iteration
	end
	
	methods
		plot(obj,var,varargin);
	end
	
	methods
		% [xi,eta] = convert_xi(kernel,i,j,xi_kl,eta_kl,k,l); % Evtl noch Eigenschaften des Objekts �bergeben f�r Speed
		%CONVERT_XI convert xi_coordinates from kl to ij representation
		% [eta,xi] = convert_eta(kernel,i,j,xi_kl,eta_kl,k,l);
		%CONVERT_ETA convert eta_coordinates from kl to ij representation
		GH = init_kernel_elementsDecoupling(obj,ppde,zdisc);
		kernel = refine(obj,factor);
		obj = transform_kernel(obj,ppde,sortVec);
		KI = invert_kernel(obj, allowNan);
		GH = getFixedPointStartDecoupling(obj,ppide,sortVec);
		test = checkDecouplingKernel(controllerKernel, ppide, plotResult);
		[err1, err2, errDeta, errGeta, test, a0TilCanon, devA0Til,A0TilEverywhere,err1All] = checkDecouplingBC(kernel,ppide);
		
		
		function obj = plus(a,b)
			% Elementwise addition of kernel elements
			obj = a; % Datenuebernahme
			for i=1:size(obj.value,1)
				for j=1:size(obj.value,2)
					obj.value{i,j} =  a.value{i,j}+b.value{i,j};
					obj.iteration = a.iteration+b.iteration;
				end
			end
		end
		
		function obj = abs(kernel)
			% Elementwise abs of the kernel elements
			obj = kernel; %Datenuebernahme
			for i=1:size(obj.value,1)
				for j=1:size(obj.value,2)
					obj.value{i,j}.G =  abs(kernel.value{i,j}.G);
					obj.value{i,j}.H =  abs(kernel.value{i,j}.H);
					obj.value{i,j}.J =  abs(kernel.value{i,j}.J);
				end
			end			
		end
		
		function m = max(kernel)
			% max of the kernel values
			mG = 0;
			mH = 0;
			mJ = 0;
			for i=1:size(kernel.value,1)
				for j=1:size(kernel.value,2)
					mGij =  max(kernel.value{i,j}.G(:));
					mHij =  max(kernel.value{i,j}.H(:));
					mJij =  max(kernel.value{i,j}.J(:));
					if mGij >= mG
						mG = mGij;
					end
					if mHij >= mH
						mH = mHij;
					end
					if mJij >= mJ
						mJ = mJij;
					end
				end
			end
			m = [mG mH mJ];
		end
		
		
		% constructor:
		function obj = DecouplingKernel(ppide,zdisc,A1Diff,sortVec)
			tic;
			fprintf('  Initializing DecouplingKernel...')
			% kernel = ppide_kernel(ppide,zdisc) 
			% create new ppide_kernel object for the object ppide of class
			% ppide_sys with the spatial discretisation zdisc
			import numeric.*
			import misc.*
			obj.value = obj.init_kernel_elementsDecoupling(ppide,zdisc);
			obj.zdisc = zdisc;
			
			% ppide is the folded representation for decoupling.
			% n is the original system order. Kernel has dimension n x 2n.
			n = size(ppide.l,2)/2;
			zInterp = cell(n,2*n,n,2*n);
			zetaInterp = cell(n,2*n,n,2*n);
% 			xiInterp = cell(n,n,n,n,size(ppide.l,4));
% 			etaInterp = cell(n,n,n,n,size(ppide.l,4));
			xiMatXi = cell(n,2*n,n,2*n);
			etaMatXi = cell(n,2*n,n,2*n);
			numtL = 1; % no need to rewrite everything.
			% Problem: only the triangular is existing for both areas.
			% --> xiMatXi may only transform areas which exists for both areas.
			% --> check that zMat only contains values in the triangular and check that other
			% values of this matrix are nowhere needed.
% 			error('Check that zMat only contains the values in the triangular and that other values are not needed!')
			for i=1:n
				for j=1:2*n
					for k=1:n
						for l=1:2*n
							xiMatXiTemp = obj.value{k,l}.xiMatInterp.eval(repmat(obj.value{i,j}.zMat(:),numtL,1),repmat(obj.value{i,j}.zetaMat(:),numtL,1),...
								reshape(repmat(1:numtL,numel(obj.value{i,j}.zMat),1),[numtL*numel(obj.value{i,j}.zMat),1]));
							% xi_kl(xi_ij,eta_ij)
							xiMatXi{i,j,k,l} = reshape(xiMatXiTemp,size(obj.value{i,j}.zMat,1),size(obj.value{i,j}.zMat,2),numtL);
							etaMatXiTemp = obj.value{k,l}.etaMatInterp.eval(repmat(obj.value{i,j}.zMat(:),numtL,1),repmat(obj.value{i,j}.zetaMat(:),numtL,1),...
								reshape(repmat(1:numtL,numel(obj.value{i,j}.zMat),1),[numtL*numel(obj.value{i,j}.zMat),1]));
							% eta_kl(xi_ij,eta_ij)
							etaMatXi{i,j,k,l} = reshape(etaMatXiTemp,size(obj.value{i,j}.zMat,1),size(obj.value{i,j}.zMat,2),numtL);
						end
					end
				end
			end
			% PROBLEM: INTERPOLATIONEN UEBER DIE SPARSE-COORDINATEN MACHT VLLT WEGNI SINN, VA.
			% WEGEN DEN NaNs...
			obj.zInterp = zInterp;
			obj.zetaInterp = zetaInterp;
			obj.xiMatXi = xiMatXi;
			obj.etaMatXi = etaMatXi;
			% store discretized versions of A, Lambda, and Mu evaluated over each grid xi, eta:
			% resample time resolution:
			%%% Interpolate system parameters to kernel resolution
			pars.lambda_disc = ppide.l_disc;  %translate_to_grid(,zdisc);
			% local couplig matrices needed for the decoupling. They only contain values for
			% j<=n
			pars.a0lr_disc = ppide.a_0(:,n+1:end,1:n,:); % is A0_lr (n times n)
			pars.c0lr_disc = ppide.c_0(:,n+1:end,1:n,:); % is A1_lr (n times n)
			pars.a0l_disc = ppide.a_0(:,1:n,1:n,:); % is A0_l (n times n)
			pars.c0l_disc = ppide.c_0(:,1:n,1:n,:); % is A1_l (n times n)
			if nargin < 3
				pars.A1Diff = numeric.diff_num(zdisc,pars.c0lr_disc,1);
			else
				pars.A1Diff = A1Diff(:,n+1:end,1:n,:); % is A1Diff_lr (n times n)
			end
			% all time-dependent coefficients need to be resampled in time-direction
			% (actually not neede as long as both kernels have same time resolution)
			varlist = {'a0lr_disc','c0lr_disc','A1Diff','a0l_disc','c0l_disc'};
			obj.tDisc = ppide.ctrl_pars.tDiscKernel;
			for i=1:length(varlist)
				if size(pars.(varlist{i}),4)>1
					pars.(varlist{i}) = permute(misc.translate_to_grid(permute(pars.(varlist{i}),[4 1 2 3]),obj.tDisc),[2 3 4 1]);
				end
			end

			% preallocate
			obj.A0lrzXiEta = cell(n,n,n);
			obj.A1lrzXiEta = cell(n,n,n);
			obj.A1lrDiffzXiEta = cell(n,n,n);
			obj.A0lzXiEta = cell(n,n,n);
			obj.A1lzXiEta = cell(n,n,n);
			obj.lambdaZeta = cell(2*n,2*n,2*n);
			obj.lambdaZ = cell(2*n,2*n,2*n);
			% store interpolants of A0 and A1 for integrations in different coordinate systems
			A0lrInterp = numeric.interpolant({zdisc,1:n,1:n,1:size(pars.a0lr_disc,4)},pars.a0lr_disc);
			A1lrInterp = numeric.interpolant({zdisc,1:n,1:n,1:size(pars.c0lr_disc,4)},pars.c0lr_disc);
			A0lInterp = numeric.interpolant({zdisc,1:n,1:n,1:size(pars.a0l_disc,4)},pars.a0l_disc);
			A1lInterp = numeric.interpolant({zdisc,1:n,1:n,1:size(pars.c0l_disc,4)},pars.c0l_disc);
			A1DiffInterp = numeric.interpolant({zdisc,1:n,1:n,1:size(pars.A1Diff,4)},pars.A1Diff);
			LambdaInterp = numeric.interpolant({linspace(0,1,ppide.ndiscPars),1:2*n,1:2*n,1:size(pars.lambda_disc,4)},pars.lambda_disc);
			% Evaluate Interpolant to get A0 A1 on each k,l grid
			kernel = obj;
			for i = 1:n
				for j=1:2*n
					if j<=n
						leftInd = j;
					else
						leftInd = j-n;
					end
					for k=1:n
						% for A0, A1, the column index is constant
						A0lrzTemp = A0lrInterp.eval({kernel.value{k,j}.zMat(:),i,leftInd,1:size(pars.a0lr_disc,4)});
						A1lrzTemp = A1lrInterp.eval({kernel.value{k,j}.zMat(:),i,leftInd,1:size(pars.c0lr_disc,4)});
						A0lzTemp = A0lInterp.eval({kernel.value{k,j}.zMat(:),i,leftInd,1:size(pars.a0l_disc,4)});
						A1lzTemp = A1lInterp.eval({kernel.value{k,j}.zMat(:),i,leftInd,1:size(pars.c0l_disc,4)});
						A1DiffzTemp = A1DiffInterp.eval({kernel.value{k,j}.zMat(:),i,leftInd,1:size(pars.A1Diff,4)});
						% AileftInd(z(xi_kj,eta_kj))
						obj.A0lrzXiEta{i,k,j} = reshape(A0lrzTemp,size(kernel.value{k,j}.xi,1),size(kernel.value{k,j}.eta,1),size(pars.a0lr_disc,4));
						obj.A1lrzXiEta{i,k,j} = reshape(A1lrzTemp,size(kernel.value{k,j}.xi,1),size(kernel.value{k,j}.eta,1),size(pars.c0lr_disc,4));
						obj.A0lzXiEta{i,k,j} = reshape(A0lzTemp,size(kernel.value{k,j}.xi,1),size(kernel.value{k,j}.eta,1),size(pars.a0l_disc,4));
						obj.A1lzXiEta{i,k,j} = reshape(A1lzTemp,size(kernel.value{k,j}.xi,1),size(kernel.value{k,j}.eta,1),size(pars.c0l_disc,4));
						obj.A1lrDiffzXiEta{i,k,j} = reshape(A1DiffzTemp,size(kernel.value{k,j}.xi,1),size(kernel.value{k,j}.eta,1),size(pars.A1Diff,4));
					end
					% lambda needs to be evaluated for a single index but for all 2n
					for k=1:2*n
						lambdaZeta = LambdaInterp.eval({kernel.value{i,j}.zetaMat(:),k,k});
						lambdaZ = LambdaInterp.eval({kernel.value{i,j}.zMat(:),k,k});
						% lambda_k(zeta(xi_ij,eta_ij))
						obj.lambdaZeta{k,i,j} = reshape(lambdaZeta,size(kernel.value{i,j}.xi,1),size(kernel.value{i,j}.eta,1));
						obj.lambdaZ{k,i,j} = reshape(lambdaZ,size(kernel.value{i,j}.xi,1),size(kernel.value{i,j}.eta,1));
					end
				end
			end
			% a0 needs to be integrated over zeta in use_F_ppides o no interpolation on the
			% xi-eta-grid is required here. Instead, it is simply stored so the resampling isnt
			% needed in each iteration step.
			obj.a0lrZeta = misc.translate_to_grid(pars.a0lr_disc,zdisc);
			obj.a1lrZeta = misc.translate_to_grid(pars.c0lr_disc,zdisc);
			obj.a0lZeta = misc.translate_to_grid(pars.a0l_disc,zdisc);
			obj.a1lZeta = misc.translate_to_grid(pars.c0l_disc,zdisc);
			obj.a1lrDiffZeta = misc.translate_to_grid(pars.A1Diff,zdisc);
			
% 			obj.xiInterp = xiInterp;
% 			obj.etaInterp = etaInterp;
			obj.value = obj.getFixedPointStartDecoupling(ppide,sortVec);

			time=toc;
			fprintf('Finished! (%0.2fs)\n',time);
		end
		
		% return K as matrix
		function K = get_K(obj,varname,tIdx)
			% K = get_K(obj,varname,tIdx)
			% tIdx give the dimension of the value of the kernel element that shall be put to
			% the end.
			import misc.*
			import numeric.*
			import dps.parabolic.control.backstepping.*
			% just get K(z,zeta,i,j,t)
			if nargin<2
				K=0;
				n = size(obj.value,1);
				K(1:length(obj.zdisc),1:length(obj.zdisc),n,2*n,1:size(obj.value{1,1}.G,3)) = 0;
				for i=1:n
					for j=1:2*n % create empty dimensions for i,j and move time to end
						K(:,:,i,j,:) = permute(obj.value{i,j}.K,[1 2 4 5 3]);
					end
				end
			else % get a different variable:
				if ~ischar(varname)
					error('varname must be a string!')
				end
				n = size(obj.value,1);
				if ~isprop(obj.value{1,1},varname)
					error([varname ' is no property of ppide_kernel_element!'])
				end
				% get size of the variable
				sizVar = sizeOf(reduce_dimension(obj.value{1,1}.(varname)));
				while sizVar(end)==1
					sizVar = sizVar(1:end-1);
				end
				% number of dimensions of var.
				% Problem: Shall always be z, Rest, n, n, t
				% it is not possible to decide where the variable has its t-dimension, so it
				% must be given in the argument. Should better be implemented with quantity.
				% may be that the time-dimension is one, so it doesnt exit anymore.
				idxVec = 1:length(sizVar);
				if nargin>2 && length(sizVar)>=tIdx
					sizK = [sizVar(idxVec~=tIdx) sizVar(tIdx+1:end) n 2*n sizVar(tIdx)];
				else
					sizK = [sizVar n 2*n];
				end
				K = zeros(sizK);
				if nargin>2 && length(sizVar)>=tIdx
					K_vec = reshape(K,prod(sizVar(idxVec~=tIdx)),n,2*n,sizVar(tIdx));
				else
					K_vec = reshape(K,[],n,2*n);
				end
				if nargin>2 && length(sizVar)>=tIdx
					for i=1:n
						for j=1:2*n
							K_vec(:,i,j,:) = reshape(obj.value{i,j}.(varname),prod(sizVar(idxVec~=tIdx)),1,1,sizVar(tIdx));
						end
					end
				else
					for i=1:n
						for j=1:2*n
							K_vec(:,i,j,:) = reshape(obj.value{i,j}.(varname),[],1);
						end
					end
				end
				K = reshape(K_vec,sizK);
			end
		end
		
	function obj = get_a0_til(obj,ppide,sortVec) % is called with the decouplingPpide
		% get the coupling matrix of the right target system state.
		import misc.*
		import numeric.*
		import dps.parabolic.control.backstepping.*
		if obj.transformed
			K = obj.get_K; % kernel in matrix form
			dzetaK_z_0 = obj.get_K('dzeta_K_z_0',2); % z zeta i j t
			zdisc=obj.zdisc;  % spatial discretization of kernel
			ndisc = length(zdisc); 
			n = size(obj.value,1); % system order		
			numtKernel = size(K,5);
			Kz0 = reshape(K(:,1,:,:,:),ndisc,n,2*n,numtKernel);
			lambdaDisc = translate_to_grid(ppide.l_disc,obj.zdisc);
			lambdaDiffDisc = translate_to_grid(ppide.l_diff,obj.zdisc);
			
			z0 = ppide.ctrl_pars.foldingPoint;
% 			z0Til = z0/(1-z0);
			z0TilLeft = z0/(1-z0);
			z0TilVec = repmat(z0TilLeft,n,1);
			invZ0Til = (1-z0)/z0;
			invZ0TilVec = repmat(invZ0Til,n,1);
			z0TilGes = [z0TilVec invZ0TilVec];
			z0TilSorted = z0TilGes(sortVec);
			z0Til = z0TilSorted(1:n);
			
			% in decoupling ppide, a0 and c0 have already added the "right" part.
			A0lr = ppide.a_0(:,n+1:end,1:n,:);
			A1lr = ppide.c_0(:,n+1:end,1:n,:);
			A0l = ppide.a_0(:,1:n,1:n,:);
			A1l = ppide.c_0(:,1:n,1:n,:);
			
			zetaMat = repmat(zdisc(:).',ndisc,1,numtKernel); %#ok<*PROPLC>
			zetaMatRight = zetaMat;
			zetaMatRight(zdisc(:).' > zdisc(:) & ones(1,1,numtKernel)) = NaN;
			
			A0Til = zeros(ndisc,n,n,numtKernel);
			A1Til = zeros(ndisc,n,n,numtKernel);
			for i=1:n
				for j=1:n
					if lambdaDisc(1,i+n,i+n) < lambdaDisc(1,j+n,j+n)
					% deactivate if to calculate residuum. (attention, coupled)
						if numtKernel > 1
							A0Til(:,i,j,:) = ...
								lambdaDisc(1,j+n,j+n)* dzetaK_z_0(:,i,j+n,:) + ...
								lambdaDisc(1,j,j)* dzetaK_z_0(:,i,j,:) + ...
								lambdaDiffDisc(1,j+n,j+n)*Kz0(:,i,j+n,:) + ...
								lambdaDiffDisc(1,j,j)*Kz0(:,i,j,:) + ...
								A0lr(:,i,j,:)  ...
								- reshape(...
									numeric.trapz_fast_nDim(zetaMatRight,...
										permute(...
											misc.multArray(K(:,:,i,n+1:end,:),... %zeta,t,z,i,1,j
												permute(A0lr(:,:,j,:),[5 1 2 3 4]),4,3,[2 5])...
										,[3 1 2 4 5 6])... z zeta t 1 1 1 
									,2,'equal','ignoreNaN')...% z 1 t	
								,ndisc,1,1,numtKernel)...
								- reshape(...
									numeric.trapz_fast_nDim(zetaMat,...
										permute(...
											misc.multArray(K(:,:,i,1:n,:),... %zeta,t,z,i,1,j
												permute(A0l(:,:,j,:),[5 1 2 3 4]),4,3,[2 5])...
										,[3 1 2 4 5 6])... z zeta t 1 1 1 
									,2,'equal','ignoreNaN')...% z 1 t	
								,ndisc,1,1,numtKernel); 
							A1Til(:,i,j,:) = ...
								z0Til(j) *(...
									- 1/z0Til(j) * lambdaDisc(1,j+n,j+n)* Kz0(:,i,j+n,:) ...
									+ lambdaDisc(1,j,j)* Kz0(:,i,j,:) + ...
									- A1lr(:,i,j,:)  ...
									+ reshape(...
										numeric.trapz_fast_nDim(zetaMatRight,...
											permute(...
												misc.multArray(K(:,:,i,n+1:end,:),... %zeta,t,z,i,1,j
													permute(A1lr(:,:,j,:),[5 1 2 3 4]),4,3,[2 5])...
											,[3 1 2 4 5 6])... z zeta t 1 1 1 
										,2,'equal','ignoreNaN')...% z 1 t	
									,ndisc,1,1,numtKernel)...
									+ reshape(...
										numeric.trapz_fast_nDim(zetaMat,...
											permute(...
												misc.multArray(K(:,:,i,1:n,:),... %zeta,t,z,i,1,j
													permute(A1l(:,:,j,:),[5 1 2 3 4]),4,3,[2 5])...
											,[3 1 2 4 5 6])... z zeta t 1 1 1 
										,2,'equal','ignoreNaN')...% z 1 t	
									,ndisc,1,1,numtKernel)... 
								);
						else
							QA0lr = zeros(size(zetaMatRight));
							PA0lr = zeros(size(zetaMatRight));
							for k=1:n
								QA0lr = QA0lr + K(:,:,i,k+n).* permute(A0lr(:,k,j),[4 1 2 3]);
								PA0lr = PA0lr + K(:,:,i,k).* permute(A0lr(:,k,j),[4 1 2 3]);
							end
							A0Til(:,i,j) = ...
								lambdaDisc(1,j+n,j+n)* dzetaK_z_0(:,i,j+n) + ...
								lambdaDisc(1,j,j)* dzetaK_z_0(:,i,j) + ...
								lambdaDiffDisc(1,j+n,j+n)*Kz0(:,i,j+n) + ...
								lambdaDiffDisc(1,j,j)*Kz0(:,i,j) + ...
								A0lr(:,i,j)  ...
								- reshape(...
									numeric.trapz_fast_nDim(zetaMatRight,...
										permute(...
											misc.multArray(K(:,:,i,n+1:end),... %zeta,z,i,1,j
												permute(A0lr(:,:,j),[4 1 2 3]),4,3,[2])...
										,[2 1 3 4 5])... z zeta 1 1 1 
									,2,'equal','ignoreNaN')...% z 1	
								,ndisc,1,1,numtKernel)...
								- reshape(...
									numeric.trapz_fast_nDim(zetaMat,...
										permute(...
											misc.multArray(K(:,:,i,1:n),... %zeta,t,z,i,1,j
												permute(A0l(:,:,j,:),[4 1 2 3]),4,3,[2])...
										,[2 1 3 4 5])... z zeta t 1 1 1 
									,2,'equal','ignoreNaN')...% z 1 t	
								,ndisc,1,1,numtKernel); 
							A0TilComp(:,i,j) = ...
								lambdaDisc(1,j+n,j+n)* dzetaK_z_0(:,i,j+n) + ...
								lambdaDiffDisc(1,j+n,j+n)*Kz0(:,i,j+n) + ...
								lambdaDisc(1,j,j)* dzetaK_z_0(:,i,j) + ...
								lambdaDiffDisc(1,j,j)*Kz0(:,i,j) + ...
								A0lr(:,i,j)  ...
								- reshape(...
									numeric.trapz_fast_nDim(zetaMatRight,...
										QA0lr...
									,2,'equal','ignoreNaN')...% z 1	
								,ndisc,1,1,numtKernel)...
								- reshape(...
									numeric.trapz_fast_nDim(zetaMat,...
										PA0lr...
									,2,'equal','ignoreNaN')...% z 1 t	
								,ndisc,1,1,numtKernel); 
							A1Til(:,i,j) = ...
								z0Til(j) *(...
									- 1/z0Til(j) * lambdaDisc(1,j+n,j+n)* Kz0(:,i,j+n) ...
									+ lambdaDisc(1,j,j)* Kz0(:,i,j) ...
									- A1lr(:,i,j)  ...
									+ reshape(...
										numeric.trapz_fast_nDim(zetaMatRight,...
											permute(...
												misc.multArray(K(:,:,i,n+1:end),... %zeta,t,z,i,1,j
													permute(A1lr(:,:,j,:),[4 1 2 3]),4,3,[2])...
											,[2 1 3 4 5])... z zeta t 1 1 1 
										,2,'equal','ignoreNaN')...% z 1 t	
									,ndisc,1,1,numtKernel)...
									+ reshape(...
										numeric.trapz_fast_nDim(zetaMat,...
											permute(...
												misc.multArray(K(:,:,i,1:n,:),... %zeta,t,z,i,1,j
													permute(A1l(:,:,j,:),[4 1 2 3]),4,3,[2])...
											,[2 1 3 4 5])... z zeta t 1 1 1 
										,2,'equal','ignoreNaN')...% z 1 t	
									,ndisc,1,1,numtKernel)... 
								);
						end
					end
				end
			end
			obj.a0_til = A0Til;
			obj.a0_neu = A1Til;
		else
			warning('kernel has not yet been transformed, no ao_til has been computed!')
		end
	end
	
	end
	
end

