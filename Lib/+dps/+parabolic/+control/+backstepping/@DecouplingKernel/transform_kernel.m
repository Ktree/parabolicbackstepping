function kernel_new = transform_kernel(kernel,ppide,sortVec)
%TRANSFORM_KERNEL transform ppide kernel into original z-zeta koordinates
% and calculate derivatives analytically

% created on 16.06.2020 by Simon Kerschbaum
% based on ppide_kernel.transform_kernel

import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
if kernel.transformed == 0
	timer = tic;
	fprintf('  Transforming kernel:\n')

	n = size(kernel.value,1);
	m = size(kernel.value,2);
	kernel_new = kernel;
	if ~isa(kernel.value{1,1},'ppide_kernel_element')
		warning('Kernel not yet initialized!')
		return
	end
	zdisc = kernel.zdisc; % for numerical comparison needed
	ndisc = numel(zdisc);
	numtKernel = size(kernel.value{1,1}.G,3);
	tVec = linspace(0,1,numtKernel);

	% transform each element and get time-derivative!
	for i =1:n
		for j=1:m
			kernel_new.value{i,j}.K = kernel.value{i,j}.transform_kernel_element(1);
			kernel_new.value{i,j}.K_t = diff_num(tVec,kernel_new.value{i,j}.K,3);
			%
			kem = kernel_new.value{i,j};
			s = kem.s;
			a = (s+1)/2 *kem.phi_i(end,:) - (s-1)/2*kem.phi_j(end,:); % a Ortsbereich
			kernel_new.value{i,j}.G_t = diff_num(tVec,kernel.value{i,j}.G,3);
			% write values outside the spatial area
			kernel_new.value{i,j}.G_t(~(acc(kem.eta.')>=acc(kem.eta_l) & acc(kem.eta.') <= min(acc(kem.xi),acc(2*a-kem.xi)))) = NaN;
			kernel_new.value{i,j}.G_t = misc.writeToOutside(kernel_new.value{i,j}.G_t);
		end
	end
	
	% derivatives:
	if ~ppide.ctrl_pars.eliminate_convection 	
		% for the decoupling transformation, J is always calculated in the decoupling
		% transformation.
		% for debugging! numerical comparison!
% 		for i=1:n
% 			for j=1:m
% 				G_eta.value{i,j} = kernel.value{i,j}.J;
% 			end
% 		end
		% comment in for debug purposes!
		% checked after implementation of integral term. Fits (at least time constant)
% 		GH=kernel.value;
% 			for i=1:n
% 				for j=1:n
% 					s = GH{i,j}.s;
% 					a = (s+1)/2 *GH{i,j}.phi_i(end,:) - (s-1)/2*GH{i,j}.phi_j(end,:); % a Ortsbereich
% 					G_eta_fin{i,j} = zeros(size(kernel.value{i,j}.xi,1),size(kernel.value{i,j}.eta,1));
% 					for xi_idx=1:size(kernel.value{i,j}.xi,1)
% 						G_eta_fin{i,j}(xi_idx,:) = numeric.diff_num(kernel.value{i,j}.eta(:,1),kernel.value{i,j}.G(xi_idx,:,1),2);
% 					end
% 					G_etaNaN = G_eta;
% 					delta{i,j} = G_eta.value{i,j}(:,:,1)-G_eta_fin{i,j};
% 					
% 					delta{i,j}(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi)))) = NaN;
% 					G_eta_fin{i,j}(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi)))) = NaN;
% 					G_etaNaN.value{i,j}(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi)))) = NaN;
% % 						delta_GH{i,j}.(varnames{nameIdx}) = misc.writeToOutside(delta_GH{i,j}.(varnames{nameIdx}));
% 					
% 					figure('Name','G')
% 					surf(kernel_new.value{i,j}.G(:,:,1).')
% 					figure('Name','H')
% 					surf(kernel_new.value{i,j}.H(:,:,1).')
% 					figure('Name',['G_eta ' num2str(i) num2str(j)])
% 					subplot(1,3,1)
% 					surf(G_etaNaN.value{i,j}(:,:,1).','edgeColor','none')
% 					subplot(1,3,2)
% 					surf(G_eta_fin{i,j}.','LineStyle', 'none')
% 					subplot(1,3,3)
% 					surf(delta{i,j}.','edgeColor','none')
% 				end
% 			end
		for i=1:n
			for j=1:m
				el = kernel_new.value{i,j};
				s = el.s;
				%%% numerical calucation of derivatives. For comparison. Comment in if needed
% 						dzetaK = zeros(ndisc,ndisc,numtKernel);
% 						dzK = zeros(ndisc,ndisc,numtKernel);
% 						if j>n
% 							for zIdx = 2:ndisc
% 								dzetaK(zIdx,1:zIdx,:) = diff_num(zdisc(1:zIdx),kernel_new.value{i,j}.K(zIdx,1:zIdx,:),2);
% 							end
% 							dzetaK(1,1,:) = dzetaK(2,1,:);
% 							for zetaIdx = 1:ndisc-1
% 								dzK(zetaIdx:end,zetaIdx,:) = diff_num(zdisc(zetaIdx:end),kernel_new.value{i,j}.K(zetaIdx:end,zetaIdx,:),1);
% 							end
% 							dzK(end,end,:) = dzK(end,end-1,:);
% 						else
% 							dzetaK(2:end,1:end,:) = diff_num(zdisc,kernel_new.value{i,j}.K(2:end,:,:),2);
% 							dzetaK(1,1,:) = dzetaK(2,1,:);
% 							dzK(:,1:end-1,:) = diff_num(zdisc,kernel_new.value{i,j}.K(:,1:end-1,:),1);
% 							dzK(end,end,:) = dzK(end,end-1,:);
% 						end
				%%% analytical calculation
				% Kz = s/sqrt(l_i(z))/l_j(zeta)*H +
				%   1/sqrt(l_i(z))/l_j(zeta)*G_eta
				% Kzeta = -l_j_diff(zeta)/l_j(zeta)*K + s/l_j(zeta)^(3/2)*H -
				%   1/l_j(zeta)^(3/2)*G_eta
				
				xiMat = el.xiMat;
				etaMat = el.etaMat;

				% check that there are non NaN!
				HInterp = numeric.interpolant({el.xi,el.eta,1:numtKernel},el.H);
				JInterp = numeric.interpolant({el.xi,el.eta,1:numtKernel},el.J);
				% H in kernel is total H and so is J
				Hzzeta = reshape(...
					HInterp.eval(repmat(xiMat(:),numtKernel,1),repmat(etaMat(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMat),1),[numtKernel*numel(xiMat),1]))...
				,size(xiMat,1),size(xiMat,2),numtKernel);
				Jzzeta = reshape(...
					JInterp.eval(repmat(xiMat(:),numtKernel,1),repmat(etaMat(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMat),1),[numtKernel*numel(xiMat),1]))...
				,size(xiMat,1),size(xiMat,2),numtKernel);
				dzK_an = ...
					+ s./sqrt(el.lambda_i(:))./(el.lambda_j(:).').*Hzzeta...
					+ 1./sqrt(el.lambda_i(:))./(el.lambda_j(:).').*Jzzeta;
				dzetaK_an = ...
					-(el.lambda_j_diff(:).')./(el.lambda_j(:).').*el.K...
					+ s./((el.lambda_j(:).').^(3/2)).*Hzzeta...
					- 1./((el.lambda_j(:).').^(3/2)).*Jzzeta;
				
				kernel_new.value{i,j}.dzeta_K_z_0 = permute(dzetaK_an(:,1,:),[1 3 2]);
				kernel_new.value{i,j}.dz_K_1_zeta = permute(dzK_an(end,:,:),[2 3 1]);
				kernel_new.value{i,j}.dz_K = dzK_an;
				kernel_new.value{i,j}.dzeta_K = dzetaK_an;
				% Plot comparison (comment in with calculation for debug purposes!)
% 				diffZ = dzK - dzK_an;
% 				diffZeta = dzetaK - dzetaK_an;
% 				figure('Name',['Diff dzK:' num2str(i) num2str(j)])
% 				subplot(2,1,1)
% 				surf(diffZ(:,:,1).');
% 				subplot(212)
% 				hold on
% 				surf(dzK(:,:,1).')
% 				surf(dzK_an(:,:,1).')
% 				figure('Name',['Diff dzetaK:' num2str(i) num2str(j)])
% 				subplot(211)
% 				surf(diffZeta(:,:,1).');
% 				subplot(212)
% 				hold on
% 				surf(dzetaK(:,:,1).')
% 				surf(dzetaK_an(:,:,1).')
			end % for j=1:n
		end % for i=1:n
	else
		error('eliminate_convection must be 0! for the decoupling kernel!')
	end % eliminate_convection =1
	timeelapsed = toc(timer); 
	fprintf(['  finished transformation! (' num2str(round(timeelapsed,2)) ' s)\n'])

	kernel_new.transformed = 1;
	
	kernel_new = kernel_new.get_a0_til(ppide,sortVec);
	
	warning('No inverse kernel is calculated so far!')
% 	kernel_new.KI = kernel_new.invert_kernel;
% 	kernel_new.inverted=1;
else
	disp('kernel already transformed')
	kernel_new=kernel;
end
	



end % function

