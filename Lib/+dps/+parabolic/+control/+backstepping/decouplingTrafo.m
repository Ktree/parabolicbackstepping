function x_til = decouplingTrafo(x,k,zdisc_x,zdisc_k)
%  compute the decoupling transformation
%    x_til(t,z) = x(t,z) - \int_0^1 E1^T k(z,zeta,t) E1^t x(t,zeta) d zeta 
%                        - \int_0^z E2^T k(z,zeta,t) E2^t x(t,zeta) d zeta
%    for signal x(t,z),
%    which is a combined fredholm-backstepping transformation.
%
% 
%    x_til = TC(x,k,zdisc_x,zdisc_k)
%      computes the transformation for signal x(t,z) which is
%      discretized at zdisc_x, with the corresponding transformation kernel
%      k(z,zeta,t) which is discretized width zdisc_k in both directions.
%
%      The kernel-array may be constant in time, k(z,zeta,i,j) and x may have multple rows as
%      differnent timesteps. If the kernel is time-varying, k(z,zeta,i,j,t), size(k,5) must
%      equal size(x,1).
%
%      To compute the inverse-control-backstepping transformation or one
%      of the observer transformations you just have to pass the
%      corresponding kernel with the respective sign.
%
%   INPUT PARAMETRS:
%     ARRAY            x : signal x(t,z,i,idx) (may contain multiple rows of
%                          spatial signals, multiple indices for the different states, and multiple further dimensions.)
%     ARRAY            k : transformation kernel k(z,zeta,i,j,t)
%     VECTOR     zdisc_x : spatial discretization of signal x
%     VECTOR     zdisc_k : spatial discretization of kernel k in both
%                          directions
%
%   OUTPUT PARAMETRS:
%     MATRIX       x_til : signal x_til(t,z,i) (may contain multiple rows of
%                          spatial signals and multiple indices)

% x ist Zustandsverlauf an diskretisierungspunkten zdisc!
% nach unten Zeit, nach rechts Ort!
% --> Transformation muss nach rechts gemacht werden und f�r jede Zeit!

% Created on 16.06.2020 by Simon Kerschbaum based on Tc


%%%%%%%%%%%%%%%%%%%%%%%%%
% 1. Einige �berpr�fungen
%%%%%%%%%%%%%%%%%%%%%%%%%
import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
if isempty(x)
	x_til = x;
	return
end
lx = length(zdisc_x);
	if lx~= size(x,2)
		error('The signal x must be discretized at zdisc_x. Size(x,2) must equal length(zdisc_x)!');
	end

	if length(zdisc_k)~=size(k,1) ||length(zdisc_k)~=size(k,2)
		error('The kernel k must be discretized at zdisc_k in both spatial directions!');
	end
	
	if 2*size(k,3)~=size(k,4)
		error('The kernel matrix k must be n x 2n!')
	end
	
	if size(k,4) ~= size(x,3)
		error('The kernel matrix must have the same order as the state vector!')
	end
	
	if ndims(k)>4 && size(x,1)~= size(k,5)
		if size(x,1) ~= 1 % x can also be time-constant!
			error('If the kernel is time-varying, the number of timesteps must equal the number of timesteps in x!')
		end
	end
sizX = size(x);
if ndims(x)>4	
	% all further dimensions are collapsed
	xResh = reshape(x,sizX(1),sizX(2),sizX(3),[]);	
else
	xResh = x;
end
%%%%%%%%%%%%%%%%%%%
% 2. Transformation
%%%%%%%%%%%%%%%%%%%
% ACHTUNG: Regelungskern k(z,zeta,i,j) ist wahrscheinlich nicht mit zdisc
% diskretisiert! Entweder zu hoch, oder auch zu niedrig!
% Das heisst, es muss immer der richtige Index herausgesucht werden! Oder
% es wird mit interp1 versucht! Beides moeglich! Testen!
% Problem: bei Interpolation kann nicht einfach der ganze 2D-Bereich
% verwendet werden, weil k links oberhalb der Hauptdiagonalen NaN stehen
% hat. Problem wird umgangen, indem hier einfach die Werte der
% Hauptdiagonalen geschrieben werden:
% k_orig=k;
% for z_idx=1:length(zdisc_k)-1
% 	k(z_idx,z_idx+1,:,:) = k(z_idx,z_idx,:,:);
% end 
k = misc.writeToOutside(k);

numTK = size(k,5);
numTX = size(x,1);
if numTK>1 && numTX>1 && numTK~=numTX
	error('When both x and K are time-dependent, the time dimension must have the same resolution!')
end
n = size(k,3);
xLeft = xResh(:,:,1:n,:,:);
xRight = xResh(:,:,n+1:end,:,:);

KGrIp = numeric.interpolant({zdisc_k,zdisc_k,1:size(k,3),1:2*n,1:numTK},k);
KRes = KGrIp.evaluate(zdisc_x,zdisc_x,1:size(k,3),1:2*n,1:numTK);
P = KRes(:,:,:,1:n,:);
Q = KRes(:,:,:,n+1:end,:);

lt = max(numTK,numTX); % which ever is time-dependent
zDiscVec = zdisc_x(:).';
ndisc = numel(zdisc_x);
zetaDiscVec = reshape(zdisc_x,1,1,ndisc);
zMatExt = repmat(zetaDiscVec,lt,ndisc,1,n,size(xResh,4));
% the trick is that zeta is only valid in the area, where it is between 0 and
% z! That means the integration is only perfomed in that area. Therefore, no
% cumtrapz is needed because the valid area direcly is the range 0 --> z
zMatLeft = zMatExt;
zMatExt( (zetaDiscVec > zDiscVec) & ones(lt,1,1,1,1) & ones(1,ndisc,1,1,1) & ones(1,1,1,n,1) & ones(1,1,1,1,size(xResh,4)) ) = NaN;

	
% z zeta i j t
Qx = zeros(lt,ndisc,ndisc,size(KRes,3),size(xResh,4));
Px = zeros(lt,ndisc,ndisc,size(KRes,3),size(xResh,4));
% xResh: t,zeta,i,jwith Rest
xRightPerm = permute(xRight,[1 5 2 3 4]);
xLeftPerm = permute(xLeft,[1 5 2 3 4]);
% to t z(new) zeta i j
% z zeta i j t is kernel original
QPerm = permute(Q,[5 1 2 3 4]);
PPerm = permute(P,[5 1 2 3 4]);
% to t z zeta i j
% manual multiplication is slightly faster than multArray
for kt=1:n
	Qik = reshape(QPerm(:,:,:,:,kt),numTK,ndisc,ndisc,[],1);
	Pik = reshape(PPerm(:,:,:,:,kt),numTK,ndisc,ndisc,[],1);
	xRightk = reshape(xRightPerm(:,:,:,kt,:),numTX,1,ndisc,1,[]);
	xLeftk = reshape(xLeftPerm(:,:,:,kt,:),numTX,1,ndisc,1,[]);
	Qx = Qx + Qik.*xRightk;
	Px = Px + Pik.*xLeftk;
end
% this would be the multArray option, but is slightly slower. 
% 	Kx = permute(misc.multArray(KPerm,xPerm,5,4,[1 3]),[1 3 2 4 6 5]);

% t z 1 i j to t z i j 1 
QxInt = permute(numeric.trapz_fast_nDim(zMatExt,Qx,3,'equal','ignoreNaN'),[1 2 4 5 3]);
PxInt = permute(numeric.trapz_fast_nDim(zMatLeft,Px,3,'equal','ignoreNaN'),[1 2 4 5 3]);
x_RighttilResh = xRight - QxInt - PxInt;
x_LefttilResh = xLeft;
x_tilResh = cat(3,x_LefttilResh,x_RighttilResh);
if ndims(x)>4
	x_til = reshape(x_tilResh,sizeX);
else
	x_til = x_tilResh;
end

end %function



