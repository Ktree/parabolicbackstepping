function [G, H] = limit_to_valid(kernel_element)
%LIMIT_TO_VALID Select only valid values out of G
%   Should only be used in the end, before plotting.


if kernel_element.j>=kernel_element.i
	s = 1;
else
	s = -1;
end
a = (s+1)/2 *kernel_element.phi_i(end) - (s-1)/2*kernel_element.phi_j(end); % a Ortsbereich


xi_disc = kernel_element.xi;
eta_disc = kernel_element.eta;

% alle Elemente rausschmeisen, die nicht gueltig sind:
G = kernel_element.G;
H = kernel_element.H;
for xi_idx = 1:length(xi_disc)
	xi = xi_disc(xi_idx);
	for eta_idx = 1:length(eta_disc)
		eta = eta_disc(eta_idx);
% 		if eta > -1
% 			test=1;
% 		end
		if eta > min(xi, 2*a-xi) || eta < kernel_element.eta_l(xi_idx)
			G(xi_idx,eta_idx) = NaN;
			H(xi_idx,eta_idx) = NaN;
		end
	end
end


end

