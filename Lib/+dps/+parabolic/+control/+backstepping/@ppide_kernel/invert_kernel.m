function [ KI ] = invert_kernel( kernel, allowNan )
%INVERT_KERNEL Compute the inverse kernel Kappa_I using the reciprocity
%relation and fixpoint iteration

% created on 07.11.2016 by Simon Kerschbaum
% modified on 15.03.2018 by SK:
%  * moved to class function of ppide_kernel and use kernel.get_K.

% TODO: Parametrisierbarkeit!

import misc.*
import numeric.*
import dps.parabolic.control.backstepping.*
import dps.parabolic.tool.*

ti = tic;
fprintf('  Inverting kernel...\n')
if nargin == 1
	allowNan = true; % default
end
K = kernel.get_K;
zdisc=kernel.zdisc;
use_F_handle = @(KI) use_F_K(K, KI, zdisc, allowNan);
KI = dps.parabolic.tool.fixpoint_iteration(K,use_F_handle,1,200,1e-8);

fprintf(['  finished inversion! (' num2str(round(toc(ti),2)) ' s)\n'])

% compare with old implementation with for-loops:
% need to comment-in the function at the end!
% use_F_handleOld = @(KI) use_F_KOld(K, KI, zdisc, allowNan);
% [KIOld OldIterations] = dps.parabolic.tool.fixpoint_iteration(K,use_F_handleOld,1,200,1e-8);
% diff = KI - KIOld;
% diff =  Iterations{3} - OldIterations{3};
% dps.hyperbolic.plot.matrix_4D(diff(:,:,:,:,1),'hide_upper_triangle',0);
% dps.hyperbolic.plot.matrix_4D(KI(:,:,:,:,1),'hide_upper_triangle',0);
% dps.hyperbolic.plot.matrix_4D(KIOld(:,:,:,:,1),'hide_upper_triangle',0);
% 
% dps.hyperbolic.plot.matrix_4D(permute(diff(end,:,:,:,:),[2 5 3 4 1]));
% dps.hyperbolic.plot.matrix_4D(permute(KI(end,:,:,:,:),[2 5 3 4 1]));
% dps.hyperbolic.plot.matrix_4D(permute(KIOld(end,:,:,:,:),[2 5 3 4 1]));
% 
% dps.hyperbolic.plot.matrix_4D(permute(diff(:,1,:,:,:),[1 5 3 4 2]));
% dps.hyperbolic.plot.matrix_4D(permute(KI(:,1,:,:,:),[1 5 3 4 2]));
% dps.hyperbolic.plot.matrix_4D(permute(KIOld(:,1,:,:,:),[1 5 3 4 2]));

end


function FK = use_F_K(K, KI, zdisc, allowNan)
	n = size(KI,3);
	ndisc=length(zdisc);
	numtKernel = size(K,5);
	zDiscVec = zdisc(:);
	zetaDiscVec = zDiscVec.';
	zetaBarVec = reshape(zdisc,1,1,ndisc);
	zMatExt = repmat(reshape(zdisc,1,1,ndisc),ndisc,ndisc,1);
	% the trick is that zeta_bar is only valid in the area, where it is between z and
	% zeta! That means the integration is only perfomed in that area. Therefore, no
	% cumtrapz is needed because the valid area direcly is the range zeta--> z
	zMatExt( (zetaBarVec < zetaDiscVec) | (zetaBarVec > zDiscVec) ) = NaN;
	% account for the time dependency
	zMatExt = repmat(zMatExt,1,1,1,n,n,numtKernel);
	
	KIK = zeros(ndisc,ndisc,ndisc,n,n,numtKernel);
	
	% both reciprocity relations can be used for determining the inverse.
	% this version seems a bit better numerically
	for k=1:n
		%           z zeta zeta_bar i j t
		KkjPerm = shiftdim(permute(K(:,:,k,:,:),[2 1 3 4 5]),-1);
		KIikPerm = reshape(KI(:,:,:,k,:),[ndisc,1,ndisc,n,1,numtKernel]);
		KIK = KIK + KIikPerm.*KkjPerm;
	end		
	% in the second version, the roles of K and KI are interchanged.
% 	for k=1:n
% 		%           z zeta zeta_bar i j t
% 		KkjPerm = shiftdim(permute(KI(:,:,k,:,:),[2 1 3 4 5]),-1);
% 		KIikPerm = reshape(K(:,:,:,k,:),[ndisc,1,ndisc,n,1,numtKernel]);
% 		KIK = KIK + KIikPerm.*KkjPerm;
% 	end		
	% z zeta 1 i j t to z zeta t
	FK = permute(numeric.trapz_fast_nDim(zMatExt,KIK,3,'equal','ignoreNaN'),[1 2 4 5 6 3]);
	% insert NaNs if requested.
	if allowNan
		FK(zetaDiscVec>zDiscVec & ones(1,1,n) & ones(1,1,1,n) & ones(1,1,1,1,numtKernel))=  NaN;
	end
end

% comment in for debug purposes
% function FK = use_F_KOld(Kappa, KI, zdisc, allowNan)
% 	import misc.*
% 	import numeric.*
% 	n = size(KI,3);
% 	FK = zeros(size(KI)); % preallocate
% 	for z_idx = 1:length(zdisc)
% 		for zeta_idx =1:length(zdisc)
% 			if allowNan && (zeta_idx > z_idx)
% 				FK(z_idx,zeta_idx,:,:,:) = NaN;
% 			else
% 				zeta_s = zdisc(zeta_idx:z_idx);
% 				clearvars temp_prod
% 				for zetas_idx = 1:length(zeta_s)
% 					for tIdx=1:size(KI,5)
% 						temp_prod(zetas_idx,:,:,tIdx) = reduce_dimension(Kappa(z_idx,zeta_idx+zetas_idx-1,:,:,tIdx))*reduce_dimension(KI(zeta_idx + zetas_idx - 1,zeta_idx,:,:,tIdx));
% 					end
% 				end
% % 				for i=1:n
% % 					for j=1:n
% 				if numel(zeta_s) > 1
% 						FK(z_idx,zeta_idx,:,:,:) = trapz_fast_nDim(zeta_s,...
% 										  temp_prod);
% 				end
% % 						if isnan(temp_prod)
% % 							test=1;
% % 						end
% % 					end
% % 				end
% 			end
% 		end
% 	end
% end