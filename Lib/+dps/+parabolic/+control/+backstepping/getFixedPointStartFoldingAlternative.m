function GH = getFixedPointStartFoldingAlternative(obj,ppide)
%getFixedPointStartFolding get starting values of fixed point iteration
%
% GH: cell array of kernel_elements

% created on 07.08.2019 by Simon Kerschbaum

% overtake old values
GH = obj.value;

n = ppide.n;
numtKernel = size(GH{1,1}.G,3);
if numtKernel > 1
	tDisc = obj.tDisc;
else
	tDisc = 1;
end

for i=1:n
	for j=1:n
		xiMat = obj.xiMatXi{i,j,i,j};
		GH{i,j}.G = 0*ones(size(xiMat));
		% in order to be able to interpolate, which will be needed for J in order to not implement
		% everything again, the values must be written into the outside-domain!
		GH{i,j}.H = -obj.MuZ{i,j,i,j}(1)/4*ones(size(xiMat));
		GH{i,j}.J = zeros(size(xiMat));
		
		GH{i,j}.G_t = numeric.diff_num(tDisc,GH{i,j}.G,3);

		GH{i,j}.K = GH{i,j}.transform_kernel_element(1);
		GH{i,j}.K_t = numeric.diff_num(tDisc,GH{i,j}.K,3);
		
		% only in the end! Must be used for fixed-point iteration, but not for starting values!
% 		GH{i,j}.J = GH{i,j}.J + GH{i,j}.s/4*numeric.cumtrapz_fast_nDim(xiMat,GH{i,j}.G_t,1,'equal','ignoreNaN');
% 		GH{i,j}.H = GH{i,j}.H + GH{i,j}.s/4*numeric.cumtrapz_fast_nDim(etaMat,GH{i,j}.G_t,2,'equal','ignoreNaN');
		if any(isnan(GH{i,j}.G(:))) || any(isnan(GH{i,j}.G(:))) || any(isnan(GH{i,j}.J(:)))
			here=1;
		end
	end
end

end

