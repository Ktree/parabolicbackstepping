function delta_kernel = use_F_ppides(kernel,ppide)
% USE_F_PPIDES compute the next step of the fixpoint iteration for systems
% of parabolic pides
%   
%   delta_G = USE_F_PPIDES(kernel,ppide)
%	Compute the next step of the fixpoint iteration
%      delta_G{i+1}(xi,eta) = F[G{i}](xi,eta).
%
%      INPUT ARGUMENTS
%    PPIDE_KERNEL  kernel   : each element of kernel.value is an object of the class
%	                         ppide_kernel_element, which represents the
%	                         corresponding element in the respective
%	                         iteration step.
%    PPIDE_SYS     ppide    : the system of parabolic equations for which
%                            the kernel equations are solved.
%
%      OUTPUT ARGUMENTS
%    PPIDE_KERNEL  delta_kernel  : result of the usage of F on G

% history:
% created on 19.04.2017 by Simon Kerschbaum

% userView = memory;
% disp(['Memory used: ' num2str(round(userView.MemUsedMATLAB/1e6)) 'MB'])
import misc.*
import numeric.*
n = ppide.n;
delta_GH = cell(n,n);
GH = kernel.value;
msg1 = sprintf('');
fprintf(msg1);
zdisc = kernel.zdisc;
ndisc = length(zdisc);
Bn = ppide.b_op1.b_n;

%%% Interpolate system parameters to kernel resolution. Ideally, this should all be done in the
%%% initialization. But it doesnt take much time...
tDisc = ppide.ctrl_pars.tDiscKernel;
if nargin(ppide.f) == 2
	% z zeta i j 
	pars.F_disc = ppide.f.on({zdisc,zdisc});
elseif nargin(ppide.f)==3
	% z zeta i j t
	pars.F_disc = permute(ppide.f.on({zdisc,zdisc,tDisc}),[1 2 4 5 3]);
end

pars.a0_disc = kernel.a0Zeta;
varlist = {'a0_disc'};
for i=1:length(varlist)
	if size(pars.(varlist{i}),4)>1
		pars.(varlist{i}) = permute(misc.translate_to_grid(permute(pars.(varlist{i}),[4 1 2 3]),tDisc),[2 3 4 1]);
	end
end

% Only if lambda is time-dependent, the spatial coordinates xi eta become time-dependent. 
%numtL = size(ppide.l,4);
numtL = size(ppide.l,4);
if numtL>1
	error('new implementation has no time dependent lambda implemented!')
end
% if not, the kernel will be time-dependent, but as the coordinates are constant, everything
% can be calculated in one step.
numtKernel = size(kernel.value{1,1}.G,3);

if ppide.ctrl_pars.eliminate_convection
	error(['New implementation only without elimination of convection term!'...
			'Set ppide.ctrl_pars.eliminate_convection to 0!' ])
end

% preallocate
Gzzeta = cell(n,n);
G = cell(n,n,n,n);
Giksum = cell(n,n);
GInterp = cell(n,n);
% store G on z,zeta-grid
for i = 1:n
	for j=1:n
		GInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.G);
		xiMat = kernel.value{i,j}.xiMat;
		etaMat = kernel.value{i,j}.etaMat;
		GijAll = GInterp{i,j}.eval(repmat(xiMat(:),numtKernel,1),repmat(etaMat(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMat),1),[numtKernel*numel(xiMat),1]));
		% fuer F benoetigt.
		Gzzeta{i,j} = reshape(GijAll,size(xiMat,1),size(xiMat,2),numtKernel);
		
		for k=1:n			
			for l=1:n				
				% store interpolations of G
				xiMatXi = kernel.xiMatXi{k,l,i,j}; % xi_ij(xi_kl,eta_kl)
				etaMatXi = kernel.etaMatXi{k,l,i,j}; % eta_ij(xi_kl,eta_kl)
				GAll = GInterp{i,j}.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
				% G_ij(xi_kl,eta_kl)
				G{i,j,k,l} = reshape(GAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
			end
		end
	end
	
end
Azeta = kernel.Azeta;      % Azeta{i,j,k,j} = Aij(zeta(xi_kj,eta_kj))
MuZ = kernel.MuZ;		   % MuZ{i,j,i,k} =  muij(z(xi_ik,eta_ik))
lambdaZeta = kernel.lambdaZeta;  % lambdaZeta{k,i,j} = lambda_k(zeta(xi_ij,eta_ij))

% calculate G on the whole z,zeta-grid.	
zDiscVec = zdisc(:);
zetaDiscVec = zDiscVec.';
zetaBarVec = reshape(zdisc,1,1,ndisc);
zMatExt = repmat(reshape(zdisc,1,1,ndisc),ndisc,ndisc,1);
% the trick is that zeta_bar is only valid in the area, where it is between z and
% zeta! That means the integration is only perfomed in that area. Therefore, no
% cumtrapz is needed because the valid area direcly is the range zeta--> z
zMatExt( (zetaBarVec < zetaDiscVec) | (zetaBarVec > zDiscVec) ) = NaN;
% account for the time dependency
zMatExt = repmat(zMatExt,1,1,1,numtKernel);
	

for lin_idx =1:n^2
% for i=1:n % all rows of G,H
% 	for j=1:n % all columns of G,H
	[i,j] = ind2sub([n n],lin_idx);
	kem = GH{lin_idx}; % abbrv.
	s = kem.s;
	delta_GH{lin_idx} = GH{lin_idx}; % overtake old values. entries G,H are overwritten later
	Gij = GH{lin_idx}.G;
	Hij = GH{lin_idx}.H;
	lambda_j_disc = kem.lambda_j;
	
	a_sigma = -(kem.aiZXieta-kem.ajZetaXieta);
	a_delta = kem.aiZXieta+kem.ajZetaXieta;

	nxi = size(GH{lin_idx}.xi,1);
	xiMat = kernel.xiMatXi{i,j,i,j};
	if size(xiMat,3)==1 && numtKernel > 1
		xiMat = repmat(xiMat,1,1,numtKernel);
	end
	etaMat = kernel.etaMatXi{i,j,i,j};
	if size(etaMat,3)==1 && numtKernel > 1
		etaMat = repmat(etaMat,1,1,numtKernel);
	end
	% resetters to avoid warning of parfor loop.
	etaDisc = GH{lin_idx}.eta;

	% z zeta ozeta t
	GF = zeros(ndisc,ndisc,ndisc,numtKernel);
	GA0 = zeros(ndisc,ndisc,numtKernel);
	for k=1:n
		%lambda_k(ozeta) (3rd dim.)
		lambda_k = reshape(GH{i,k}.lambda_j,1,1,ndisc);
		% z zeta zeta_bar t
		FkjPerm = shiftdim(permute(pars.F_disc(:,:,k,j,:),[2 1 5 3 4]),-1);
		Gikzzeta = reshape(Gzzeta{i,k}(:,:,:),[ndisc,1,ndisc,numtKernel]);
		% lambda_j_disc depends on zeta --> row vector!
		GF = GF + lambda_j_disc.'./lambda_k.*Gikzzeta.*FkjPerm;

		% 2nd dimension is integrated
		lambda_kZeta = reshape(GH{i,k}.lambda_j,1,ndisc);
		GikzzetaA0 = Gzzeta{i,k}(:,:,:);
		GA0 = GA0 + 1./lambda_kZeta .* GikzzetaA0 .* reshape(pars.a0_disc(:,k,j,:),1,ndisc,[]);
	end		
	% z zeta 1 t to z zeta t
	GFInt = permute(numeric.trapz_fast_nDim(zMatExt,GF,3,'equal','ignoreNaN'),[1 2 4 3]);
	GFInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},GFInt);
	GFAll = GFInterp.eval(repmat(kem.zMat(:),numtKernel,1),repmat(kem.zetaMat(:),numtKernel,1),...
		reshape(repmat(1:numtKernel,numel(kem.zMat),1),[numtKernel*numel(kem.zMat),1]));
	GFXiEta = reshape(GFAll,size(kem.zMat,1),size(kem.zMat,2),numtKernel);

	zMatA0 = repmat(zetaDiscVec,ndisc,1,numtKernel);
	zMatA0( zetaDiscVec > zDiscVec & ones(1,1,numtKernel) ) = NaN;
	GA0Int = reshape(numeric.trapz_fast_nDim(zMatA0,GA0,2,'equal','ignoreNaN'),[ndisc,numtKernel]);
	GA0IntInterp = numeric.interpolant({zdisc,1:numtKernel},GA0Int);
	zEtaEta = reshape(kem.zMatInterp{1}.eval(etaDisc(etaDisc>=0),etaDisc(etaDisc>=0)),[],1);
	GA0All = GA0IntInterp.eval(repmat(zEtaEta,numtKernel,1),...
		reshape(repmat(1:numtKernel,numel(zEtaEta),1),[numtKernel*numel(zEtaEta),1]));
	GA0EtaEta = reshape(GA0All,1,numel(zEtaEta),numtKernel);
	
	if any(kem.b_j(:))
		error('Current implementation does not include kem.bj! (needed for convection)')
	end
	Giksum{i,j} = zeros(size(GH{i,j}.G));
	for k=1:n
		% compute sums
		Giksum{i,j} = Giksum{i,j} + lambdaZeta{j,i,j}./lambdaZeta{k,i,j}.*G{i,k,i,j}.*Azeta{k,j,i,j}...
			+ MuZ{i,k,i,j}.*G{k,j,i,j};
	end
	
	% result from last iteration is written into outside area at the end of the method so there
	% are no NaNs creating interpolation problems.
	G_xil_eta = zeros(size(xiMat));
	if s==1 && Bn(j,j) ~= 0
		HInterp = numeric.interpolant({GH{i,j}.xi,GH{i,j}.eta,1:numtKernel},GH{i,j}.H);
		Hetaeta = ...
				reshape(...
					HInterp.eval(...
						repmat(etaDisc(etaDisc>=0),numtKernel,1),...
						repmat(etaDisc(etaDisc>=0),numtKernel,1),...
						reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
					)...
					,1,numel(etaDisc(etaDisc>=0)),numtKernel...
				)...
			;
		Getaeta = ...
			reshape(...
				GInterp{i,j}.eval(...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					repmat(etaDisc(etaDisc>=0),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1]))...
			,1,numel(etaDisc(etaDisc>=0)),numtKernel);

		G_xil_eta(:,etaDisc>=0,:) = numeric.cumtrapz_fast_nDim(etaDisc(etaDisc>=0),...
			zeros(size(xiMat(:,etaDisc>=0,:))) + 2*Hetaeta+reshape(kem.r{3},1,1,[]).*Getaeta + sqrt(lambda_j_disc(1))*GA0EtaEta,...
			2);
	end
	Gi = numeric.cumtrapz_fast_nDim(xiMat,Hij,1,'equal','ignoreNaN');
	delta_GH{i,j}.G = G_xil_eta+Gi;

	% first eta integral in (2.171c, Diss Kopie 26)
	Hi = numeric.cumtrapz_fast_nDim(etaMat,...
		-1/4*a_delta.*Hij + s/4 * kem.a_ij_tilXiEta.*Gij + s/4*Giksum{i,j}...
		+s/4*GFXiEta,2,'equal','ignoreNaN');
	% remaining part is everything calculated for G
	Hii = s/4*a_sigma.*(G_xil_eta+Gi);
	delta_GH{lin_idx}.H = Hi + Hii;
	% remaining parts are time derivatives of kernel
	if numtKernel > 1
		G_t = diff_num(tDisc,GH{i,j}.G,3);
	else
		G_t = zeros(size(GH{i,j}.G));
	end

	if ~ppide.ctrl_pars.quasiStatic && numtKernel > 1
		% dynamic part of kernel
		delta_GH{lin_idx}.H = delta_GH{lin_idx}.H+...
			-1/16/s*numeric.cumtrapz_fast_nDim(etaMat,...
				a_delta...
				.*numeric.cumtrapz_fast_nDim(etaMat,...
					G_t,2,'equal','ignoreNaN'....
				),...
			2,'equal','ignoreNaN')...
			+1/16*a_sigma.*...
			cumtrapz_fast_nDim(...
				xiMat,cumtrapz_fast_nDim(etaMat,G_t,2,'equal','ignoreNaN'),...
				1,'equal','ignoreNaN');
		if s==1		
			% For last double integral in H and G:
			% G_t_int_xi = int_eta_l(xi)^xi G(xi,eta_s)d eta_s
			% will be integrated over xi from 0 to eta in the second step.
			G_t_int_xi = trapz_fast_nDim(etaMat,G_t,2,'equal','ignoreNaN'); % nxi x 1 x nt
			% As it shall be integrated over eta in the next step, it is resampled to fit to eta
			G_t_int_eta = zeros(nxi,size(GH{lin_idx}.eta,1),numtKernel);
			for tIdx=1:numtL
				if numtL == 1 % there may be time-dependency, but not in lambda!
					tIdxAll = 1:numtKernel;
				else
					tIdxAll = tIdx; % there is time-dependency in lambda, everything needs to be done for each time
				end                                   % permute is to get xi-position in eta-position!
				G_t_int_eta(1:nxi,:,tIdxAll) = repmat(permute(translate_to_grid(...
					G_t_int_xi(:,:,tIdxAll),GH{lin_idx}.eta(:,tIdx),...
					'gridOld',GH{lin_idx}.xi(:,tIdx)),[2 1 3]),nxi,1,1);
				% the resultonly depends on eta but is repeated for all xi
			end
			% eta_g_0 is the vector of all eta values >= 0. All smaller values are set to NaN, so
			% that the integration can always be performed over the whole eta-vector
			% would be easier without 'equal','ingnoreNaN'!
			eta_g_0 = GH{lin_idx}.eta;
			eta_g_0(GH{lin_idx}.eta<0) = NaN;
			eta_g_0 = repmat(permute(eta_g_0,[3 1 2]),nxi,1,1);
			if numtL == 1
				eta_g_0 = repmat(eta_g_0,1,1,numtKernel);
			end
			delta_GH{lin_idx}.H = delta_GH{lin_idx}.H...
				+ 1/8*a_sigma...
				.*cumtrapz_fast_nDim(...
					eta_g_0,G_t_int_eta,2,'equal','ignoreNaN'...
				);
			delta_GH{lin_idx}.G = delta_GH{lin_idx}.G...
				+ 1/2/s*...
				cumtrapz_fast_nDim(...
					eta_g_0,G_t_int_eta,2,'equal','ignoreNaN'...
				);
		end
		delta_GH{lin_idx}.G = delta_GH{lin_idx}.G...
			+ 1/4/s *...
			cumtrapz_fast_nDim(xiMat,...
				cumtrapz_fast_nDim(etaMat,G_t,2,'equal','ignoreNaN'),...
			1,'equal','ignoreNaN');
	end % ~quasiStatic && timeDependent
end % parfor lin_idx
msg2 = newline;
reverseStr = repmat(sprintf('\b'), 1, length(msg1)+length(msg2)-1);
fprintf(reverseStr); % alles wieder l�schen
% fprintf('\n')



% continue values of the kernel into the area outside of the spatial
% domain. Is very important to get correct values at the borders!
% TODO: rewrite using writeToOutside!
% Insert NaN into matrices! Important for easy integration.
% logical indexing, a lot faster than loops
varnames = {'G','H'};
for i=1:n
	for j=1:n
		s = GH{i,j}.s;
		a = (s+1)/2 *GH{i,j}.phi_i(end,:) - (s-1)/2*GH{i,j}.phi_j(end,:); % a Ortsbereich
		tVec = ones(1,1,numtKernel);
		for nameIdx = 1:length(varnames)
			delta_GH{i,j}.(varnames{nameIdx})(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi))) & tVec) = NaN;
			delta_GH{i,j}.(varnames{nameIdx}) = misc.writeToOutside(delta_GH{i,j}.(varnames{nameIdx}));
		end
	end
end

	% Hint for implementation of time-varying lambda:
	% It is not possible to calculate the numerical time-derivative of G, because the spatial
	% domain changes in each time step. One method is to use interpolation, to go back to the
	% original grid. Basically, this is nothing else than differentiating K, which is perhaps
	% the best solution.

tVec = linspace(0,1,numtKernel);
delta_kernel = kernel; % Datenuebernahme
delta_kernel.value = delta_GH;
% get original coordinates for G_t
for i=1:n
	for j=1:n
		% transform each element. Needed in the next loop of the fixed-point iteration!
		% Attention: In the result of the fixed-point iteration, which is calculated as the sum
		% of the delta-kernels,, K and K_t are not preserved! They need to be computed again!
		delta_kernel.value{i,j}.K = delta_kernel.value{i,j}.transform_kernel_element(1);
		% Derivative in original coordinates
		delta_kernel.value{i,j}.K_t = diff_num(tVec,delta_kernel.value{i,j}.K,3);
	end
end
	
end % function

