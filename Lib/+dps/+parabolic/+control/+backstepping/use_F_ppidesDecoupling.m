function delta_kernel = use_F_ppidesDecoupling(kernel,ppide,sortVec,sortBack)
% USE_F_PPIDES compute the next step of the fixpoint iteration for systems
% of parabolic pides
%   
%   delta_G = USE_F_PPIDES(kernel,ppide)
%	Compute the next step of the fixpoint iteration
%      delta_G{i+1}(xi,eta) = F[G{i}](xi,eta).
%
%      INPUT ARGUMENTS
%    PPIDE_KERNEL  kernel   : each element of kernel.value is an object of the class
%	                         ppide_kernel_element, which represents the
%	                         corresponding element in the respective
%	                         iteration step.
%    PPIDE_SYS     ppide    : the system of parabolic equations for which
%                            the kernel equations are solved.
%
%      OUTPUT ARGUMENTS
%    PPIDE_KERNEL  delta_kernel  : result of the usage of F on G

% history:
% created on 12.06.2020 by Simon Kerschbaum starting from a copy of use_F_ppidesFolding


import misc.*
import numeric.*

n = ppide.n/2;
delta_GH = cell(n,2*n);
GH = kernel.value;
fast=1; % Set external in get_idx to 1 to be faster!

reverseStr = '';
msg2='';
msg1 = sprintf('');
fprintf(msg1);
run=0;
zdisc = kernel.zdisc;
ndisc = length(zdisc);
numtKernel = size(kernel.value{1,1}.G,3);
tDisc = ppide.ctrl_pars.tDiscKernel;


G = cell(n,2*n,n,2*n);
J = cell(n,2*n,n,2*n);
H = cell(n,2*n,n,2*n);
Gzzeta = cell(n,2*n);
GInterp = cell(n,2*n);
Hzzeta = cell(n,2*n);
HInterp = cell(n,2*n);
Jzzeta = cell(n,2*n);
JInterp = cell(n,2*n);

Bn = ppide.b_op2.b_n;


% evaluate elements on each coordinate grid
for i = 1:n
	for j=1:2*n
		GInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.G);
		JInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.J);
		HInterp{i,j} = numeric.interpolant({kernel.value{i,j}.xi,kernel.value{i,j}.eta,1:numtKernel},kernel.value{i,j}.H);
		
		xiMatZ = kernel.value{i,j}.xiMat; %xi,eta(z,zeta)
		etaMatZ = kernel.value{i,j}.etaMat;
		GijAll = GInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
		HijAll = HInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
		JijAll = JInterp{i,j}.eval(repmat(xiMatZ(:),numtKernel,1),repmat(etaMatZ(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatZ),1),[numtKernel*numel(xiMatZ),1]));
		% used for integrations over z or zeta
		Gzzeta{i,j} = reshape(GijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
		Hzzeta{i,j} = reshape(HijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
		Jzzeta{i,j} = reshape(JijAll,size(xiMatZ,1),size(xiMatZ,2),numtKernel);
		for k=1:n			
			% store interpolations of J
			for l=1:2*n				
				% store interpolations of G
				xiMatXi = kernel.xiMatXi{k,l,i,j}; % xi_ij(xi_kl,eta_kl)
				etaMatXi = kernel.etaMatXi{k,l,i,j}; % eta_ij(xi_kl,eta_kl)
				% Important! Do not evaluate Interpolant with a grid! This creates far too many
				% data points!
				GAll = GInterp{i,j}.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
				% G_ij(xi_kl,eta_kl)
				G{i,j,k,l} = reshape(GAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
				HAll = HInterp{i,j}.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
% 				% G_ij(xi_kl,eta_kl)
				H{i,j,k,l} = reshape(HAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
% 				JAll = JInterp{i,j}.eval(repmat(xiMatXi(:),numtKernel,1),repmat(etaMatXi(:),numtKernel,1),...
% 					reshape(repmat(1:numtKernel,numel(xiMatXi),1),[numtKernel*numel(xiMatXi),1]));
% 				% G_ij(xi_kl,eta_kl)
% 				J{i,j,k,l} = reshape(JAll,size(xiMatXi,1),size(xiMatXi,2),numtKernel);
			end
		end
	end
end
% lambdaZeta = kernel.lambdaZeta;  % lambdaZeta{k,i,j} = lambda_k(zeta(xi_ij,eta_ij))
% lambdaZ = kernel.lambdaZ;  % lambdaZ{k,i,j} = lambda_k(z(xi_ij,eta_ij))
% % AileftInd(z(xi_kj,eta_kj)) = kernel.A0zXiEta{i,k,j}
% A0lrXiEta = kernel.A0lrzXiEta;
% A1lrXiEta = kernel.A1lrzXiEta;
% A1lrDiffXiEta = kernel.A1lrDiffzXiEta;

% dependend on zeta (for integration)
A0lr = kernel.a0lrZeta; %(zeta,i,j,t)
A1lr = kernel.a1lrZeta;
A0l = kernel.a0lZeta; %(zeta,i,j,t)
A1l = kernel.a1lZeta;
% A1lrDiff = kernel.a1DiffZeta;

zetaMatLeft = repmat(zdisc(:).',ndisc,1,numtKernel); % quadratic area
zetaMatRight = zetaMatLeft;
zetaMatRight(zdisc(:)<zdisc(:).' & ones(1,1,numtKernel)) = NaN; % triangular area.


% previously: check if any G or H is identicallz zero
% (as fixpoint iteration is used on basis of the differences, one of the
% two states is zero in each step!)
G_is_nonzero=0;
for i=1:n
	for j=1:2*n
		if any(GH{i,j}.G(:))
			G_is_nonzero=1;
		end
	end
end

z0 = ppide.ctrl_pars.foldingPoint;
% z0Til = z0/(1-z0);
z0TilLeft = z0/(1-z0);
z0TilVec = repmat(z0TilLeft,n,1);
invZ0Til = (1-z0)/z0;
invZ0TilVec = repmat(invZ0Til,n,1);
z0TilGes = [z0TilVec invZ0TilVec];
z0TilSorted = z0TilGes(sortVec);
z0Til = z0TilSorted(1:n);
for lin_idx =n^2+1:2*n^2 % the right eleents do not rely on the solution for the left elements so they are handles first
% for i=1:n % all rows of G,H
% for j=1:2*n % all columns of G,H
	[i,j] = ind2sub([n 2*n],lin_idx);	
	kem = GH{lin_idx}; % abbrv.
	s = kem.s;
	delta_GH{lin_idx} = GH{lin_idx}; % overtake old values. entries G,H are overwritten later
	Hij = GH{lin_idx}.H; % H1
	Jij = GH{lin_idx}.J;
	etaDisc = kem.eta;
	xiDisc = kem.xi;
	
	a_sigma = -(kem.aiZXieta-kem.ajZetaXieta);
	a_delta = kem.aiZXieta+kem.ajZetaXieta;
	
	xiMat = kernel.xiMatXi{i,j,i,j};	
	if size(xiMat,3)==1 && numtKernel > 1
		xiMat = repmat(xiMat,1,1,numtKernel);
	end
	etaMat = kernel.etaMatXi{i,j,i,j};
	if size(etaMat,3)==1 && numtKernel > 1
		etaMat = repmat(etaMat,1,1,numtKernel);
	end
% 	xiMat = repmat(xiDisc,1,numel(etaDisc),numtKernel);
% 	etaMat = repmat(etaDisc.',numel(xiDisc),1,numtKernel);
% 	xiMat  =misc.writeToOutside(xiMat);
% 	xiMat(~(acc(xiDisc) >= acc(etaDisc.') & acc(xiDisc) >= [kem.xi_l_sparse.' zeros(1,numel(etaDisc)-numel(kem.xi_l_sparse))] & ones(1,1,numtKernel))) = NaN;
% 	etaMat  =misc.writeToOutside(etaMat);
% 	etaMat(~(acc(etaDisc.') >= acc(kem.eta_l) & ones(1,1,numtKernel))) = NaN;
	
	% initialize empty new values. Important for block-type solution.
	delta_GH{lin_idx}.G = zeros(size(xiMat));
	delta_GH{lin_idx}.H = zeros(size(xiMat));
	delta_GH{lin_idx}.J = zeros(size(xiMat));
	
	% G
	dGij = numeric.cumtrapz_fast_nDim(etaMat,Jij,2,'equal','ignoreNaN');
		
	% H
	if numtKernel > 1
		G_t = diff_num(tDisc,GH{i,j}.G,3);
	else
		G_t = zeros(size(GH{i,j}.G));
	end
	if ~ppide.ctrl_pars.quasiStatic && numtKernel > 1
		% H1
		dHGt = 1/4*cumtrapz_fast_nDim(...
				etaMat,G_t,2,'equal','ignoreNaN');
	else % ~quasiStatic && timeDependent
		dHGt = zeros(size(xiMat));
	end
	dHij = numeric.cumtrapz_fast_nDim(etaMat,...
		-a_delta/4.*Hij + s*a_sigma/4.*Jij,2,'equal','ignoreNaN');
	delta_GH{lin_idx}.H = dHij + dHGt;
	delta_GH{lin_idx}.G = dGij;
	
	if GH{i,j}.lambda_i(1) >= GH{i,j}.lambda_j(1)
		% needed for J(eta,eta)
		% 1. things that are evaluated at (eta,eta) later on. First calculate for all
		% xi_ij, eta_ij.
		J1 = z0Til(j-n)*H{i,j-n,i,j}; % M(eta,eta)

		HikSumzZeta = zeros(size(zetaMatRight));
		GikSumzZeta = zeros(size(zetaMatRight));
		for k=1:n	

			lambda_kr = reshape(GH{i,k+n}.lambda_j,1,numel(GH{i,k+n}.lambda_j));
			lambda_kl = reshape(GH{i,k}.lambda_j,1,numel(GH{i,k}.lambda_j));

			A1lrkj = reshape(A1lr(:,k,j-n,:),1,size(A1lr,1),numtKernel);
			A0lrkj = reshape(A0lr(:,k,j-n,:),1,size(A0lr,1),numtKernel);
			A1lkj = reshape(A1l(:,k,j-n,:),1,size(A1l,1),numtKernel);
			A0lkj = reshape(A0l(:,k,j-n,:),1,size(A0l,1),numtKernel);

			% lambda_i is always stored from right!
			HikSumzZeta = HikSumzZeta + ...
				1/2*z0Til(j-n) ... * sqrt(GH{i,j}.lambda_i(:)) ...
				.* (numeric.trapz_fast_nDim(zetaMatRight,...
					1./lambda_kr...
					.* (GH{i,k+n}.s*Hzzeta{i,k+n}+Jzzeta{i,k+n}) .* A1lrkj,... % Hik(z,zeta)
				2,'equal','ignoreNaN') + ...
				numeric.trapz_fast_nDim(zetaMatLeft,...
					1./lambda_kl...
					.* (Hzzeta{i,k}+Jzzeta{i,k}) .* A1lkj,... % Mik(z,zeta)
				2,'equal','ignoreNaN')); 

			GikSumzZeta = GikSumzZeta - ... % j>n so this is always lambda_j^r
				1/2*sqrt(GH{i,j}.lambda_j(1)) * (...
				 numeric.trapz_fast_nDim(zetaMatRight,...
					1./ lambda_kr .* Gzzeta{i,k+n} .* A0lrkj, 2, 'equal','ignoreNaN')... % Gik
				+numeric.trapz_fast_nDim(zetaMatLeft,...
					1./ lambda_kl .* Gzzeta{i,k} .* A0lkj, 2, 'equal','ignoreNaN')); % Dik
		end
		HikSumInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},HikSumzZeta);
		HikSumAll = HikSumInterp.eval(repmat(kem.zMat(:),numtKernel,1),repmat(kem.zetaMat(:),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(kem.zMat),1),[numtKernel*numel(kem.zMat),1]));
		HikSum = reshape(HikSumAll,size(kem.zMat,1),size(kem.zMat,2),numtKernel);
		GikSumInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},GikSumzZeta);
		GikSumAll = GikSumInterp.eval(repmat(kem.zMat(:),numtKernel,1),repmat(kem.zetaMat(:),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(kem.zMat),1),[numtKernel*numel(kem.zMat),1]));
		GikSum = reshape(GikSumAll,size(kem.zMat,1),size(kem.zMat,2),numtKernel);

		JForEtaEta = J1 + HikSum + GikSum;	
	else  % lambda_i^r >= lambda_j^r. In the other case, there is nothing to add for eta,eta
		JForEtaEta = zeros(size(xiMat));
	end
	JForEtaEtaOut = misc.writeToOutside(JForEtaEta);
	JForEtaEtaInterp = numeric.interpolant({delta_GH{i,j}.xi,delta_GH{i,j}.eta,1:numtKernel},JForEtaEtaOut);
	JAtEtaEta = zeros(size(xiMat));
	JAtEtaEta(:,etaDisc>=0,:) = zeros(size(xiMat(:,etaDisc>=0,:))) + ...
		reshape(...
			JForEtaEtaInterp.eval(...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
			)...
		,1,numel(etaDisc(etaDisc>=0)),numtKernel);
	
	J2 = numeric.cumtrapz_fast_nDim(xiMat,...
		(-a_delta/4.*Hij + a_sigma/4/s .* Jij),1,'equal','ignoreNaN');
	
	if ~ppide.ctrl_pars.quasiStatic && numtKernel > 1
		% H1
		dJGt = 1/4*cumtrapz_fast_nDim(...
				xiMat,G_t,1,'equal','ignoreNaN');
	else % ~quasiStatic && timeDependent
		dJGt = zeros(size(xiMat));
	end
	
	delta_GH{i,j}.J = JAtEtaEta + J2 + dJGt;
	if any(isnan(delta_GH{i,j}.J(:)))
		here=1;
	end
end % parfor linIdx % G and H first need to be calculated for all i,j so that their result can be inserted for J!


for lin_idx = 1:n^2 % now the left indices. That means
	% Gij = Dij!
	% Gij+n = Gij
	% Hij = Mij
	% Hij+n = Hij
	% Jij = Nij
	% Jij+n = Jij could now be done in one loop!
	
	[i,j] = ind2sub([n 2*n],lin_idx);	
	kem = GH{lin_idx}; % abbrv.
	s = kem.s;
	delta_GH{lin_idx} = GH{lin_idx}; % overtake old values. entries G,H are overwritten later
	Hij = GH{lin_idx}.H; % H1
	Jij = GH{lin_idx}.J;
	a_sigma = -(kem.aiZXieta-kem.ajZetaXieta);
	a_delta = kem.aiZXieta+kem.ajZetaXieta;
	
	xiDisc = GH{lin_idx}.xi;
	etaDisc = GH{lin_idx}.eta;

	xiMat = repmat(xiDisc,1,numel(etaDisc),numtKernel);
	etaMat = repmat(etaDisc.',numel(xiDisc),1,numtKernel);
	xiMat = kernel.xiMatXi{i,j,i,j};
	if size(xiMat,3)==1 && numtKernel > 1
		xiMat = repmat(xiMat,1,1,numtKernel);
	end
	etaMat = kernel.etaMatXi{i,j,i,j};
	if size(etaMat,3)==1 && numtKernel > 1
		etaMat = repmat(etaMat,1,1,numtKernel);
	end
% 	xiMat  =misc.writeToOutside(xiMat);
% 	xiMat(~(acc(xiDisc) >= acc(etaDisc.') & acc(xiDisc) >= [kem.xi_l_sparse.' zeros(1,numel(etaDisc)-numel(kem.xi_l_sparse))] & ones(1,1,numtKernel))) = NaN;
% 	etaMat  =misc.writeToOutside(etaMat);
% 	etaMat(~(acc(etaDisc.') >= acc(kem.eta_l) & ones(1,1,numtKernel))) = NaN;
	
	% initialize empty new values. Important for block-type solution.
	delta_GH{lin_idx}.G = zeros(size(xiMat));
	delta_GH{lin_idx}.H = zeros(size(xiMat));
	delta_GH{lin_idx}.J = zeros(size(xiMat));

	% now j<=n. 
	dHij = numeric.cumtrapz_fast_nDim(etaMat,...
		-a_delta/4 .* Hij + s*a_sigma/4 .* Jij,2,'equal','ignoreNaN');
	dGij = numeric.cumtrapz_fast_nDim(etaMat,Jij,2,'equal','ignoreNaN');
	
	if numtKernel > 1
		G_t = diff_num(tDisc,GH{i,j}.G,3);
	else
		G_t = zeros(size(GH{i,j}.G));
	end
	if ~ppide.ctrl_pars.quasiStatic && numtKernel > 1
		% H1
		dHGt = 1/4*cumtrapz_fast_nDim(...
				etaMat,G_t,2,'equal','ignoreNaN');
	else % ~quasiStatic && timeDependent
		dHGt = zeros(size(xiMat));
	end
	J2 = numeric.cumtrapz_fast_nDim(xiMat,...
		(-a_delta/4.*Hij + a_sigma/4/s .* Jij),1,'equal','ignoreNaN');
	
	if ~ppide.ctrl_pars.quasiStatic && numtKernel > 1
		% H1
		dJGt = 1/4*cumtrapz_fast_nDim(...
				xiMat,G_t,1,'equal','ignoreNaN');
	else % ~quasiStatic && timeDependent
		dJGt = zeros(size(xiMat));
	end
	% j <= n --> J = N!
	HikSumzZeta = zeros(size(zetaMatRight));
	GikSumzZeta = zeros(size(zetaMatRight));
	if GH{i,j+n}.lambda_i(1) >= GH{i,j+n}.lambda_j(1)
% 		J1 = dH{i,j+n,i,j}; % will be H(eta,eta) 
		J1 = 1/z0Til(j) * H{i,j+n,i,j}; % will be H(eta,eta) 
		for k=1:n
			lambda_kr = reshape(GH{i,k+n}.lambda_j,1,numel(GH{i,k+n}.lambda_j));
			lambda_kl = reshape(GH{i,k}.lambda_j,1,numel(GH{i,k}.lambda_j));

			A1lrkj = reshape(A1lr(:,k,j,:),1,size(A1lr,1),numtKernel);
			A0lrkj = reshape(A0lr(:,k,j,:),1,size(A0lr,1),numtKernel);
			A1lkj = reshape(A1l(:,k,j,:),1,size(A1l,1),numtKernel);
			A0lkj = reshape(A0l(:,k,j,:),1,size(A0l,1),numtKernel);

% 			error('Hier weiter, irgendwo ist Problem, dass Jzzeta NaNs drin hat. Obwohl Inkrement am Ende eigtl immer nach aussen geschrieben werden muesste!')
			HikSumzZeta = HikSumzZeta - ...
				1/2 ...
				.* (numeric.trapz_fast_nDim(zetaMatRight,...
					1./lambda_kr...
					.* (GH{i,k+n}.s*Hzzeta{i,k+n}+Jzzeta{i,k+n}) .* A1lrkj,... % Hik(z,zeta)
				2,'equal','ignoreNaN') + ...
				numeric.trapz_fast_nDim(zetaMatLeft,...
					1./lambda_kl...
					.* (Hzzeta{i,k}+Jzzeta{i,k}) .* A1lkj,... % Mik(z,zeta)
				2,'equal','ignoreNaN')); 

			GikSumzZeta = GikSumzZeta - ...
				sqrt(GH{i,j+n}.lambda_j(1))/2/z0Til(j) * (...
				 numeric.trapz_fast_nDim(zetaMatRight,...
					1./ lambda_kr .* Gzzeta{i,k+n} .* A0lrkj, 2, 'equal','ignoreNaN')... % Gik
				+numeric.trapz_fast_nDim(zetaMatLeft,...
					1./ lambda_kl .* Gzzeta{i,k} .* A0lkj, 2, 'equal','ignoreNaN')); % Dik
		end
		HikSumInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},HikSumzZeta);
		HikSumAll = HikSumInterp.eval(repmat(kem.zMat(:),numtKernel,1),repmat(kem.zetaMat(:),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(kem.zMat),1),[numtKernel*numel(kem.zMat),1]));
		HikSum = reshape(HikSumAll,size(kem.zMat,1),size(kem.zMat,2),numtKernel);
		GikSumInterp = numeric.interpolant({zdisc,zdisc,1:numtKernel},GikSumzZeta);
		GikSumAll = GikSumInterp.eval(repmat(kem.zMat(:),numtKernel,1),repmat(kem.zetaMat(:),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(kem.zMat),1),[numtKernel*numel(kem.zMat),1]));
		GikSum = reshape(GikSumAll,size(kem.zMat,1),size(kem.zMat,2),numtKernel);
		JForEtaEta = J1 + HikSum + GikSum;
	else
		JForEtaEta = zeros(size(xiMat)); % lambda_i^r >= lambda_j^r. In the other case, there is nothing to add for eta,
	end
	% write to outside for interpolation is required, since original parameters are
	% evaluated for JforEtaEta, which yields NaN outsite valid area.
	JForEtaEtaOut = misc.writeToOutside(JForEtaEta);
	JForEtaEtaInterp = numeric.interpolant({delta_GH{i,j}.xi,delta_GH{i,j}.eta,1:numtKernel},JForEtaEtaOut);
	JAtEtaEta = zeros(size(xiMat));
	JAtEtaEta(:,etaDisc>=0,:) = zeros(size(xiMat(:,etaDisc>=0,:))) + ...
		reshape(...
			JForEtaEtaInterp.eval(...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				repmat(etaDisc(etaDisc>=0),numtKernel,1),...
				reshape(repmat(1:numtKernel,numel(etaDisc(etaDisc>=0)),1),[numtKernel*numel(etaDisc(etaDisc>=0)),1])...
			)...
		,1,numel(etaDisc(etaDisc>=0)),numtKernel);
	delta_GH{lin_idx}.J = JAtEtaEta + J2 + dJGt;
	
% 	for blockIdx = 1:ceil(max(xiDisc)/(2*GH{i,j}.phi_j(end)))
% 		xiMax = acc(blockIdx * 2*GH{i,j}.phi_j(end));
% 		xiMin = acc((blockIdx - 1)* 2*GH{i,j}.phi_j(end));
% 		blockXiIdc = acc(xiDisc) > xiMin & acc(xiDisc) <= xiMax;
% 		blockEtaIdc = acc(etaDisc) > xiMin & acc(etaDisc) <= xiMax; % corresponding area for eta
% 		% integration needs to be started exactly on the LOS!
% 		% However, not every k*2*phi_jl(1) is on the grid!
% 		% Therefore, it may happen that blockXiExt == blockXi. No matter
% 		blockXiIdcExt = acc(xiDisc) >= xiMin & acc(xiDisc) <= xiMax;
% 		if blockIdx > 1
% 			lastIndBefore  = find(acc(xiDisc)< xiMin,1,'last');
% 			blockXiIdcExt(lastIndBefore) = 1; % take last index into integal to get continuous boundary!
% 		end
% 		blockXiIdxVecExt = find(blockXiIdcExt);
% 		
% 		blockXi = xiDisc(blockXiIdc);
% 		blockXiExt = xiDisc(blockXiIdcExt);
% 		blockEta = etaDisc(blockEtaIdc);
		
		% needs to be repeated eacht time inside the loop, since result of last loop run is
		% needed!
% 		warninig('Sollte eigtl oberes sein!')
% 		NXiEtaNew = misc.writeToOutside(delta_GH{i,j}.J);
	NXiEtaNew = misc.writeToOutside(Jij);
	NXiEta = misc.writeToOutside(Jij);
	NXiEtaNewInterp = numeric.interpolant({xiDisc,etaDisc,1:numtKernel},NXiEtaNew);
	NXiEtaInterp = numeric.interpolant({xiDisc,etaDisc,1:numtKernel},NXiEta);

% 	NXiEtalExt = ...
% 	reshape(...
% 		NXiEtaInterp.eval(...
% 			repmat(blockXiExt,numtKernel,1),...
% 			repmat(kem.eta_l(blockXiIdcExt),numtKernel,1),...
% 			reshape(repmat(1:numtKernel,numel(blockXiExt),1),...
% 				[numtKernel*numel(blockXiExt),1])...
% 		)...
% 	,numel(blockXiExt),1,numtKernel);
	validXiIdc = xiDisc > 2*GH{i,j}.phi_j(end);
	validXiIdcExt = xiDisc >= 2*GH{i,j}.phi_j(end);% for integration. Start at boundary
	NXiEtal = ...
	reshape(...
		NXiEtaNewInterp.eval(...
			repmat(xiDisc(validXiIdc),numtKernel,1),...
			repmat(kem.eta_l(validXiIdc),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(xiDisc(validXiIdc)),1),...
				[numtKernel*numel(xiDisc(validXiIdc)),1])...
		)...
	,numel(xiDisc(validXiIdc)),1,numtKernel);
	NXiEtalExt = ...
	reshape(...
		NXiEtaNewInterp.eval(...
			repmat(xiDisc(validXiIdcExt),numtKernel,1),...
			repmat(kem.eta_l(validXiIdcExt),numtKernel,1),...
			reshape(repmat(1:numtKernel,numel(xiDisc(validXiIdcExt)),1),...
				[numtKernel*numel(xiDisc(validXiIdcExt)),1])...
		)...
	,numel(xiDisc(validXiIdcExt)),1,numtKernel);
	% H:
	
	MXiEtaL = zeros(size(xiMat));
	if Bn(j,j) == 0 % dir
		MXiEtaL(validXiIdc,:,:) = -NXiEtal + zeros(size(xiMat(validXiIdc,:,:)));
	else % rob
		MXiEtaL(validXiIdc,:,:) = NXiEtal + zeros(size(xiMat(validXiIdc,:,:)));
	end
	
	DXiEtaL = zeros(size(xiMat));
	if Bn(j,j) ~= 0	% rob
		DXiEtaL(validXiIdcExt,:,:) = ...
			numeric.cumtrapz_fast_nDim(xiDisc(validXiIdcExt),2*NXiEtalExt,1) + zeros(size(xiMat(validXiIdcExt,:,:)));
	end
	% save last point for next block: Important because even the xiBlockExt may start after
	% the last value! This would lead to a jump in the kernel which acutally does not
	% happen, at least for Robin BC.
	% storage needed at this point because needed for J later.
	delta_GH{lin_idx}.G = dGij + DXiEtaL;
	delta_GH{lin_idx}.H = dHij + dHGt + MXiEtaL;

	if any(isnan(delta_GH{i,j}.J(:)))
		here=1;
	end
% 	end % for blockIdx
end % for lin_idx (j<=n)

% end

% continue values of the kernel into the area outside of the spatial
% domain. Is very important to get correct values at the borders!
% must be done before an interpolant of delta_GH is created.
% TODO: rewrite using writeToOutside is only possible if ensured that there are NaNs outside
% the domain

% to write the values into outside the spatial domain, NaNs are inserted first, so
% writeToOutside can be called. This is very efficient as it needs as little loops as possible
% Insert NaN into matrices! Important for easy integration.
% logical indexing, a lot faster than loops. For min_xi_diff = 0.001, it takes 2seconds
% compared to 110 seconds with the older method
varnames = {'G','H','J'};
for i=1:n
	for j=1:2*n
		s = GH{i,j}.s;
		a = (s+1)/2 *GH{i,j}.phi_i(end,:) - (s-1)/2*GH{i,j}.phi_j(end,:); % a Ortsbereich
		for nameIdx = 1:length(varnames)
			delta_GH{i,j}.(varnames{nameIdx})(~(acc(GH{i,j}.eta.')>=acc(GH{i,j}.eta_l) & acc(GH{i,j}.eta.') <= min(acc(GH{i,j}.xi),acc(2*a-GH{i,j}.xi))) & ones(1,1,numtKernel)) = NaN;
			delta_GH{i,j}.(varnames{nameIdx}) = misc.writeToOutside(delta_GH{i,j}.(varnames{nameIdx}));
		end
	end
end


msg2 = newline;
reverseStr = repmat(sprintf('\b'), 1, length(msg1)+length(msg2)-1);
fprintf(reverseStr); % alles wieder l�schen
% fprintf('\n')

tVec = linspace(0,1,numtKernel);
delta_kernel = kernel; % Datenuebernahme
delta_kernel.value = delta_GH;
% get original coordinates for G_t

for i=1:n
	for j=1:2*n
		if any(isnan(delta_kernel.value{i,j}.G(:)))
			here=1;
		end
		if any(isnan(delta_kernel.value{i,j}.H(:)))
			here=1;
		end
		if any(isnan(delta_kernel.value{i,j}.J(:)))
			here=1;
		end
		% transform each element. Needed in the next loop of the fixed-point iteration!
		% Attention: In the result of the fixed-point iteration, which is calculated as the sum
		% of the delta-kernels,, K and K_t are not preserved! They need to be computed again!
		delta_kernel.value{i,j}.K = delta_kernel.value{i,j}.transform_kernel_element(1);
		% Derivative in original coordinates
		delta_kernel.value{i,j}.K_t = diff_num(tVec,delta_kernel.value{i,j}.K,3);
	end
end
% delta_kernel.plot('G')
% delta_kernel.plot('H')
end % function



