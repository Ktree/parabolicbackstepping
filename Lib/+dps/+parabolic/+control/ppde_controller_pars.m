classdef ppde_controller_pars
% PPDE_CONTROLLER_PARS contains all parameters that have to be set as
% controller configuration for control of systems of parabolic pdes
	
% created by Simon Kerschbaum on 12.09.2016
% modified on 20.04.2017 by SK:
%  - added min_xi_diff and min_eta_diff

	properties
		% backstepping parameters:
		mu = 1;
		mu_o = 1;
		zdiscCtrl = linspace(0,1,11);            % Aufl�sung Reglerkern
		zdiscRegEq = linspace(0,1,11);           % Aufl�sung Regulator equations
		zdiscObsKernel = linspace(0,1,11);       % Aufl�sung Beobachterkern
		zdiscObs = linspace(0,1,11);             % Aufl�sung Beobachterrealisierung
		zdiscTarget = linspace(0,1,11);          % Aufl�sung Simulation Zielsystem
		tDiscKernel = linspace(0,1,11);          % Aufl�sung Zeitachse
		g_strich = {@(z) 0*z};
		g_strich_disc = [];                      % Discretized version of g_strich, to check if g_strich was modified!
		eliminate_convection (1,1) logical = 0;  % parameter to set way of determining the kernel (backstepping control)
		
		foldingPoint = 0.5;                      % foldingPoint for bilateral Backstepping
		
        % paramers of observers:
		eigDist                                  % Eigenwerte St�rbeobachter
		eigRef                                   % Eigenwerte F�hrungsbeobachter
		
        % parameters for robust output regulation
		By                                       % Eingangsmatrix internes Modell
		eigInt                                   % Eigenwerte internes Modell   
		makeTargetNeumann (1,1) logical = 0      % make traget system BCs to Neumann (if robin)
		quasiStatic (1,1) logical = 0            % if this flag is set, the ppide kernel for time-dependent
		                                         % coefficients is calculated as if it were constant at
		                                         % the different timestamps.
		                                         % parameters for delay compensation
		considerInputDelay (1,1) logical = 0;    % if true, the controller will compensate input delays.

		% numerical parameters:
		it_min = 1;
		it_max = 25;
		tol = 1e-3;	
		min_z_res = 1e-12;                  % auf diese Nachkommastelle werden alle Zahlen gerundet
		min_xi_diff = 0.05;                 % minimaler Abstand zweier Punkte im groben xi-eta-Ortsbereich
		maxNumXi = [];						% alternativ: maximale Anzahl Punkte in xi-Richtung
		min_eta_diff = 0.05;                % wird erst bei PIDEs verwendet
		min_xi_diff_border = 0;             % minimaler Abstand zweier Punkte im xi-eta-Ortsbereich, die durch einen Randpunkt in z/zeta Koordinaten erzeugt werden
		min_eta_diff_border  = 0;           % wird erst bei PIDEs verwendet
		min_dist = 1e-6;                    % minimaler Abstand in allen xi-eta Ortsbereichen,
                                            % kleinerer Abstand wird als gleich angesehen
                                            % wird erst bei PIDEs verwendet.
	end
	
	methods
		function obj = ppde_controller_pars(varargin)
			% obj = ppde_controller_pars(varargin)
			%   create new ppde_controller_pars object.
			%   You can pass all class properties by 'name', value syntax
			import misc.*
			arglist = properties(obj);
			found=0;
            written={};
            storage=struct;
			if ~isempty(varargin)
				if mod(length(varargin),2) % uneven number
					error('When passing additional arguments, you must pass them pairwise!')
                end
				for index = 1:2:length(varargin) % loop through all passed arguments:
					for arg = 1:length(arglist)
						if strcmp(varargin{index},arglist{arg})
							storage.(arglist{arg}) = varargin{index+1};
                            written=[written arglist{arg}];
							found=1;
						break
						end 
					end % for arg
					% argument wasnt found in for-loop
					if ~found
						error([varargin{index} ' is not a valid property of class ' class(obj) '!']);
					end
					found=0; % reset found
				end % for index
            end 
            
            % Check whether zdiscCtrl was passed:
%             if ~isfield(storage,'zdiscCtrl')
%                 % set default value in case of no zdiscCtrl passed
%                 storage.zdiscCtrl = linspace(0,1,11);
% 			end
			
			if ~isfield(storage,'zdiscCtrl')
				storage.zdiscCtrl = linspace(0,1,11);
			end
			if ~isfield(storage,'tDiscKernel')
				storage.tDiscKernel = linspace(0,1,11);
			end

			% TODO: provide default, zero parameter! Correct implemenation!
            % discretize g_strich
			if ~isfield(storage,'g_strich') % no a was passed at all
				for i=1:size(obj.g_strich,1)
					for j=1:size(obj.g_strich,2)
						obj.g_strich_disc{i,j} = eval_pointwise(obj.g_strich{i,j},storage.zdiscCtrl);
					end
                end
                n_g = 1;
            else
                if ~iscell(storage.g_strich)
                    error('"g_strich" must be provided as cell array!');
                end
                if(size(storage.g_strich,1) ~= size(storage.g_strich,2))
                    error('"g_strich" must be provided in square form!');
                end
                n_g = size(storage.g_strich,1);
			end
            
            %% Check dimensions of internal model
            
            % Check internal model
            if (isfield(storage,'By'))
                if(isfield(storage,'eigInt'))
                    % Both By and eigInt passed -> now check dimensions
                    nInt = size(storage.By,1);
                    if(~isvector(storage.eigInt))
                        error('Eigenvalues of internal model must be passed as vector!');
                    end
                    if(length(storage.eigInt) ~= nInt)
                        error('System order of internal model is n = %d but there are %d eigenvalues passed. Check dimensions!',nInt,length(storage.eigInt));
                    end
                else
                    error('When passing "By" to controller parameters, the eigenvlues "eigInt" must be passed too!');
                end
            else
                if(isfield(storage,'eigInt'))
                    error('When passing "eigInt" to controller parameters, the input matrix "By" must be passed too!');
                end
            end
            
            %% Check mu, mu_o and g_strich
            
            % discretize mu, mu_o an check if they have proper form
            % (square, correct discretization etc.) and save n_mu and n_mu_o
            discList = {'mu','mu_o'};
			% Make mu will eigther be a function handle or a constant matrix
			for parname = discList
				if isfield(storage,parname{1})
					if isa(storage.(parname{1}),'function_handle') % mu ist function handle
						if nargin(storage.(parname{1}))==1
							if ~isequal(size(storage.(parname{1})(1),1),size(storage.(parname{1})(1),2))
								error('When passing "%s" as a function handle, it must be of square form!',parname{1});
							end
							eval([parname{1} ' = storage.' parname{1} ';']);
							eval(['n_' parname{1} ' = size(storage.(parname{1})(1),1);']);
						elseif nargin(storage.(parname{1}))==2
							if ~isequal(size(storage.(parname{1})(1,1),1),size(storage.(parname{1})(1,1),2))
								error('When passing "%s" as a function handle, it must be of square form!',parname{1});
							end
							for z_idx=1:length(storage.zdiscCtrl)
								% evaluate time dimension
								eval([parname{1} '(z_idx,:,:,:) = permute((misc.eval_pointwise(@(z)storage.' parname{1} '(storage.zdiscCtrl(z_idx),z),storage.tDiscKernel)),[2 3 1]);']);
							end
							eval(['n_' parname{1} ' = size(storage.(parname{1})(1,1),1);']);
						else
							error('When passing "%s" as a function handle, it must have 1 (space) or 2 (space and time) arguments.',parname{1});
						end
                    elseif ismatrix(storage.(parname{1})) % check if it is constant
                        if(size(storage.(parname{1}),1) ~= size(storage.(parname{1}),2))
                            % check if fist dimension is equal to ndiscCtrl
                            % which probably means that n = 1 and therfore
                            % mu is a scalar discretized function
                            if(size(storage.(parname{1}),1) == length(storage.zdiscCtrl))
                                if(size(storage.(parname{1}),2) == 1)
                                    eval([parname{1} ' = storage.' parname{1} ';']);
                                    eval(['n_' parname{1} ' = 1;']);
                                else
                                    error('It seems, that "%s" is passed in discretized form for n = 1, but it is not of square form!',parname{1});
                                end
                            else
                                error('Wrong representation of "%s". It seems that n = 1 and %s is given in discretized form, but the first dimension doesn�t fit zdiscCtrl.',parname{1},parname{1});
                            end
                        else
                            eval([parname{1} ' = storage.' parname{1} ';']);
                            eval(['n_' parname{1} ' = size(storage.(parname{1}),1);']);
                        end
                    else % Seems to be discretized already
                        if(size(storage.(parname{1}),1) ~= length(storage.zdiscCtrl))
                            error('It seems you tried to provide "%s" in discretized form, but its first (space) dimension doesn�t fit zdiscCtrl!',parname{1});
                        end
                        if ~isequal(size(storage.(parname{1}),2),size(storage.(parname{1}),3))
                            error('"%s" must be passed in square form!',parname{1});
						end
						if length(size(storage.(parname{1})))>3
							if(size(storage.(parname{1}),4) ~= length(storage.tDiscKernel))
								error('It seems you tried to provide "%s" in discretized form, but its forth (time) dimension doesn�t fit tDiscKernel!',parname{1});
							end
							if length(size(storage.(parname{1})))>4
								error('It seems you tried to provide "%s" in discretized form, but has five dimensions!',parname{1});
							end
						end
						if (size(storage.(parname{1}),2) == length(storage.zdiscCtrl)) | (size(storage.(parname{1}),2) == length(storage.tDiscKernel))
							error('n = ndiscCtrl or n = tDiscKernel seems to be a very bad choice!');
						end
						eval(['n_' parname{1} ' = size(storage.(parname{1}),2);']);
						eval([parname{1} ' = storage.' parname{1} ';']);
% 						eval(['Interp' parname{1} ' = numeric.interpolant({storage.zdiscCtrl,1:n_' parname{1} ',1:n_' parname{1} '},storage.' parname{1} ');']);
% 						eval([parname{1} ' = @(z) squeeze(Interp' parname{1} '.evaluate(z(:),1:n_' parname{1} ',1:n_' parname{1} '));']);
					end % isa(storage.(parname{1}),'function_handle')
					eval(['storage.' parname{1} ' = ' parname{1} ';']); % pass to storage so it will be written to object
				end % if isfield(storage,parname{1})
			end % for parname = discList
            
            % check whether mu, mu_o and g_strich dimensions fit to each other
            if ~isempty(written)
                written_names = fieldnames(storage);
                for i=1:length(written_names)
                    switch written_names{i}
                        case 'mu'
                            if(n_g ~= n_mu)
                                error('Dimension missmatch: "g_strich" implys n = %d while "mu" implys n = %d',n_g,n_mu);
                            end
                            if exist('n_mu_o','var')
                                if(n_mu_o ~= n_mu)
                                    error('Dimension missmatch: "mu_o" implys n = %d while "mu" implys n = %d',n_mu_o,n_mu);
                                end
                            end
                        case 'mu_o'
                            if(n_g ~= n_mu_o)
                                error('Dimension missmatch: "g_strich" implys n = %d while "mu_o" implys n = %d',n_g,n_mu_o);
                            end
                            if exist('n_mu','var')
                                if(n_mu_o ~= n_mu)
                                    error('Dimension missmatch: "mu_o" implys n = %d while "mu" implys n = %d',n_mu_o,n_mu);
                                end
                            end
                    end
                end
            end
            
            % Write inputs to object
            fn = fieldnames(storage);
			for fn_idx = 1:length(fn)
				obj.(fn{fn_idx})=storage.(fn{fn_idx});
			end
            
        end % function obj = ppde_controller_pars(varargin)
        
    end % methods
	
end

