% ppideTest
% Test script to check if output regulation for ppides is working.

addpath(genpath([pwd '\' 'NewLib']));
addpath(genpath([pwd '\' 'external']));

load +dps/+parabolic/+mat/+test/disturbance.mat
load +dps/+parabolic/+mat/+test/reference.mat
simRefComp = simRef;
simDistComp = simDist;


% shared variables:

names{1} = 'simRef';
names{2} = 'simDist';

import misc.*
import numeric.*

ndiscKernel = 8;% Aufloesung Kern
ndiscObsKernel = 8; % Aufloesung Beobachterkern und Entkopplungsgleichungen

sim_pars = misc.simulation_parameters('frel',1e-6,...
								 'fabs',1e-6,... 
								 'tsmax',2e-2); % 8
% Ploteinstellungen
plot_pars.plot_res_1D = 50;
plot_pars.plot_res_2D_lines = 150;
plot_pars.plot_res_2D_num_lines = 20;
							
% Sukzessive Approximation:
itmax = 10; 
itmin = 1;
tol = 1e-3; % Abbruchkriterium sukzessive Approximation
min_xi_diff = 0.05; % numerischer Parameter, f�r numerische Effizienz
min_eta_diff = 0.05;

ndiscPars = 51; % Aufloesung gespeicherte Parameter
ndiscRegEq = 51; % Aufloesung Regulator equations
ndiscPlant = 51;% Aufl�sung Streckenapproximation
ndiscObs = 13; % Aufloesung Beobachterrealisierung
ndiscTarget = ndiscPlant; % Aufloesung Zielsystem (f�r Analysezwecke)
 
% Strecke festlegen:
n=2; % Anzahl Zust�nde

% Streckenparameter
lambda = @(z) [3/2+z.^2.*cos(2*pi*z), 0;0, 1/2-1/4*sin(2*pi*z)];
lambda_diff = @(z) [2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi,0;0, -1/4*cos(2*pi*z)*2*pi];
C = @(z) [0 0; 0 0]+0*z; % Konvektionsmatrix
a = @(z) 1*[1 1+z; 1/2+z 1]; 
a0 = @(z) 1*[1*z 1-z; z 1-z]; 
F = @(z,zeta) 1*[exp(z+zeta) exp(z-zeta);1-exp(-(z-zeta)) exp(-(z+zeta))];
% 	x0 = @(z) [get_x0_ppides(z,1);get_x0_ppides(z,1);];
% Ausgangsoperator definieren:
c_op = dps.output_operator('c_m',[1 0;0 0],'z_m',0.25,'c_c',@(z) [0 0;0 1]);

% Messung (Nur f�r Beobachter, nicht f�r internes Modell!)
Cm_op = dps.output_operator('c_0',eye(n));

% Rechte Randbedingung festlegen:
Bdr = 0*eye(n,n);
Bnr = eye(n,n);
b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
						  'b_d',-Bdr,...
						  'b_n',Bnr);

% linken Rand-Operator definieren:
numDir = 0; % number of Dirichlet BCs
Bd = -1*diag(n:-1:1);
% Bd = eye(n);
% Bd = eye(n);
b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
						  'b_d',blkdiag(eye(numDir),Bd),... %Dirichlet
						  'b_n',blkdiag(zeros(numDir,numDir),eye(n-numDir))); %Neumann
					  
% Regelung konfigurieren:
mu_mat = 5*eye(n);
mu_o_mat = 2*mu_mat;

% Freiheitsgrad zu 0 festlegen.
for i=1:n
	for j=1:n
		g_strich{i,j} = @(eta) 0+0*eta; % Freiheitsgrad, anonyme Matrixfunktion mit ausschlie�lich Elementen unter der Hauptdiagonalen
	end
end


% Ortsvektoren festlegen
zdiscKernel = linspace(0,1,ndiscKernel);
zdiscRegEq = linspace(0,1,ndiscRegEq);
zdiscPlant = linspace(0,1,ndiscPlant);
zdiscTarget = linspace(0,1,ndiscTarget);
zdisc_obs_kernel = linspace(0,1,ndiscObsKernel);
zdiscObs = linspace(0,1,ndiscObs);
zdiscPars = linspace(0,1,ndiscPars);

% F wird in diskreter Form �bergeben
% F_disc = zeros(ndiscPars,ndiscPars,n,n);
% for z_idx = 1:ndiscPars
% 	F_disc(z_idx,:,:,:) = eval_pointwise(@(zeta) F(zdiscPars(z_idx),zeta),zdiscPars);
% end

% Signalmodell parametrieren
Sd = [0 4;-4 0];
Sr1 = [0 1;0 0];
Sr2 = [0 2;0 0];
pd_til = [1 0];
pr_til1 = [0.5 2];
pr_til2 = [0.5 -5];
Sr = blkdiag(Sr1,Sr2);
pr_til = blkdiag(pr_til1,pr_til2);
sM = dps.parabolic.control.outputRegulation.signalModel(Sd,Sr,pd_til,pr_til);

% Eigenwertvorgabe St�r- und F�hrungsbeobachter:
eigDist = eig(sM.Sd)-10;  % Eigenwerte St�rbeobachter
eigDist = [-6.5+1.8i -6.5-1.8i];  % Eigenwerte St�rbeobachter
eigRef = (-3-(1:sM.nr)).';  % Eigenwerte F�hrungsbeobachter
% eigRef = [-;-11;-12;-7];
	  
% ppide als Objekt erzeugen:
ppide = dps.parabolic.system.ppide_sys(...
				'name','Plant',...
				'l',lambda,...
				'l_diff',lambda_diff,...
				'c',C,...
				'b',@(z) 0*z + zeros(n,n),...  % ver
				'a', a,...
				'b_op1',b_op1,...
				'b_op2',b_op2,...
				'b_1',eye(n,n),...
				'b_2',eye(n,n),... % ein Eingang f�r jeden Zustand
				'g_2',ones(n,1),... % St�rung am linken Rand
				'g_3',ones(n,1),... % St�rung am rechten Rand
				'g_1',@(z) 2*ones(n,1),...
				'c_op',c_op,...
				'cm_op',Cm_op,...
				'ndisc',ndiscPlant,...
				'ndiscPars',ndiscPars,...
				'f',F,...
				'a_0',a0,...
				'ctrl_pars',dps.parabolic.control.ppde_controller_pars('eliminate_convection',1,...
												 'mu',mu_mat,...
												 'mu_o',mu_o_mat,...
												 'zdiscCtrl',zdiscKernel,...
												 'zdiscRegEq',zdiscRegEq,...
												 'zdiscObsKernel',zdisc_obs_kernel,...
												 'zdiscObs',zdiscObs,...
												 'zdiscTarget',zdiscTarget,...
												 'eigDist',eigDist,...
												 'eigRef',eigRef,...
												 'it_max',itmax,...
												 'it_min',itmin,...
												 'tol',tol,...
												 'g_strich',g_strich,...
												 'min_xi_diff',min_xi_diff,...
												 'min_eta_diff',min_eta_diff));			
											 
% Eigenwerte untersuchen und langsamsten Eigenwert anzeigen:
evalc('ppide_appr = dps.parabolic.system.ppide_approximation(ppide);');
A = ppide_appr.A;											 

% eigenwerte Zielsystem pr�fen, um sicherzugehen, dass die Wahl von mu
% passt:
evalc('ew_target = ppide.eigTarget(ppide.ctrl_pars.mu);');


% Simulate reference behaviour:
% create Initial values of signal model states
tickL = 3;
bigTicks = 3;
bigBlocks = 4;
tv = 0:tickL:tickL*bigTicks*bigBlocks-tickL;
smallTicks = 2;
r1C = 0;
r2C = 0;
v0_ref(1,:) = [zeros(1,sM.nd) 0 1 0 1];
for t_idx = 2:length(tv)
	% S�gezahn
	v0_ref(t_idx,:) = [zeros(1,sM.nd) 0 1 0 1];
	if r1C < bigTicks-1
		v0_ref(t_idx,3) = NaN;
		v0_ref(t_idx,4) = NaN;
		r1C = r1C+1;
	else
		r1C = 0;
	end
	if r2C < smallTicks-1
		v0_ref(t_idx,5) = NaN;
		v0_ref(t_idx,6) = NaN;
		r2C = r2C+1;
	else
		r2C = 0;
	end
end

sim_pars.tend = tickL*bigTicks*bigBlocks;
simRefpars = {...
		'simPars',sim_pars,...
		'sM',sM,...
		'v0',v0_ref,...
		'tv',tv,...
		'vHat0',zeros(sM.nv,1),...
		'plotPars',plot_pars,...
		'useReferenceObserver',1,...
		'useDisturbanceObserver',1,...
		'useStateObserver',1,...
		'useStateFeedback',1,...
		'useOutputRegulation',1,...
		'name',names{1},...
};
% 		'xHat0',0*IPOTot*x0_disc,...

evalc('simRef = dps.parabolic.control.backstepping.ppide_simulation(ppide,simRefpars);');

% Simulate disturbance behaviour
sim_parsDist = sim_pars;
sim_parsDist.tend = 5;
v0Dist = [0 1 zeros(1,sM.nr)];% simulate_ppide_sys
tvDist = 0;

simDistpars = {...
		'v0',v0Dist,...
		'simPars',sim_parsDist,...
		'sM',sM,...
		'tv',tvDist,...
		'vHat0',zeros(sM.nv,1),...
		'plotPars',plot_pars,...
		'useReferenceObserver',1,...
		'useDisturbanceObserver',1,...
		'useStateObserver',1,...
		'useStateFeedback',1,...
		'useOutputRegulation',1,...
		'name',names{2},...
};

evalc('simDist = dps.parabolic.control.backstepping.ppide_simulation(ppide,simDistpars);');

% Simulation test:

evalc('simRef = simRef.simulate;');
 % resample to Comp resolution:
tComp = simRefComp.t;
xERes = interp1(simRef.t,simRef.xE,tComp);
errxE = simRefComp.xE - xERes;
% if mean(abs(errxE)) > 1e-3
% 	warning(['reference: mean(abs(errxE(:)))=' num2str(mean(abs(errxE(:))))])
% end
% assert verh�lt sich nicht anders, als mit if und error!
assert(mean(abs(errxE(:))) < 1e-3,['reference: mean(abs(errxE(:)))=' num2str(mean(abs(errxE(:))))])

simDist = simRef;
evalc('simDist = simDist.setPars(ppide,simDistpars);');
evalc('simDist.simulate;');
tComp = simDistComp.t;
xEResDist = interp1(simDist.t,simDist.xE,tComp);
errxE = simDistComp.xE - xEResDist;
if mean(abs(errxE)) > 1e-3
	error(['disturbance: mean(abs(errxE(:)))=' num2str(mean(abs(errxE(:))))])
end
%% setParsTest
evalc('simRef = simRef.setPars(ppide,simRefpars);');
evalc('simDist = simDist.setPars(ppide,simDistpars);');

%% Test plot functions
simRef = simRef.plotOutput;
simRef = simRef.plotSigModStates;

