
% cd C:\Users\uf83iwyd\Localhome\Workspace\Forschung\Matlab_Workspace
% addmyPath(genmyPath([pwd '\' 'M_files']));
% addpath(genpath([pwd '\' 'NewLib']));
% external nochmal einbinden, falls man in NewLib arbeitet!
addpath(genpath([pwd '\' 'external']));
addpath(genpath([pwd '\..\' 'miscLib']));
addpath(genpath([pwd '\+dps'])); % add current folder to include subfolders, which are not namespaces

set(groot,'defaulttextinterpreter','none'); 
import misc.*
import numeric.*
import dps.parabolic.system.*
import dps.parabolic.control.backstepping.*
import dps.parabolic.script.*


%% Pfad und Namen der Simulationen:
myPath = 'NewLib/+dps/+parabolic/+mat/timeDependent/';
import misc.*
import numeric.*

names{1} = 'sim';

% wichtigsten Aufloesungen gleich hier festlegen:
ndiscKernel = 31;% Aufloesung Kern
ndiscObsKernel = 31; % Aufloesung Beobachterkern und Entkopplungsgleichungen

% Vorhandene Dateien laden, falls existent:
% for i=1:length(names)
% 	if ~exist(names{i},'var')
% 		srchstr = [myPath... % myPath
% 			'*'...                    % new date
% 			names{i}...                 % new name
% 			'_n'...		% num States
% 			'*'...
% 			'_nc'...
% 			num2str(ndiscKernel)... % ndiscCtrl
% 			'_noK'... % rest unchanged
% 			num2str(ndiscObsKernel)...
% 			'.mat'];
% 		fls = dir(srchstr);
% 		if ~isempty(fls)
% 			load([myPath fls(end).name]); %nichts laden
% 		end
% 	end
% end


%%
% Simulationskonfiguration
sim_pars = misc.simulation_parameters('frel',1e-6,...
								 'fabs',1e-6,... 
						 		 'tsmax',2e-2,...
								 'tend',1); % 8
% Ploteinstellungen
plot_pars.plot_res_1D = 50;
plot_pars.plot_res_2D_lines = 150;
plot_pars.plot_res_2D_num_lines = 20;
							
% Sukzessive Approximation:
itmax = 40; 
itmin = 1;
tol = 1e-3; % Abbruchkriterium sukzessive Approximation
min_xi_diff = 0.02; % numerischer Parameter, f�r numerische Effizienz
min_eta_diff = 0.02;
min_xi_diff_border = 0.00;
min_eta_diff_border = 0.00;

ndiscPars = 151; % Aufloesung gespeicherte Parameter
ndiscRegEq = 5; % Aufloesung Regulator equations
ndiscPlant = 101;% Aufl�sung Streckenapproximation
ndiscObs = 101; % Aufloesung Beobachterrealisierung
ndiscTarget = ndiscPlant; % Aufloesung Zielsystem (f�r Analysezwecke)
ndiscTime = 51; % Aufl�sung Diskretisierung Zeitachse Kern
ndiscTimePlant = 151; % Aufl�sung Zeitachse Simulatoin
 
% Strecke festlegen:
n=3; % Anzahl Zust�nde
% reset everything if n has changed!
if exist('nOld','var')
	if n~= nOld 
		clearvars
		warning('n has changed, resetted everything.')
		dps.parabolic.script.SimTimeDependentPPIDEs
		return
	end
else
	nOld = n;
end
	


% Streckenparameter
% create Gevrey Signals
b = signals.gevrey.Bump('K',1,'order',1.8,'N',ndiscPars);
[bDisc0, tBump0] = b.eval(0);
[bDisc1, tBump1] = b.eval(1);
bDisc1 = bDisc1./max(bDisc1(:));
% figure('Name','BumpFunction')
% plot(tBump1,bDisc1);
% xticks([0 0.5 1]);
% yticks([0 1]);
% 
% figure('Name','StepFunction')
% plot(tBump0,bDisc0);
% xticks([0 0.5 1]);
% yticks([0 1]);
% [bDisc2, tBump2] = b.eval(1);
% bDisc3 = bDisc2(floor(length(tBump)/4):floor(length(tBump)*2/3));
% bDisc3 = bDisc2(floor(length(tBump)*4/10):floor(length(tBump)*2/3));
% bDisc3 = bDisc2(floor(length(tBump)*3/10):floor(length(tBump)*3/3));
% bDisc3 = bDisc2(floor(length(tBump)/2):floor(length(tBump)*1));
K1 = -5;
O1 = 3;
a11Interp = numeric.interpolant({tBump1},K1*bDisc1+O1);
K2 = 1;
O2 = 0;
bInterp = numeric.interpolant({tBump0},K2*bDisc0+O2);

if n==1
% 	lambda = @(z) 1+0*z; 
	lambda = @(z) 3/2+z.^2.*cos(2*pi*z);
% 	lambda_diff = @(z) 0*z;
	lambda_diff = @(z) 2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi;
	C = @(z) 0+0*z;
% 	a = @(z,t) 0+4*z-4*cos(2*pi*t);
% 	a = @(z,t)(2+3*sin((z+t)*2*pi));
	a = @(z,t) bInterp2.evaluate(t);
% 	a = @(z)(2+3*sin((z)*2*pi));
% 	a = @(z) 1+0*z;
	a0 = @(z) 0+0*z;
	F = @(z,zeta) 0*z+0*zeta;
	x0 = @(z) dps.parabolic.tool.get_x0_ppides(z,1);
% 	c_op = output_operator('c_m',1,'z_m',0.5,'c_0',0);
elseif n==2
% 	lambda = @(z) [3/2+z.^2.*cos(2*pi*z), 0;0, 1/2-1/4* sin(2*pi*z)];
% 	lambda_diff = @(z) [2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi,0;0, -1/4*cos(2*pi*z)*2*pi];
	C = @(z) 0*[1 0; 0 1]; % Konvektionsmatrix
% 	a = @(z,t) 1*[bInterp.evaluate(t)-2 3*cos((z+t)*2*pi); (2+3*sin((z+t)*2*pi)) -bInterp.evaluate(t)-2]; %geht!
% 	a = @(z) 1*[-5 3*cos((z)*2*pi); (2+3*sin((z)*2*pi)) -5]; %geht!
% 	a = @(z) 1*[-1 0; 0 -1]+0*z; %geht!
% 	mu = @(z,t) 1*[10 0; -(2+3*sin((z+t)*2*pi)) 10]; %geht!
% 	mu =@(z) 0*[10 0;-(2+3*sin((z)*2*pi)) 10];
% diffusion
	lambda = @(z) [3/2+z.^2.*cos(2*pi*z), 0;0, 1/2-1/4*sin(2*pi*z)];
	lambda = @(z) [1/2-1/4*sin(2*pi*z), 0;0, 1/2-1/4*sin(2*pi*z)];
	% and derivative
	lambda_diff = @(z) [2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi,0;0, -1/4*cos(2*pi*z)*2*pi];
	lambda_diff = @(z) [-1/4*cos(2*pi*z)*2*pi,0;0, -1/4*cos(2*pi*z)*2*pi];
	% reaction
% 	a = @(z,t) 1*[a11Interp.evaluate(t) 3*cos((z+t)*2*pi); (2+2*sin((z+t)*2*pi)) a11Interp.evaluate(t)];
	a = @(z,t) 1*[(2+2*sin((z+t)*2*pi)) a11Interp.evaluate(t);
				   3*cos((z+t)*2*pi) 3];
% 	a = @(z,t) 1*[a11Interp.evaluate(1-t) 3*cos((z+(1-t))*2*pi); (2+2*sin((z+(1-t))*2*pi)) a11Interp.evaluate(1-t)];
% 	a = @(z) zeros(n,n);
% 	mu =@(z) 10*[1 0;0 1]+0*z;
	mu = 10*eye(n);
% 	a0 = @(z) 1*[z z^2; z -5]; 
% 	a0 = @(z,t) 0*([z z^2; z -5]+t);  
% 	a0 = @(z,t) 1*([0 0; 0 0]+sin(2*pi*t)); 
	a0 = @(z,t) ones(n,n)+z+sin(pi/2*(1-t));
% 	a0 = @(z) 0*[5 0; 0 5]; 
% 	F = @(z,zeta,t) [exp(z+zeta) exp(z-zeta);1-exp(-(z-zeta)) exp(-(z+zeta))]+t; % geht nicht!
	F = @(z,zeta,t) ones(n,n)+z+zeta+2*exp(-t);
% 	F = @(z,zeta) 2*[exp(z+zeta) exp(z-zeta);1-exp(-(z-zeta)) exp(-(z+zeta))];
% 	F = @(z,zeta) 0*[5*z*zeta 0;0 5];
	x0 = @(z) [dps.parabolic.tool.get_x0_ppides(z,1);dps.parabolic.tool.get_x0_ppides(z,1)];
elseif n==3	
% 	lambda = @(z) [3/2+z.^2.*cos(2*pi*z), 0, 0;0, 1/2-1/4* sin(2*pi*z), 0;0 0 1/2-1/4* sin(2*pi*z)];
	lambda = @(z) [3/2+z.^2.*cos(2*pi*z), 0, 0;0, 3/2+z.^2.*cos(2*pi*z), 0;0 0 1/2-1/4* sin(2*pi*z)];
	lambda_diff = @(z) [2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi,0, 0;
						0,2*z.*cos(2*pi*z)-z.^2.*sin(2*pi*z)*2*pi, 0;
						0, 0, -1/4*cos(2*pi*z)*2*pi];
% 	a = @(z,t) 1*[bInterp.evaluate(t)-2 3*cos((z+t)*2*pi) 5; (2+3*sin((z+t)*2*pi)) 2+5*(t+1)^(-4)]; %geht!
% 	a =@(z,t) 2*ones(n,n)+2*z+3*t;
	a = @(z,t) 1*[a11Interp.evaluate(t) 3*cos((z+t)*2*pi) 1; (2+2*sin((z+t)*2*pi)) a11Interp.evaluate(t) 1;
				  3*cos((z+t)*2*pi) 1 a11Interp.evaluate(t)];
	mu = @(z,t) 10*eye(n)-0*[0 0 0;
							  1 0 0;
							  2*a11Interp.evaluate(t) 3*cos((z+t)*2*pi) 0];
% 	mu = 10*eye(n);
	a0 = @(z,t) 0*(ones(n,n)+z+sin(pi/2*(1-t)));
	F = @(z,zeta,t) ones(n,n)+z+zeta+2*exp(-t);
	x0 = @(z) [dps.parabolic.tool.get_x0_ppides(z,1);dps.parabolic.tool.get_x0_ppides(z,1);dps.parabolic.tool.get_x0_ppides(z,1)];
	C = @(z) zeros(n);
else 
	error(['n = ' num2str(n) ' not parametrized']);
end
% Rechte Randbedingung festlegen:
numDirRight = 0; % number of Dirichlet BCs

Bdr = @(t) t*(1-t)*2*diag((n-numDirRight):-1:1);
% Bdr = 0*diag((n-numDirRight):-1:1);
if isa(Bdr,'function_handle')
	b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
							  'b_d', @(t) blkdiag(eye(numDirRight),Bdr(t)),... %Dirichlet
							  'b_n',blkdiag(zeros(numDirRight),eye(n-numDirRight))); % Robin
else
	b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
							  'b_d',blkdiag(eye(numDirRight),Bdr),... %Dirichlet
							  'b_n',blkdiag(zeros(numDirRight),eye(n-numDirRight))); % Robin					  
end
% linken Rand-Operator definieren:
numDir = 1; % number of Dirichlet BCs
% Bd = @(t) -(3-sin(pi*t))*eye(n-numDir);
Bd = @(t) -eye(n-numDir)*bInterp.evaluate(t);
% Bd = -0*diag((n-numDir):-1:1);
% Bd = eye(n);
% Bd = eye(n);
if isa(Bd,'function_handle')
b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
						  'b_d',@(t) blkdiag(eye(numDir),Bd(t)),... %Dirichlet
						  'b_n',blkdiag(zeros(numDir,numDir),eye(n-numDir))); %Neumann
% 					  'b_d',@(t) blkdiag(eye(numDir),Bd(t)),... %Dirichlet
else
b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
						  'b_d',blkdiag(eye(numDir),Bd),... %Dirichlet
						  'b_n',blkdiag(zeros(numDir),eye(n-numDir))); % Robin	
end

% Bdr = @(t) [t+sin(t) -t;sin(2*t) cos(t)];
% Bnr = eye(n,n);
% b_op2 = dps.parabolic.system.boundary_operator('z_b',1,...
% 						  'b_d',Bdr,...
% 						  'b_n',Bnr);
% 
% % leftBC
% numDir = 0; % number of Dirichlet BCs 
% Bd = @(t) -eye(n).*[bInterp.evaluate(t) 0;0 bInterp.evaluate(t)];
% b_op1 = dps.parabolic.system.boundary_operator('z_b',0,...
% 						  'b_d',blkdiag(eye(numDir),Bd),... %Dirichlet
% 						  'b_n',blkdiag(zeros(numDir,numDir),eye(n-numDir))); %Neumann
					  


% Messung (unabh�ngig von Randbedingung!)
Cm_op = dps.output_operator('c_0',blkdiag(zeros(numDir),eye(n-numDir)),...
							'c_d0',blkdiag(eye(numDir),zeros(n-numDir)));

% Regelung konfigurieren:
% mu_mat = @(z,t) [10+2*z+t,0;0,10-z];
% mu_o_mat = @(z,t) 1.3.*[10+2*z+t,0;0,10-z];
% mu_mat = 10*eye(n);
mu_mat = mu; % geht
% mu_o_mat = @(z,t) 10*eye(n)+[0 10*(t+z); 0 0]; % geht
mu_o_mat = 10*eye(n);


% Freiheitsgrad zu 0 festlegen.
clearvars g_strich
for i=1:n
	for j=1:n
		g_strich{i,j} = @(eta) 0+0*eta; % Freiheitsgrad, anonyme Matrixfunktion mit ausschlie�lich Elementen unter der Hauptdiagonalen
	end
end


% Ortsvektoren festlegen
zdiscKernel = linspace(0,1,ndiscKernel);
zdiscRegEq = linspace(0,1,ndiscRegEq);
zdiscPlant = linspace(0,1,ndiscPlant);
zdiscTarget = linspace(0,1,ndiscTarget);
zdisc_obs_kernel = linspace(0,1,ndiscObsKernel);
zdiscObs = linspace(0,1,ndiscObs);
zdiscPars = linspace(0,1,ndiscPars);
tDiscKernel = linspace(0,1,ndiscTime);
tDiscPlant = linspace(0,1,ndiscTimePlant);
	  
% help = mu_mat;
% clear mu_mat
% for zidx = 1:ndiscKernel
% 	mu_mat(zidx,:,:,:) = permute((misc.eval_pointwise(@(z)help(zdiscKernel(zidx),z),tDiscKernel)),[2 3 1]);
% end

% ppide als Objekt erzeugen:
ppide = dps.parabolic.system.ppide_sys(...
				'name','Plant',...
				'l',lambda,...
				'l_diff',lambda_diff,...
				'c',C,...
				'b',@(z) 0*z + zeros(n,n),...  % ver
				'a', a,...
				'b_op1',b_op1,...
				'b_op2',b_op2,...
				'b_1',eye(n,n),...
				'b_2',eye(n,n),... % ein Eingang f�r jeden Zustand
				'ndisc',ndiscPlant,...
				'ndiscTime',ndiscTimePlant,...
				'ndiscPars',ndiscPars,...
				'f',F,...
				'a_0',a0,...
				'cm_op',Cm_op,...
				'ctrl_pars',dps.parabolic.control.ppde_controller_pars('eliminate_convection',0,...
												 'makeTargetNeumann',1,...
												 'quasiStatic',0,...
												 'mu',mu_mat,...
												 'mu_o',mu_o_mat,...
												 'zdiscCtrl',zdiscKernel,...
												 'zdiscRegEq',zdiscRegEq,...
												 'zdiscObsKernel',zdisc_obs_kernel,...
												 'zdiscObs',zdiscObs,...
												 'zdiscTarget',zdiscTarget,...
												 'tDiscKernel',tDiscKernel,...
												 'it_max',itmax,...
												 'it_min',itmin,...
												 'tol',tol,...
												 'g_strich',g_strich,...
												 'min_xi_diff',min_xi_diff,...
												 'min_eta_diff',min_eta_diff,...
												 'min_xi_diff_border',min_xi_diff_border,...
												 'min_eta_diff_border',min_eta_diff_border));			

											 
% Eigenwerte untersuchen und langsamsten Eigenwert anzeigen:
ppide_appr = dps.parabolic.system.ppide_approximation(ppide);
A = ppide_appr.A;
eigs=[];
for tIdx = 1:size(A,3)
	eigs = [eigs;eig(A(:,:,tIdx))];
end
	
[meigreal, ind] = max(real(eigs));
[meigimag, indImag] = max(imag(eigs));
meig = eigs(ind);
meigImag = eigs(indImag);
% noch verbessern:
if meig>0
	disp(['die Strecke ist instabil: ' num2str(meig)])
else
	disp(['die Strecke ist stabil: ' num2str(meig)])
end

% eigenwerte Zielsystem pr�fen, um sicherzugehen, dass die Wahl von mu
% passt:
ew_target = ppide.eigTarget(ppide.ctrl_pars.mu);
ew_obs = ppide.eigTarget(ppide.ctrl_pars.mu_o);

if max(real(ew_target)) > 0
	error(['Largest eigenvalue of the target system: ' num2str(max(real(ew_target))) '!'])
end
if max(real(ew_obs)) > 0
	error(['Largest eigenvalue of the observer target system: ' num2str(max(real(ew_obs))) '!'])
end

fprintf(['Positivster Eigenwert des Zielsystems (ohne mu) = ' num2str(max(ppide.eigTarget(zeros(n,n)))) '\n']);

%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
% ENDE EINGABEN

%% Simulate
dps.parabolic.script.simScriptTimeDependent


% simRefScript
%%
% dps.parabolic.script.simQuasiStatic










