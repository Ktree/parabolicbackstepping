# ParabolicBackstepping
These scripts were used to create the simulation results of the Dissertation:
Simon Kerschbaum: Backstepping Control of Coupled Parabolic Systems with Varying Parameters (2020)
at the Chair of Automatic Control,
Friedrich-Alexander-Universität Erlangen-Nürnberg.

Each script in the main folder is independent of each other.
It is recommended to start with the script 
    c_simulationAcademicExample
since this contains the most explaining comments.

## Contents
For the simulations, two libraries are used. 
`Lib` contains all the classes and methods created by the author for the implementation of the backstepping control of parabolic systems.
`conI` is the common library created at the infinite-dimensional sysmstems group at the chair of automatic control, University of Erlangen and the "Institut für Mess-, Regel- und Mikrotechnik" at the University of Ulm. It contains auxiliary matlab functions and classes and is available under the LGPL under [this link](https://gitlab.com/control-system-tools/coni"conI Repository").
The included folder is a copy of the repository at commit a0deba56085ce6ca14e6a11a52bddd837cc9887, which is on branch `dissertationKerschbaum`. It is not merged to master, since the master branch is set-up for usage with Matlab2020, while this library is supposed to run on Matlab2018a.
The interested contributor is highly welcomed to perform these updates and to merge the branches.

## License
The code is available under the LGPL 3.0 (see file LICENSE).

## Usage and prerequesits
Download the files into a folder on your local drive. Run Matlab 2018a and switch to this folder. Then simply execute the scripts in the main folder.

The provided code is intended to run under Matlab(R) 2018a. The current master branch of the utilized library conI is optimized for usage in Matlab(R) 2020a. Therefore, the included copy of the conI library is at a commit not finally merged into the master branch.
